# coding=utf-8
import json
import os
import jinja2
import webapp2
from google.appengine.api import memcache
import uuid
from google.appengine.api import urlfetch
urlfetch.set_default_fetch_deadline(300)
import socket
socket.setdefaulttimeout(300)
from helpers import *


###############################################################################
#                        Handlers para requests web                           #
###############################################################################
class MainHandler(webapp2.RequestHandler):
    """
    Funcion handler principal para iniciar mapa en plataforma SIMA.

    Parametros:
        webapp2.RequestHandler: Resultado de un request http.

    Devuelve:
        Respuesta http con informacion para iniciar mapa.
    """

    def get(self, path=''):
        try:
            """
            Returns the main web page, populated with EE map and polygon info.
            """
            mapid = GetSimaMapId()
            template_values = {
                'eeMapId': mapid['mapid'],
                'eeToken': mapid['token'],
                'serializedPolygonIds': '[]',
                'serializedCuencaIds': '[]'
            }
            template = JINJA2_ENVIRONMENT.get_template('www/index.html')
            self.response.out.write(template.render(template_values))
        except:
            # En caso de error, presentar mensaje.
            content = json.dumps(
                {'error': 'System error (Main Handler)'})
            self.response.headers['Content-Type'] = 'application/json'
            self.response.headers['Access-Control-Allow-Origin'] = '*'
            self.response.out.write(content)


class MapIdHandler(webapp2.RequestHandler):
    """
    Funcion handler para consulta de informacion de mapa actual.
    Recibe un request http generado en el front-end y ejecuta la funcion
    GetSimaMapId para consultar el id del mapa desplegado.

    Parametros:
        webapp2.RequestHandler: Resultado de un request http.

    Devuelve:
        Respuesta http con informacion correspondiente al mapa desplegado en pantalla.
    """

    def get(self, path=''):
        try:
            # Consultar informacion de mapa actual.
            mapid = GetSimaMapId()
            # Generar respuesta para devolver a front-end.
            content = {}
            content["mapid"] = mapid['mapid']
            content["token"] = mapid['token']
            self.response.headers['Content-Type'] = 'application/json'
            self.response.headers['Access-Control-Allow-Origin'] = '*'
            # Devoler respuesta al front-end
            self.response.out.write(json.dumps(content))
        except:
            # En caso de error, presentar mensaje.
            content = json.dumps(
                {'error': 'System error (MapIdHandler)'})
            self.response.headers['Content-Type'] = 'application/json'
            self.response.headers['Access-Control-Allow-Origin'] = '*'
            self.response.out.write(content)


class EODataHandler(webapp2.RequestHandler):
    """
    Funcion handler para manejo y despliegue de datasets sobre mapa.
    Recibe un request http generado en el front-end y segun la fuente del
    dataset seleccionado se ejecuta la funcion correspondiente.

    Parametros:
        webapp2.RequestHandler: Resultado de un request http.

    Variables extraidas desde webapp2.RequestHandler:
        eo_product: producto a desplegar sobre mapa.
        eo_type: tipo de producto (Image, ImageCollection)
        eo_dataset: nombre de dataset a mostrar.
        eo_min: valor minimo para paleta de colores
        eo_max: valor maximo para paleta de colores
        eo_palette: paleta de colores a usar
        eo_day_start: fecha inicial para ImageCollection
        eo_day_end: fecha final para ImageCollection
        aggregation_type: tipo de valor a representar (min, max, etc.)
        masking_toggle: opcion para mascara de capa
        cachekey: identificador en cache para mapa actual

    Devuelve:
        Respuesta http con informacion para despliegue de dataset.
    """

    def get(self, path=''):
        print('Eo Data Handler GET')
        try:
            # Obtener parametros del request http.
            eo_product = self.request.get('product_id')
            eo_dataset = self.request.get('dataset_id')
            eo_type = self.request.get('type_id')
            eo_min = self.request.get('min_id')
            eo_max = self.request.get('max_id')
            eo_palette = self.request.get('palette_id')
            eo_group = self.request.get('group_id')
            eo_day_start = self.request.get('day_start_id')
            eo_day_end = self.request.get('day_end_id')
            existing_cachekey = self.request.get('cachekey')
            masking_toggle = self.request.get('masking_toggle') == "true"
            agregation_type = self.request.get('agregation_type')
            # Liberar cache
            if len(existing_cachekey) > 0:
                memcache.delete(existing_cachekey)
            cachekey = uuid.uuid4().hex
            # Verificar fuente de datos para ejecutar funcion correspondiente.
            if eo_group == 'WI':
                mapid = calculateLandsatIndexes(
                    eo_product, eo_day_start, eo_day_end, eo_min, eo_max,
                    eo_palette, agregation_type, cachekey)
            elif eo_group == 'GFW':
                mapid = GetGFW_EOData_Id(
                    eo_dataset, eo_product, eo_min, eo_max,
                    eo_palette, masking_toggle, cachekey)
            else:
                mapid = GetSima_EOData_Id(
                    eo_product, eo_type, eo_dataset, eo_min,
                    eo_max, eo_palette, eo_day_start, eo_day_end,
                    agregation_type, masking_toggle, cachekey)
            # Generar respuesta http para devolver al front-end.
            content = json.dumps(
                {'eeMapId': mapid['mapid'],
                 'eeToken': mapid['token'],
                 'cachekey': cachekey})
            self.response.headers['Content-Type'] = 'application/json'
            self.response.headers['Access-Control-Allow-Origin'] = '*'
            # Devolver respuesta al front-end.
            self.response.out.write(content)
        except Exception as e:
            print(e)
            # En caso de error, presentar mensaje.
            content = json.dumps(
                {'error': 'System error (EODataHandler) ' + (str(e))})
            self.response.headers['Content-Type'] = 'application/json'
            self.response.headers['Access-Control-Allow-Origin'] = '*'
            self.response.out.write(content)


class resourcesHandler(webapp2.RequestHandler):
    """
    Funcion handler para manejo de assets.
    Recibe el resultado de un request http generado en el front-end,
    luego extrae las variables necesarias y en funcion del tipo de
    request se ejecuta la funcion getCustomAssets o visualizeAsset.

    Parametros:
        webapp2.RequestHandler: Resultado de un request http

    Variables extraidas desde webapp2.RequestHandler:
        mode_id: tipo de operacion ('read', 'write').
        dataset_id: id del dataset seleccionado.

    Devuelve:
        Respuesta http con informacion de asset seleccionado.
    """
    # Verificar fuente de los datos para ejecutar funcion correspondiente.

    def get(self, path=''):
        try:
            cachekey = uuid.uuid4().hex
            mode_id = self.request.get('mode_id')
            # Verificar tipo de operacion y generar respuesta http
            # para devolver al front-end.
            if mode_id == 'read':
                content = getCustomAssets()
            elif mode_id == 'query':
                dataset_id = self.request.get('dataset_id')
                content = json.dumps(getCustomAssetProperties(dataset_id))
            else:
                dataset_id = self.request.get('dataset_id')
                mapid = visualizeAsset(dataset_id, cachekey)
                content = json.dumps(
                    {'eeMapId': mapid['mapid'],
                     'eeToken': mapid['token'],
                     'cachekey': cachekey})
            self.response.headers['Content-Type'] = 'application/json'
            self.response.headers['Access-Control-Allow-Origin'] = '*'
            # Devolver respuesta al front-end.
            self.response.out.write(content)
        except:
            # En caso de error, presentar mensaje.
            content = json.dumps(
                {'error': 'System error (resourcesHandler)'})
            self.response.headers['Content-Type'] = 'application/json'
            self.response.headers['Access-Control-Allow-Origin'] = '*'
            self.response.out.write(content)


class shapefileHandler(webapp2.RequestHandler):
    """
    Funcion handler para manejo de shapefiles.
    Recibe el resultado de un request http generado en el front-end y
    ejecuta la funcion getShapefileAssets para listar poligonos disponibles.

    Parametros:
        webapp2.RequestHandler: Resultado de un request http

    Devuelve:
        Respuesta http con listado de poligonos.
    """

    def get(self, path=''):
        try:
            content = getShapefileAssets()
            self.response.headers['Content-Type'] = 'application/json'
            self.response.headers['Access-Control-Allow-Origin'] = '*'
            self.response.out.write(content)
        except:
            # En caso de error, presentar mensaje.
            content = json.dumps(
                {'error': 'System error (shapefileHandler)'})
            self.response.headers['Content-Type'] = 'application/json'
            self.response.headers['Access-Control-Allow-Origin'] = '*'
            self.response.out.write(content)


class HistogramHandler(webapp2.RequestHandler):
    """
    Funcion handler para generar histogramas.
    Recibe el resultado de un request http generado en el front-end,
    luego extrae las variables necesarias y ejecuta la funcion GetHistogram

    Parametros:
        webapp2.RequestHandler: Resultado de un request http

    Variables extraidas desde webapp2.RequestHandler:
        polygon_id : si es auto, entonces es el id del shape seleccionado,
        de lo contrario, corresponde al  poligono generado por el usuario.
        band_id: id de la capa escogida dentro del dataset_id.
        dataset_id: id del dataset seleccionado.
        mode_id: tipo de poligono (auto, manual).
        min_id: el rango minimo de valores para graficar.
        max_id: el rango maximo de valores para graficar.
        shapefile_id: id de shapefile escogida por el usuario.

    Devuelve:
        Valores para graficar histograma en front-end
    """

    def get(self, path=''):
        try:
            # Obtener parametros del request http.
            polygon_id = self.request.get('polygon_id')
            shapefile_id = self.request.get('shapefile_id')
            band_id = self.request.get('band_id')
            # scale_id = self.request.get('scale_id')
            dataset_id = self.request.get('dataset_id')
            min_id = self.request.get('min_id')
            max_id = self.request.get('max_id')
            mode_id = self.request.get('mode_id')
            # Verificar que tipo de poligono se recibe:
            # auto: poligono de un shapefileHandler
            # else: poligono dibujado por usuario
            if mode_id == 'auto':
                if ((shapefile_id in SHAPEFILE_DATA) and
                        (polygon_id in SHAPEFILE_DATA[shapefile_id])):
                    # Llamar a funcion que genera histograma.
                    content = GetHistogram(
                        polygon_id, band_id, dataset_id, mode_id,
                        min_id, max_id, shapefile_id)
                else:
                    content = json.dumps(
                        {'error': 'Unrecognized polygon ID: ' + polygon_id})
            else:
                # Llamar a funcion que genera histograma.
                content = GetHistogram(
                    polygon_id, band_id, dataset_id, mode_id, min_id, max_id)

            # Enviar informacion de histograma.
            self.response.headers['Content-Type'] = 'application/json'
            self.response.headers['Access-Control-Allow-Origin'] = '*'
            self.response.out.write(content)
        except:
            # En caso de error, presentar mensaje.
            content = json.dumps(
                {'error': 'System error (HistogramHandler)'})
            self.response.headers['Content-Type'] = 'application/json'
            self.response.headers['Access-Control-Allow-Origin'] = '*'
            self.response.out.write(content)


class PieChartHandler(webapp2.RequestHandler):
    """
    Funcion handler para generar graficos de pie (pie charts).
    Recibe el resultado de un request http generado en el front-end,
    luego extrae las variables necesarias y ejecuta la funcion GetPieChart

    Parametros:
        webapp2.RequestHandler: Resultado de un request http

    Variables extraidas desde webapp2.RequestHandler:
        polygon_id : si es auto, entonces es el id del shape seleccionado,
        de lo contrario, corresponde al  poligono generado por el usuario.
        band_id: id de la capa escogida dentro del dataset_id.
        mode_id: tipo de poligono (auto, manual).
        dataset_id: id del dataset seleccionado.
        agg_id: tipo de estadistica (minimo, maximo, promedio) a considerar.
        type_id: tipo de dato de EarthEngine (Image o ImageCollection).
        eo_startdate: fecha inicial para considerar en grafico.
        eo_enddate: fecha final para considerar en grafico.
        shapefile_id: id de shapefile escogida por el usuario.

    Devuelve:
        Valores para generar grafico de pie en front-end
    """

    def get(self, path=''):
        try:
            # Obtener parametros del request http.
            polygon_id = self.request.get('polygon_id')
            dataset_id = self.request.get('dataset_id')
            shapefile_id = self.request.get('shapefile_id')
            band_id = self.request.get('band_id')
            mode_id = self.request.get('mode_id')
            agg_id = self.request.get('agg_id')
            type_id = self.request.get('type_id')
            eo_startdate = self.request.get('startdate_id')
            eo_enddate = self.request.get('enddate_id')
            # Llamar a funcion que genera piechart.
            content = GetPieChart(
                polygon_id, band_id, mode_id, dataset_id, agg_id, type_id,
                eo_startdate, eo_enddate, shapefile_id)
            # Enviar informacion de piechart.
            self.response.headers['Content-Type'] = 'application/json'
            self.response.headers['Access-Control-Allow-Origin'] = '*'
            self.response.out.write(content)
        except:
            # En caso de error, presentar mensaje.
            content = json.dumps(
                {'error': 'System error (PieChartHandler)'})
            self.response.headers['Content-Type'] = 'application/json'
            self.response.headers['Access-Control-Allow-Origin'] = '*'
            self.response.out.write(content)


class SeriesHandler(webapp2.RequestHandler):
    """
    Funcion handler para generar graficos de series temporales.
    Recibe el resultado de un request http generado en el front-end,
    luego extrae las variables necesarias y ejecuta la funcion GetPieChart

    Parametros:
        webapp2.RequestHandler: Resultado de un request http

    Variables extraidas desde webapp2.RequestHandler:
        polygon_id : si es auto, entonces es el id del shape seleccionado,
        de lo contrario, corresponde al poligono generado por el usuario.
        band_id: id de la capa escogida dentro del dataset_id.
        scale_id: escala geografica de EarthEngine utilizada para analisis.
        dataset_id: id del dataset seleccionado.
        mode_id: tipo de poligono (auto, manual).
        eo_startdate: fecha inicial para considerar en grafico.
        eo_enddate: fecha final para considerar en grafico.
        shapefile_id: id de shapefile escogida por el usuario.

    Devuelve:
        Valores para generar grafico de series temporales en front-end
    """

    def get(self):
        try:
            # Obtener parametros del request http.
            polygon_id = self.request.get('polygon_id')
            band_id = self.request.get('band_id')
            scale_id = self.request.get('scale_id')
            dataset_id = self.request.get('dataset_id')
            mode_id = self.request.get('mode_id')
            eo_startdate = self.request.get('startdate_id')
            eo_enddate = self.request.get('enddate_id')
            shapefile_id = self.request.get('shapefile_id')
            # Verificar que tipo de poligono se recibe:
            # auto: poligono de un shapefileHandler
            # else: poligono dibujado por usuario
            if mode_id == 'auto':
                if ((shapefile_id in SHAPEFILE_DATA) and
                        (polygon_id in SHAPEFILE_DATA[shapefile_id])):
                    # Llamar a funcion que genera timeseries.
                    content = GetPolygon_TimeSeries(
                        polygon_id, band_id, scale_id, dataset_id,
                        mode_id, eo_startdate, eo_enddate,
                        shapefile_id)
                else:
                    content = json.dumps(
                        {'error': 'Unrecognized polygon ID: ' + polygon_id})
            else:
                # Llamar a funcion que genera timeseries.
                content = GetPolygon_TimeSeries(
                    polygon_id, band_id, scale_id, dataset_id,
                    mode_id, eo_startdate, eo_enddate)
            # Enviar informacion de timeseries.
            self.response.headers['Content-Type'] = 'application/json'
            self.response.headers['Access-Control-Allow-Origin'] = '*'
            self.response.out.write(content)
        except:
            # En caso de error, presentar mensaje.
            content = json.dumps(
                {'error': 'System error (SeriesHandler)'})
            self.response.headers['Content-Type'] = 'application/json'
            self.response.headers['Access-Control-Allow-Origin'] = '*'
            self.response.out.write(content)


class PixelValueHandler(webapp2.RequestHandler):
    """
    Funcion handler para consultar valor de pixel.
    Recibe el resultado de un request http generado en el front-end,
    luego extrae las variables necesarias y ejecuta la funcion GetPixelValue

    Parametros:
        webapp2.RequestHandler: Resultado de un request http

    Variables extraidas desde webapp2.RequestHandler:
        polygon_id: poligono generado por el usuario en el front end
        band_id: banda definida por el usuario en el front end
        scale_id: escala geografica de EarthEngine utilizada para analisis.
        mode_id: indica el tipo de poligono/punto (manual en este caso)
        cachekey: identificador en cache

    Devuelve:
        Valor de capa activa en posicion de pixel
    """

    def get(self, path=''):
        try:
            # Obtener parametros del request http.
            polygon_id = self.request.get('polygon_id')
            band_id = self.request.get('band_id')
            scale_id = self.request.get('scale_id')
            mode_id = self.request.get('mode_id')
            cachekey = self.request.get('cachekey')
            # Enviar informacion de pixel
            # Llamar a funcion que obtiene valor del pixel seleccionado
            content = GetPixelValue(polygon_id, band_id, scale_id,
                                    mode_id, cachekey)
            # Enviar informacion de pixel
            self.response.headers['Content-Type'] = 'application/json'
            self.response.headers['Access-Control-Allow-Origin'] = '*'
            self.response.out.write(content)
        except:
            # En caso de error, presentar mensaje.
            content = json.dumps(
                {'error': 'System error (PixelValueHandler)'})
            self.response.headers['Content-Type'] = 'application/json'
            self.response.headers['Access-Control-Allow-Origin'] = '*'
            self.response.out.write(content)


class PolygonAreaHandler(webapp2.RequestHandler):
    """
    Funcion handler para consultar el area de un poligono.
    Recibe el resultado de un request http generado en el front-end,
    luego extrae las variables necesarias y ejecuta la funcion GetPolygonArea

    Parametros:
        webapp2.RequestHandler: Resultado de un request http

    Variables extraidas desde webapp2.RequestHandler:
        polygon_id: poligono generado por el usuario en el front end
        mode_id: indica el tipo de poligono (auto, manual).
        shapefile_id: id de shapefile seleccionado por el usuario.

    Devuelve:
        Area de poligono seleccionado
    """

    def get(self, path=''):
        try:
            # Obtener parametros del request http.
            polygon_id = self.request.get('polygon_id')
            mode_id = self.request.get('mode_id')
            shapefile_id = self.request.get('shapefile_id')
            # Llamar a funcion que calcula area de poligonos.
            content = GetPolygonArea(polygon_id, mode_id, shapefile_id)
            # Enviar informacion con area de poligono.
            self.response.headers['Content-Type'] = 'application/json'
            self.response.headers['Access-Control-Allow-Origin'] = '*'
            self.response.out.write(content)
        except:
            # En caso de error, presentar mensaje.
            content = json.dumps(
                {'error': 'System error (PolygonAreaHandler)'})
            self.response.headers['Content-Type'] = 'application/json'
            self.response.headers['Access-Control-Allow-Origin'] = '*'
            self.response.out.write(content)


# Definicion de parametros para JINJA.
jinjaLoaderPath = os.path.dirname(os.path.dirname(os.path.dirname(__file__)))
JINJA2_ENVIRONMENT = jinja2.Environment(
    loader=jinja2.FileSystemLoader(jinjaLoaderPath),
    autoescape=True,
    extensions=['jinja2.ext.autoescape'])
