# coding=utf-8
import json
import os
import config
import ee
from google.appengine.api import memcache
from google.appengine.api import urlfetch
urlfetch.set_default_fetch_deadline(300)
import socket
socket.setdefaulttimeout(300)


###############################################################################
#                                   Helpers                                   #
###############################################################################

###############################################################################
# GRAFICOS DE PIE (PIECHARTS)
###############################################################################
def GetPieChart(polygon_id, band_id, mode_id, dataset_id,
                agg_id, type_id, eo_startdate, eo_enddate, shapefile_id=None):
    """
    Funcion para generar graficos de pie (piechart).

    Parametros:
        polygon_id : si mode_id == auto, entonces es el id del shape escogido,
        de lo contrario corresponde al  poligono generado por el usuario.
        band_id: id de la capa escogida dentro del dataset_id.
        mode_id: tipo de poligono (auto, manual).
        dataset_id: id del dataset escogido.
        agg_id: tipo de estadistica (minimo, maximo, promedio) a considerar.
        type_id: tipo de dato de EarthEngine (Image o ImageCollection).
        eo_startdate: fecha inicial para considerar en grafico.
        eo_enddate: fecha final para considerar en grafico.
        shapefile_id: id de shapefile escogida por el usuario.

    Devuelve:
        JSON con valores para graficar graficos de pie
    """

    # Para actualizar el grafico automaticamente al cambiar las fechas,
    # guardamos los parametros en cache.
    key = 'last_values'
    if polygon_id == 'last':
        mem = memcache.get(key)
        if mem is None:
            return json.dumps({'error': ''})
        last_value = json.loads(mem)
        polygon_id = last_value.get("polygon_id")
        mode_id = last_value.get("mode_id")
        shapefile_id = last_value.get("shapefile_id")
    else:
        last_value = {}
        last_value["polygon_id"] = polygon_id
        last_value["mode_id"] = mode_id
        last_value["shapefile_id"] = shapefile_id
        memcache.set(key, json.dumps(last_value), MEMCACHE_EXPIRATION)
    # Verificar tipo de poligono:
    # auto: poligono de un shapefileHandler
    # else: poligono dibujado por usuario
    if mode_id == 'auto':
        feature = GetFeature(polygon_id, shapefile_id)
    else:
        featureStr = strToFeature(polygon_id, mode_id)
        feature = ee.Feature(json.loads(featureStr))
    # Verificar que tipo de imagen se recibe:
    # Image: imagen unica con solo un valor temporal.
    # ImageCollection: coleccion de imagenes con valores temporales
    # definidos por eo_startdate y eo_enddate.
    if type_id == 'Image':
        dataset = ee.Image(dataset_id).select(band_id)
        if band_id == 'max_extent':
            dataset = ee.Image(dataset_id).select(band_id).unmask(0)
    elif type_id == 'ImageCollection':
        dataset = ee.ImageCollection(dataset_id).filterDate(
            eo_startdate, eo_enddate).select(band_id)
        # Si la imagen es una coleccion, revisar que tipo de estadistica
        # temporal se quiere representar.
        if agg_id == 'minimo':
            dataset = dataset.min()
        elif agg_id == 'maximo':
            dataset = dataset.max()
        elif agg_id == 'promedio':
            dataset = dataset.mean()
    elif type_id == 'ICNoDate':
        dataset = ee.ImageCollection(dataset_id).select(band_id).mosaic()

    # Calcular piechart para las clases presentes dentro del area seleccionada.
    image_with_transition_class = ee.Image.pixelArea().addBands(dataset)
    reduction = image_with_transition_class.reduceRegion(
        bestEffort=True,
        reducer=ee.Reducer.sum().group(groupField=1,
                                       groupName='transition_class_value'),
        geometry=feature.geometry(),
        scale=30,
        maxPixels=MAX_PIXELS)
    values = ee.List(reduction.get('groups')).getInfo()
    feature = ee.Feature(None, {
        'values': values
    })
    # Devolver informacion para graficar el piechart.
    return json.dumps(feature.getInfo())


###########################################################################
# HISTOGRAMAS
###########################################################################
def GetHistogram(polygon_id, band_id, dataset_id, mode_id,
                 min_id, max_id, shapefile_id=None):
    """
    Funcion para generar histogramas.

    Parametros:
        polygon_id : si mode_id == auto, entonces es el id del shape escogido,
        de lo contrario corresponde al  poligono generado por el usuario.
        band_id: id de la capa escogida dentro del dataset_id.
        dataset_id: id del dataset escogido.
        mode_id: tipo de poligono (auto, manual).
        min_id: el rango minimo de valores para graficar.
        max_id: el rango maximo de valores para graficar.
        shapefile_id: id de shapefile escogida por el usuario.

    Devuelve:
        JSON con valores para graficar histograma.
    """

    # Para actualizar el grafico automaticamente al cambiar las fechas,
    # guardamos los parametros en cache.
    key = 'last_values'
    if polygon_id == 'last':
        mem = memcache.get(key)
        if mem is None:
            return json.dumps({'error': ''})
        last_value = json.loads(mem)
        # print(last_value)
        polygon_id = last_value.get("polygon_id")
        mode_id = last_value.get("mode_id")
        shapefile_id = last_value.get("shapefile_id")
    else:
        last_value = {}
        last_value["polygon_id"] = polygon_id
        last_value["mode_id"] = mode_id
        last_value["shapefile_id"] = shapefile_id
        memcache.set(key, json.dumps(last_value), MEMCACHE_EXPIRATION)
    try:
        # Verificar tipo de poligono:
        # auto: poligono de un shapefileHandler
        # else: poligono dibujado por usuario
        if mode_id == 'auto':
            feature = GetFeature(polygon_id, shapefile_id)
        else:
            featureStr = strToFeature(polygon_id, mode_id)
            feature = ee.Feature(json.loads(featureStr))

        # Seleccionar banda de interes dentro del dataset
        collection = ee.Image(dataset_id).select(band_id)

        # Definir funcion y calcular histograma para area seleccionada.
        def ComputeHistogram(img):
            reduction = img.reduceRegion(
                bestEffort=True,
                reducer=ee.Reducer.histogram(),
                geometry=feature.geometry(),
                scale=30,
                maxPixels=MAX_PIXELS
            )
            values = ee.Dictionary(reduction.get(band_id))
            classes_val = values.get('bucketMeans')
            count_val = values.get('histogram')
            return ee.Feature(None, {
                'classes': classes_val,
                'values': count_val
            })
        chart_data = ComputeHistogram(collection).getInfo()
        # Devolver informacion para graficar histograma.
        return json.dumps(chart_data)
    except ee.EEException:
        # En caso de error, devolver mensaje informativo.
        chart_data = json.dumps(
            {'error': 'El area seleccionada no contiene informacion.'})
        return chart_data


############################################################################
# SERIES TEMPORALES
############################################################################
def GetPolygon_TimeSeries(polygon_id, band_id, scale_id, dataset_id, mode_id,
                          eo_startdate, eo_enddate, shapefile_id=None):
    """
    Funcion para generar graficos de serie temporal (timeseries).
    Dependiendo del tipo de dato de entrada (producto o indice remoto),
    se llama a una subfuncion correspondiente.

    Parametros:
        polygon_id : si mode_id == auto, entonces es el id del shape escogido,
        de lo contrario corresponde al poligono generado por el usuario.
        band_id: id de la capa escogida dentro del dataset_id.
        scale_id: escala geografica de EarthEngine utilizada para analisis.
        dataset_id: id del dataset escogido.
        mode_id: tipo de poligono (auto, manual).
        eo_startdate: fecha inicial para considerar en grafico.
        eo_enddate: fecha final para considerar en grafico.
        shapefile_id: id de shapefile escogida por el usuario.

    Devuelve:
        JSON con valores para graficar serie temporal.
    """

    # Para actualizar el grafico automaticamente al cambiar las fechas,
    # guardamos los parametros en cache.
    key = 'last_values'
    if polygon_id == 'last':
        mem = memcache.get(key)
        if mem is None:
            return json.dumps({'error': ''})
        last_value = json.loads(mem)
        # print(last_value)
        polygon_id = last_value.get("polygon_id")
        mode_id = last_value.get("mode_id")
        shapefile_id = last_value.get("shapefile_id")
    else:
        last_value = {}
        last_value["polygon_id"] = polygon_id
        last_value["mode_id"] = mode_id
        last_value["shapefile_id"] = shapefile_id
        memcache.set(key, json.dumps(last_value), MEMCACHE_EXPIRATION)

    # Definir diccionario vacio para guardar valores de series temporales.
    details = {}
    try:
        # Utilizamos diferentes metodos para generar las series temporales
        # dependiendo del tipo de dato de entrada (JRC, MODIS, Indices).
        if dataset_id == 'JRC/GSW1_0/YearlyHistory':
            # Para dataset JRC YearlyHistory
            # Contar pixeles de agua
            pixCount = ComputePolygon_TimeSeries(
                polygon_id, band_id, scale_id, dataset_id, mode_id, 1,
                eo_startdate, eo_enddate, shapefile_id)
            # Si estamos graficando un pixel:
            # Revisar si hay deteccion de agua en el pixel seleccionado.
            if mode_id == 'Point':
                # Definir valores a graficar
                pixValid = 0
                pixValue = pixCount
                # Definir nombre de variables a graficar para cada eje.
                y1_name = 'Deteccion de Agua'
                y2_name = ''
            else:
                # Si estamos graficando un poligono:
                # Multiplicar numero de pixeles por area en Km
                pixAreaKm = 0.0009
                pixValue = [[pixCount[i][0], pixCount[i][1] * pixAreaKm]
                            for i in range(len(pixCount))]
                # Contar numero de pixeles totales.
                pixTot = ComputePolygon_TimeSeries(
                    polygon_id, band_id, scale_id, dataset_id, mode_id, -1,
                    eo_startdate, eo_enddate, shapefile_id)
                # Contar numero de pixeles validos.
                pixOk = ComputePolygon_TimeSeries(
                    polygon_id, band_id, scale_id, dataset_id, mode_id, 0,
                    eo_startdate, eo_enddate, shapefile_id)
                # Calcular proporcion de pixeles validos sobre el total.
                pixValid = [[pixOk[i][0], int((pixOk[i][1] / pixTot[i][1]) * 100)]
                            for i in range(len(pixOk))]
                # Definir nombre de variables a graficar para cada eje.
                y1_name = 'Superficie de Agua [km2]'
                y2_name = 'Pixeles Validos [%]'
        elif dataset_id == 'MODIS/006/MOD44W':
            # Para dataset MOD44W
            # Contar pixeles de agua
            pixCount = ComputePolygon_TimeSeries(
                polygon_id, band_id, scale_id, dataset_id, mode_id, 0,
                eo_startdate, eo_enddate, shapefile_id)
            # Si estamos graficando un pixel:
            # Revisar si hay deteccion de agua en el pixel seleccionado.
            if mode_id == 'Point':
                # Definir valores a graficar
                pixValid = 0
                pixValue = pixCount
                # Definir nombre de variables a graficar para cada eje.
                y1_name = 'Deteccion de Agua'
                y2_name = ''
            else:
                # Si estamos graficando un poligono:
                # Multiplicar numero de pixeles por area en Km
                pixAreaKm = 0.0625
                pixValue = [[pixCount[i][0], pixCount[i][1] * pixAreaKm]
                            for i in range(len(pixCount))]
                # El producto MOD44W solo marca un pixel como invalido si
                # este se encuentra fuera del area de analisis del producto.
                # Aunque Colombia se encuentra completamente dentro del area
                # de analisis del producto MOD44W, preferimos no entregar
                # informacion respecto a pixeles validos en el grafico.
                pixValid = 0
                # Definir nombre de variables a graficar para cada eje.
                y1_name = 'Superficie de Agua [km2]'
                y2_name = ''
        else:
            # Para indices remotos
            pixCount = ComputePolygon_TimeSeries_Index(
                polygon_id, band_id, scale_id, dataset_id, mode_id,
                eo_startdate, eo_enddate, shapefile_id)
            # Definir valores a graficar.
            pixValid = 0
            pixValue = pixCount
            # Definir nombre de variables a graficar para cada eje.
            y1_name = band_id.upper()
            y2_name = ''
        # Agregar valores obtenidos a los detalles del grafico
        details['y1_name'] = y1_name
        details['y2_name'] = y2_name
        details['ts_value'] = pixValue
        details['ts_valid'] = pixValid
    except ee.EEException as e:
        # Si se genera un error, guardar informacion del error.
        details['error'] = str(e)
    # Devolver la informacion para graficar la serie temporal.
    return json.dumps(details)


def ComputePolygon_TimeSeries(polygon_id, band_id, scale_id, dataset_id,
                              mode_id, value_id, eo_startdate, eo_enddate,
                              shapefile_id=None):
    """
    Funcion para calcular graficos de serie temporal de productos.

    Parametros:
        polygon_id : si mode_id == auto, entonces es el id del shape escogido,
        de lo contrario corresponde al  poligono generado por el usuario.
        band_id: id de la capa escogida dentro del dataset_id.
        scale_id: escala geografica de EarthEngine utilizada para analisis.
        dataset_id: id del dataset escogido.
        mode_id: tipo de poligono (auto, manual).
        value_id: valor de pixel que se usa como referencia para seleccionar
                  los valores de la serie temporal.
        eo_startdate: fecha inicial para considerar en grafico.
        eo_enddate: fecha final para considerar en grafico.
        shapefile_id: id de shapefile escogida por el usuario.

    Devuelve:
        Valores para graficar serie temporal.
    """
    # Definir parametro para ordenar series temporales:
    # 'system:time_start' corresponde a la representacion
    # interna de fechas dentro de EarthEngine.
    sortVal = 'system:time_start'

    # Definicion de funcion para calcular suma de valores en region.
    def ComputeSum(img):
        """
        Funcion para calcular suma de valores en una region.
        Esta funcion se puede ejecutar sobre una coleccion con el metodo .map()

        Parametros:
            img: imagen sobre la que se ejecutara la funcion.

        Las variables no definidas dentro de los parametros de la funcion
        se heredan desde la funcion superior (ComputePolygon_TimeSeries)
        """
        # Primero seleccionamos los pixeles correspondientes a cada clase.
        imgClass = img.gt(value_id)
        reduction = imgClass.reduceRegion(
            bestEffort=True,
            reducer=ee.Reducer.sum(),
            geometry=feature.geometry(),
            scale=int(scale_id),
            maxPixels=MAX_PIXELS)
        return ee.Feature(None, {
            band_id: reduction.get(band_id),
            sortVal: img.get(sortVal)})
    # Verificar tipo de poligono:
    # auto: poligono de un shapefileHandler
    # else: poligono dibujado por usuario
    if mode_id == 'auto':
        feature = GetFeature(polygon_id, shapefile_id)
    else:
        featureStr = strToFeature(polygon_id, mode_id)
        feature = ee.Feature(json.loads(featureStr))
    # Seleccionar banda de dataset y ordenar por fecha.
    collection = ee.ImageCollection(dataset_id).select(band_id).sort(sortVal)
    # Filtrar por fechas y region seleccionada
    collection = collection.filterBounds(
        feature.geometry()).filterDate(eo_startdate, eo_enddate)
    # ejecutar funcion ComputeSum a toda la coleccion
    chart_data = collection.map(ComputeSum).getInfo()

    # Obtener resultados.
    def ExtractSum(feature):
        return [feature['properties'][sortVal], feature['properties'][band_id]]
    resultSum = map(ExtractSum, chart_data['features'])
    return resultSum


def ComputePolygon_TimeSeries_Index(polygon_id, band_id, scale_id, dataset_id,
                                    mode_id, eo_startdate, eo_enddate,
                                    shapefile_id=None):
    """
    Funcion para calcular graficos de serie temporal de indices remotos.

    Parametros:
        polygon_id : si mode_id == auto, entonces es el id del shape escogido,
        de lo contrario corresponde al  poligono generado por el usuario.
        band_id: id de la capa escogida dentro del dataset_id.
        scale_id: escala geografica de EarthEngine utilizada para analisis.
        dataset_id: id del dataset escogido.
        mode_id: tipo de poligono (auto, manual).
        eo_startdate: fecha inicial para considerar en grafico.
        eo_enddate: fecha final para considerar en grafico.
        shapefile_id: id de shapefile escogida por el usuario.

    Devuelve:
        Valores para graficar serie temporal.
    """

    # Definir parametro para ordenar series temporales:
    # 'system:time_start' corresponde a la representacion
    # interna de fechas dentro de EarthEngine.
    sortVal = 'system:time_start'

    # Definicion de funcion para calcular promedio de valores en region.
    def ComputeMean(img):
        """
        Funcion para calcular promedio de valores en una region.
        Esta funcion se puede ejecutar sobre una coleccion con el metodo .map()

        Parametros:
            img: imagen sobre la que se ejecutara la funcion.

        Las variables no definidas dentro de los parametros de la funcion
        se heredan desde la funcion superior (ComputePolygon_TimeSeries_Index)
        """
        reduction = img.reduceRegion(
            bestEffort=True,
            reducer=ee.Reducer.mean(),
            geometry=feature.geometry(),
            scale=int(scale_id),
            maxPixels=MAX_PIXELS)
        return ee.Feature(None, {
            band_id: reduction.get(band_id),
            sortVal: img.get(sortVal)})

    # Verificar tipo de poligono:
    # auto: poligono de un shapefileHandler
    # else: poligono dibujado por usuario
    if mode_id == 'auto':
        feature = GetFeature(polygon_id, shapefile_id)
    else:
        featureStr = strToFeature(polygon_id, mode_id)
        feature = ee.Feature(json.loads(featureStr))
    # Seleccionar banda de dataset y ordenar por fecha.
    collection = ee.ImageCollection(dataset_id).sort(sortVal)
    # Filtrar por fechas y region seleccionada
    collection = collection.filterBounds(
        feature.geometry()).filterDate(eo_startdate, eo_enddate)
    # Calcular indice con funcion calculateAggrIndex (coleccion)
    collection = calculateAggrIndex(band_id, collection)
    # ejecutar funcion ComputeMean a toda la coleccion
    chart_data = collection.map(ComputeMean).getInfo()

    # Obtener resultados.
    def ExtractMean(feature):
        return [feature['properties'][sortVal], feature['properties'][band_id]]
    resultMean = map(ExtractMean, chart_data['features'])
    return resultMean


def GetPixelValue(polygon_id, band_id, scale_id, mode_id,
                  cachekey):
    """
    Funcion para obtener valor de punto seleccionado al hacer click

    Parametros:
        polygon_id: poligono generado por el usuario en el front end
        band_id: banda definida por el usuario en el front end
        scale_id: escala geografica de EarthEngine utilizada para analisis.
        mode_id: indica el tipo de poligono/punto (manual en este caso)
        cachekey: identificador en cache

    Devuelve:
        Informacion de valor de pixel
    """
    # Transformar el punto seleccionado en una geometria de Earth Engine.
    featureStr = strToFeature(polygon_id, mode_id)
    feature = ee.Feature(json.loads(featureStr))
    # Obtener valor del punto para el dataset desplegado en el mapa.
    data = memcache.get(cachekey).reduceRegion(
        ee.Reducer.first(), feature.geometry(), int(scale_id)).get(band_id)
    # Devolver valor de punto
    pixelValue = data.getInfo()
    # Si el valor del punto es tipo float, redondear a 2 cifras significativas.
    if type(pixelValue) == float:
        pixelValue = '{:.2g}'.format(pixelValue)
    pixInfo = json.dumps(pixelValue)
    return pixInfo


def GetPolygonArea(polygon_id, mode_id, shapefile_id=None):
    """
    Funcion para obtener valor de punto seleccionado al hacer click

    Parametros:
        polygon_id: poligono generado por el usuario en el front end.
        mode_id: indica el tipo de poligono (auto, manual).
        shapefile_id: id de shapefile escogido por el usuario.

    Devuelve:
        Informacion sobre area de poligono
    """
    # Constante para transformar m2 a km2
    m2tokm2 = 1E-6
    # Verificar tipo de poligono:
    # auto: poligono de un shapefileHandler
    # else: poligono dibujado por usuario
    if mode_id == 'auto':
        feature = GetFeature(polygon_id, shapefile_id)
    else:
        featureStr = strToFeature(polygon_id, mode_id)
        feature = ee.Feature(json.loads(featureStr))
    # Transformar poligono en geometria de Earth Engine
    geometry = ee.Geometry(feature.geometry().getInfo(), None, False)
    # Calculo de Area con metodo .area(), el valor 10 corresponde
    # al error maximo esperable
    area = geometry.area(10).getInfo()
    # Si el area es mayor a 1km, transformar resultado a numero entero
    if area < 1 / m2tokm2:
        area = area * m2tokm2
        area = str(round(area, 4)) + 'km2'
    else:
        area = long(area * m2tokm2)
        area = str(area) + ' km2'
    return json.dumps(area)


###########################################################################
# CAPAS DE MAPA
###########################################################################
def GetGFW_EOData_Id(eo_dataset, eo_band, eo_min, eo_max, eo_palette,
                     masking_toggle, cachekey):
    """
    Funcion para desplegar datos de GlobalForestWatch sobre mapa base.

    Parametros:
        eo_dataset: nombre de dataset de GlobalForestWatch
        eo_band: banda a desplegar sobre basemap
        eo_min: valor minimo para paleta de colores
        eo_max: valor maximo para paleta de colores
        eo_palette: paleta de colores a usar
        masking_toggle: opcion para mascara de capa
        cachekey: identificador en cache para mapa actual

    Devuelve:
        Informacion necesaria para desplegar capa sobre mapa base.
    """

    # Obtener limites de colombia desde dataset disponible en Earth Engine.
    AREA_ESTUDIO = ee.FeatureCollection(
        'USDOS/LSIB_SIMPLE/2017').filter(
        ee.Filter.eq('country_na', 'Colombia'))

    # Verificar la banda seleccionada
    if eo_band == 'combined':
        # Si elegimos el dataset combinado, se genera una capa que muestra
        # la cobertura de bosque, las areas de ganancia, y las de perdida.
        treecover = ee.Image(eo_dataset).select('treecover2000')
        treecover = treecover.gt(50).eq(1).updateMask(treecover)
        loss = ee.Image(eo_dataset).select('loss')
        loss = loss.eq(1).updateMask(treecover)
        gain = ee.Image(eo_dataset).select('gain')
        gain = gain.eq(1).multiply(2).updateMask(treecover)
        image = treecover.add(loss).add(gain).rename(
            ['combined']).select('combined')
    else:
        # Si elegimos alguna otra capa, se muestra esa capa.
        image = ee.Image(eo_dataset).select(eo_band)
        image = image.updateMask(image)

    # Acotar imagen a area de estudio
    image = image.clip(AREA_ESTUDIO)

    # Guardar informacion del mapa actual en cache
    memcache.set(cachekey, image, MEMCACHE_EXPIRATION)

    # Si se elige, activar mascara de capa actual.
    if masking_toggle:
        image = image.unmask(0)

    # Obtener id de mapa para capa generada
    mapId = image.getMapId(
        {'min': eo_min, 'max': eo_max, 'palette': eo_palette})
    # Devolver informacion de capa generada con detalles del mapa actual.
    return mapId


def GetSima_EOData_Id(eo_product, eo_type, eo_dataset, eo_min, eo_max,
                      eo_palette, eo_startdate, eo_enddate, aggregation_type,
                      masking_toggle, cachekey):
    """
    Funcion para desplegar datos sobre mapa base.

    Parametros:
        eo_product: producto a desplegar sobre mapa.
        eo_type: tipo de producto (Image, ImageCollection)
        eo_dataset: nombre de dataset a mostrar.
        eo_min: valor minimo para paleta de colores
        eo_max: valor maximo para paleta de colores
        eo_palette: paleta de colores a usar
        eo_startdate: fecha inicial para ImageCollection
        eo_enddate: fecha final para ImageCollection
        aggregation_type: tipo de valor a representar (min, max, etc.)
        masking_toggle: opcion para mascara de capa
        cachekey: identificador en cache para mapa actual

    Devuelve:
        Informacion necesaria para desplegar capa sobre mapa base.
    """
    # Obtener limites de colombia desde dataset disponible en Earth Engine.
    AREA_ESTUDIO = ee.FeatureCollection(
        'USDOS/LSIB_SIMPLE/2017').filter(
        ee.Filter.eq('country_na', 'Colombia'))

    # Verificar tipo de dato
    if eo_type == 'Image':
        # Si es una imagen, revisar si la opcion para enmascarar esta activada.
        if masking_toggle:
            image = ee.Image(eo_dataset).select(eo_product).unmask(0)
        else:
            image = ee.Image(eo_dataset).select(eo_product)
    elif eo_type == 'ImageCollection':
        # Si es una coleccion, seleccionar que mostrar.
        if aggregation_type == 'single':
            # Para valor unico, elegir la primera imagen del periodo definido.
            image = ee.Image(ee.ImageCollection(eo_dataset).filter(
                ee.Filter.date(eo_startdate, eo_enddate)).filterBounds(
                AREA_ESTUDIO).select(eo_product).first())
            imCollection = image
        elif aggregation_type == 'promedio':
            # Para promedio, calcular promedio del periodo definido.
            imCollection = ee.ImageCollection(eo_dataset).filter(
                ee.Filter.date(eo_startdate, eo_enddate)).filterBounds(
                AREA_ESTUDIO).select(eo_product)
            image = imCollection.mean()
        elif aggregation_type == 'maximo':
            # Para maximo, calcular valor maximo del periodo definido.
            imCollection = ee.ImageCollection(eo_dataset).filter(
                ee.Filter.date(eo_startdate, eo_enddate)).filterBounds(
                AREA_ESTUDIO).select(eo_product)
            image = imCollection.max()
        elif aggregation_type == 'minimo':
            # Para minimo, calcular valor minimo del periodo definido.
            imCollection = ee.ImageCollection(eo_dataset).filter(
                ee.Filter.date(eo_startdate, eo_enddate)).filterBounds(
                AREA_ESTUDIO).select(eo_product)
            image = imCollection.min()
        elif aggregation_type == 'diferencia':
            # Para diferencia, calcular diferencia entre primer y ultimo valor
            # dentro del periodo definido.
            startYear = eo_startdate.split("-")[0]
            endYear = eo_enddate.split("-")[0]
            y1s = startYear + '-01-01'
            y1e = startYear + '-12-31'
            y2s = endYear + '-01-01'
            y2e = endYear + '-12-31'
            img1 = ee.ImageCollection(eo_dataset).filter(
                ee.Filter.date(y1s, y1e)).filterBounds(
                AREA_ESTUDIO).select(eo_product).first()
            img2 = ee.ImageCollection(eo_dataset).filter(
                ee.Filter.date(y2s, y2e)).filterBounds(
                AREA_ESTUDIO).select(eo_product).first()
            image = ee.Image(img2).select(eo_product).subtract(
                ee.Image(img1).select(eo_product))
        # Revisar si la opcion para enmascarar esta activada
        if masking_toggle:
            image = image.unmask(0)
        else:
            image = image.updateMask(image)
    elif eo_type == 'ICNoDate':
        image = ee.ImageCollection(eo_dataset).filterBounds(
            AREA_ESTUDIO).select(eo_product).mosaic()

    # Acotar imagen a area de estudio
    image = image.clip(AREA_ESTUDIO)

    # Guardar informacion del mapa actual en cache
    memcache.set(cachekey, image, MEMCACHE_EXPIRATION)
    if eo_type == 'ImageCollection':
        memcache.set(cachekey + 'aggr', imCollection, MEMCACHE_EXPIRATION)

    # Obtener id de mapa para capa generada
    mapId = image.getMapId(
        {'min': eo_min, 'max': eo_max, 'palette': eo_palette})

    # Devolver informacion de capa generada con detalles del mapa actual.
    return mapId


###########################################################################
# INDICES REMOTOS
###########################################################################

def computeNDWI(image):
    """
    Funcion para calculo de indice NDWI.
    Esta funcion se puede ejecutar sobre una coleccion con el metodo .map()

    Parametros:
        image: imagen sobre la que se calculara el indice.

    Devuelve:
        Imagen correspondiente al indice calculado.

    Referencia:
        Mcfeeters S. (1996)
        The use of the normalized difference water index in the
        delineation of open water features.
        Int. J. Remote Sens, 17:1425–1432
        doi: 10.1080/01431169608948714
    """
    # Como este indice es una relacion entre bandas, no es necesario
    # re-escalar el valor de la reflectancia.
    ndwi = image.normalizedDifference(['B3', 'B5']).rename(['ndwi'])
    ndwi = image.addBands(ndwi).select(['ndwi'])
    return ndwi


def computeMNDWI(image):
    """
    Funcion para calculo de indice MNDWI.
    Esta funcion se puede ejecutar sobre una coleccion con el metodo .map()

    Parametros:
        image: imagen sobre la que se calculara el indice.

    Devuelve:
        Imagen correspondiente al indice calculado.

    Referencia:
        Xu H. (2006)
        Modification of normalised difference water index (NDWI)
        to enhance open water features in remotely sensed imagery.
        Int. J. Remote Sens, 27:3025–3033
        doi: 10.1080/01431160600589179
    """
    # Como este indice es una relacion entre bandas, no es necesario
    # re-escalar el valor de la reflectancia.
    mndwi = image.normalizedDifference(['B3', 'B6']).rename(['mndwi'])
    mndwi = image.addBands(mndwi).mask(mndwi).select(['mndwi'])
    return mndwi


def computeWI2015(image):
    """
    Funcion para calculo de indice WI2015.
    Esta funcion se puede ejecutar sobre una coleccion con el metodo .map()

    Parametros:
        image: imagen sobre la que se calculara el indice.

    Devuelve:
        Imagen correspondiente al indice calculado.

    Referencia:
        Fisher, A., Flood, N., and Danaher, T. (2016)
        Comparing Landsat water index methods for automated water
        classification in eastern Australia.
        Remote Sensing of Environment, 175, 167–182
        doi:10.1016/j.rse.2015.12.055
    """
    # Nota:
    # La reflectancia viene escalada entre 0 y 10000, por lo que necesitamos
    # aplicar el factor de escala de 0.0001 para devolverla al rango 0-1.
    # Las bandas reescaladas tienen el sufijo _1 por defecto
    image = image.addBands(image.multiply(0.0001))
    wi2015 = image.addBands(
        image.select('B3_1').multiply(ee.Image.constant(171)).add(
        image.select('B4_1').multiply(ee.Image.constant(3))).subtract(
        image.select('B5_1').multiply(ee.Image.constant(70))).subtract(
        image.select('B6_1').multiply(ee.Image.constant(45))).subtract(
        image.select('B7_1').multiply(ee.Image.constant(71))).add(
        ee.Image.constant(1.7204)
        ).rename(['wi2015'])).select('wi2015')
    return wi2015


def computeLDAWI(image):
    """
    Funcion para calculo de indice LDAWI.
    Esta funcion se puede ejecutar sobre una coleccion con el metodo .map()

    Parametros:
        image: imagen sobre la que se calculara el indice.

    Devuelve:
        Imagen correspondiente al indice calculado.

    Referencia:
        Fisher, A. & Danaher, T. (2013)
        A water index for SPOT5 HRG satellite imagery, New South Wales,
        Australia, determined by linear discriminant analysis.
        Remote Sensing 5, 5907–5925
        doi:10.3390/rs5115907
    """
    # image.select('B3') = image.select('B3').multiply(0.0001)
    # image.select('B4') = image.select('B4').multiply(0.0001)
    # image.select('B5') = image.select('B5').multiply(0.0001)
    # image.select('B6') = image.select('B6').multiply(0.0001)
    # Nota:
    # La reflectancia viene escalada entre 0 y 10000, por lo que necesitamos
    # aplicar el factor de escala de 0.0001 para devolverla al rango 0-1.
    # Las bandas reescaladas tienen el sufijo _1 por defecto
    image = image.addBands(image.multiply(0.0001))
    # Para este indice necesitamos primero calcular el logaritmo natural
    # de las bandas originales.
    l8srLog = image.log()
    ldawi = image.addBands(
        l8srLog.select('B3_1').multiply(ee.Image.constant(-76.18)).add(
        l8srLog.select('B4_1').multiply(ee.Image.constant(-18.20))).add(
        l8srLog.select('B5_1').multiply(ee.Image.constant(-43.00))).add(
        l8srLog.select('B6_1').multiply(ee.Image.constant(96.42))).add(
        l8srLog.select('B3_1').multiply(l8srLog.select('B4_1')).multiply(ee.Image.constant(3.79))).add(
        l8srLog.select('B3_1').multiply(l8srLog.select('B5_1')).multiply(ee.Image.constant(16.28))).add(
        l8srLog.select('B3_1').multiply(l8srLog.select('B6_1')).multiply(ee.Image.constant(-6.25))).add(
        l8srLog.select('B4_1').multiply(l8srLog.select('B5_1')).multiply(ee.Image.constant(1.54))).add(
        l8srLog.select('B4_1').multiply(l8srLog.select('B6_1')).multiply(ee.Image.constant(-1.14))).add(
        l8srLog.select('B5_1').multiply(l8srLog.select('B6_1')).multiply(ee.Image.constant(-12.77))).add(
        ee.Image.constant(224.14)
        ).rename(['ldawi'])).select('ldawi')
    return ldawi


def computeAWEISW(image):
    """
    Funcion para calculo de indice AWEISW (AWEI para zonas con sombra).
    Esta funcion se puede ejecutar sobre una coleccion con el metodo .map()

    Parametros:
        image: imagen sobre la que se calculara el indice.

    Devuelve:
        Imagen correspondiente al indice calculado.

    Referencia:
        Feyisa, G. L., Meilby, H., Fensholt, R. & Proud, S. R. (2014)
        Automated Water Extraction Index:
        A new technique for surface water mapping using Landsat imagery.
        Remote Sensing of Environment. 140, 23–35
        doi:10.1016/j.rse.2013.08.029
    """
    # La reflectancia viene escalada entre 0 y 10000, por lo que necesitamos
    # aplicar el factor de escala de 0.0001 para devolverla al rango 0-1.
    # Las bandas reescaladas tienen el sufijo _1 por defecto
    image = image.addBands(image.multiply(0.0001))
    aweiSW = image.addBands(
        image.select('B2_1').add(image.select('B3_1').multiply(
            ee.Image.constant(2.5))).subtract(
        ee.Image(image.select('B5_1').add(
            image.select('B6_1'))).multiply(ee.Image.constant(1.5))).subtract(
            image.select('B7_1').multiply(ee.Image.constant(.25))).rename(
            ['awei_sw'])).select('awei_sw')
    return aweiSW


def computeAWEINSW(image):
    """
    Funcion para calculo de indice AWEINSW (AWEI para zonas sin sombra).
    Esta funcion se puede ejecutar sobre una coleccion con el metodo .map()

    Parametros:
        image: imagen sobre la que se calculara el indice.

    Devuelve:
        Imagen correspondiente al indice calculado.

    Referencia:
        Feyisa, G. L., Meilby, H., Fensholt, R. & Proud, S. R. (2014)
        Automated Water Extraction Index:
        A new technique for surface water mapping using Landsat imagery.
        Remote Sensing of Environment. 140, 23–35
        doi:10.1016/j.rse.2013.08.029
    """
    # La reflectancia viene escalada entre 0 y 10000, por lo que necesitamos
    # aplicar el factor de escala de 0.0001 para devolverla al rango 0-1.
    # Las bandas reescaladas tienen el sufijo _1 por defecto
    image = image.addBands(image.multiply(0.0001))
    aweiNSW = image.addBands(
        ee.Image(
            image.select('B3_1').subtract(
            image.select('B6_1'))).multiply(ee.Image.constant(4)).subtract(
        ee.Image(
            image.select('B5_1').multiply(ee.Image.constant(0.25))).add(
            image.select('B6_1').multiply(ee.Image.constant(2.75)))
        ).rename(['awei_nsw'])).select('awei_nsw')
    return aweiNSW


def computeTCW(image):
    """
    Funcion para calculo de indice TCW.
    Esta funcion se puede ejecutar sobre una coleccion con el metodo .map()

    Parametros:
        image: imagen sobre la que se calculara el indice.

    Devuelve:
        Imagen correspondiente al indice calculado.

    Referencias:
        Crist, E. P. (1985)
        A TM Tasseled Cap equivalent transformation for reflectance factor data.
        Remote Sensing of Environment. 17, 301–306
        doi:10.1016/0034-4257(85)90102-6

        Ali Baig, M.H., Zhang, L., Tong, S., Tong, Q. (2014)
        Derivation of a tasselled cap transformation based on Landsat 8 at-satellite reflectance.
        Remote Sensing Letters. 5:5, 423-431.
        doi: 10.1080/2150704X.2014.915434
    """
    image = image.addBands(image.multiply(0.0001))
    tcw = image.addBands(
        image.select('B2_1').multiply(ee.Image.constant(0.1511)).add(
        image.select('B3_1').multiply(ee.Image.constant(0.1973))).add(
        image.select('B4_1').multiply(ee.Image.constant(0.3283))).add(
        image.select('B5_1').multiply(ee.Image.constant(0.3407))).add(
        image.select('B6_1').multiply(ee.Image.constant(-0.7117))).add(
        image.select('B7_1').multiply(ee.Image.constant(-0.4559))
        ).rename(['tcw'])).select('tcw')
    return tcw


def calculateIndex(eo_band, image):
    """
    Funcion para seleccionar funcion correspondiente segun indice a calcular.

    Parametros:
        eo_band: nombre de indice a calcular
        image: imagen sobre la que se calculara el indice

    Devuelve:
        Imagen del indice seleccionado.
    """
    if eo_band == 'ndwi':
        l8_index = computeNDWI(image)
    elif eo_band == 'mndwi':
        l8_index = computeMNDWI(image)
    elif eo_band == 'wi2015':
        l8_index = computeWI2015(image)
    elif eo_band == 'ldawi':
        l8_index = computeLDAWI(image)
    elif eo_band == 'awei_sw':
        l8_index = computeAWEISW(image)
    elif eo_band == 'awei_nsw':
        l8_index = computeAWEINSW(image)
    elif eo_band == 'tcw':
        l8_index = computeTCW(image)
    return l8_index


def calculateAggrIndex(eo_band, collection):
    """
    Funcion para llamar a funciones de calculo de indices sobre una coleccion.

    Parametros:
        eo_band: nombre de indice a calcular
        collection: coleccion de imagenes sobre la que se calculara el indice

    Devuelve:
        Coleccion de imagenes correspondiente al indice calculado
    """
    if eo_band == 'ndwi':
        l8_index = collection.map(computeNDWI)
    elif eo_band == 'mndwi':
        l8_index = collection.map(computeMNDWI)
    elif eo_band == 'wi2015':
        l8_index = collection.map(computeWI2015)
    elif eo_band == 'ldawi':
        l8_index = collection.map(computeLDAWI)
    elif eo_band == 'awei_sw':
        l8_index = collection.map(computeAWEISW)
    elif eo_band == 'awei_nsw':
        l8_index = collection.map(computeAWEINSW)
    elif eo_band == 'tcw':
        l8_index = collection.map(computeTCW)
    return l8_index


def calculateLandsatIndexes(eo_band, start_date, end_date,
                            eo_min, eo_max, eo_palette, aggregation_type,
                            cachekey):
    """
    Funcion para seleccionar datasets, filtrar por fecha y area, y calcular
    indices remotos sobre valor temporal a representar (max, promedio, min).

    Parametros:
        eo_band: nombre del indice a calcular
        start_date: dia inicial del periodo a considerar
        end_date: dia final del periodo a considerar
        eo_min: valor minimo para paleta de colores
        eo_max: valor maximo para paleta de colores
        eo_palette: paleta de colores a usar
        aggregation_type: tipo de valor a representar (min, max, etc.)
        cachekey: identificador en cache

    Devuelve:
         # Devolver informacion de capa generada con detalles del mapa actual.
    """
    # Definir funcion para filtro de nubes L8
    def maskL8sr(image):
        """
        Funcion para actualizar mascara de imagen landsat 8 y eliminar nubes.

        Parametros:
            image: imagen sobre la que se aplicara el filtro de nubes

        Devuelve:
            Imagen con mascara actualizada
        """
        # Los bits 3 y 5 corresponden a sobra de nube y nube respectivamente.
        cloudShadowBitMask = (1 << 3)
        cloudsBitMask = (1 << 5)
        # Obtener el dataset de calidad de pixel
        qa = image.select('pixel_qa')
        # Para observaciones validas, los bits 3 y 5 deben ser 0.
        # Revisamos esto usando operaciones de bits.
        mask = qa.bitwiseAnd(cloudShadowBitMask).eq(
            0).And(qa.bitwiseAnd(cloudsBitMask).eq(0))
        # Devolvemos imagen con mascara actualizada
        return image.updateMask(mask)

    # Definir area de estudio
    AREA_ESTUDIO = ee.FeatureCollection(
        'USDOS/LSIB_SIMPLE/2017').filter(
        ee.Filter.eq('country_na', 'Colombia'))
    # Verificar tipo de valor seleccionado
    if aggregation_type == 'single':
        # Para valor unico, generar mosaico espacial.
        collection = ee.ImageCollection('LANDSAT/LC08/C01/T1_SR').filter(
            ee.Filter.date(start_date, end_date)).filterBounds(
            AREA_ESTUDIO).map(maskL8sr).mosaic()
        l8_index = calculateIndex(eo_band, collection)
    elif aggregation_type == 'promedio':
        # Para promedio, calcular promedio del periodo definido.
        collection = ee.ImageCollection('LANDSAT/LC08/C01/T1_SR').filter(
            ee.Filter.date(start_date, end_date)).filterBounds(
            AREA_ESTUDIO).map(maskL8sr)
        collection = calculateAggrIndex(eo_band, collection)
        l8_index = collection.mean()
    elif aggregation_type == 'maximo':
        # Para maximo, calcular valor maximo del periodo definido.
        collection = ee.ImageCollection('LANDSAT/LC08/C01/T1_SR').filter(
            ee.Filter.date(start_date, end_date)).filterBounds(
            AREA_ESTUDIO).map(maskL8sr)
        collection = calculateAggrIndex(eo_band, collection)
        l8_index = collection.max()
    elif aggregation_type == 'minimo':
        # Para minimo, calcular valor minimo del periodo definido.
        collection = ee.ImageCollection('LANDSAT/LC08/C01/T1_SR').filter(
            ee.Filter.date(start_date, end_date)).filterBounds(
            AREA_ESTUDIO).map(maskL8sr)
        collection = calculateAggrIndex(eo_band, collection)
        l8_index = collection.min()

    # Acotar imagen a area de estudio
    image = l8_index.clip(AREA_ESTUDIO)

    # Guardar informacion de coleccion e indices en cache
    memcache.set(cachekey + 'aggr', collection, MEMCACHE_EXPIRATION)
    memcache.set(cachekey, image, MEMCACHE_EXPIRATION)

    # Devolver informacion de capa generada con detalles del mapa actual.
    mapId = image.getMapId(
        {'min': eo_min, 'max': eo_max, 'palette': eo_palette})
    return mapId


##############################################################################
# CUSTOM ASSETS
##############################################################################

def getCustomAssetProperties(asset_id):
    """
    Devuelve propiedades de asset custom para mostrarlas en front-end.
    Si las propiedades no estan definidas o el dataset no es accesible
    se devuelve un diccionario de propiedades vacio.
    Parametros:
        asset_id: id del asset escogido.
    Devuelve:
        properties: propiedades asset seleccionado
    """
    try:
        assetInfo = ee.data.send_('/info', {'id': asset_id}, 'GET')
        properties = assetInfo['properties']
    except:
        properties = {'title': '', 'desc': '', 'classes': '', 'source': ''}
    return properties


def getCustomAssets():
    """
    Funcion para obtener lista de archivos custom en carpeta remota <ROOT_ID>
    y sus propiedades asociadas (titulo, descripcion, etc...).

    Devuelve:
        Lista de archivos
    """
    assets_list = []
    try:
        # Enviar request http a Earth Engine
        folder_list = ee.data.send_('/list', {'id': ROOT_ID}, 'GET')
        # Generar lista con informacion de archivos
        for element in folder_list:
            asset_id = str(element['id'])
            asset_name = asset_id
            asset_props = getCustomAssetProperties(asset_id)
            if "title" in asset_props.keys():
                if (asset_props["title"] != ''):
                    asset_name = asset_props["title"]
            # asset_name = asset_name[len(asset_name) - 1]
            assets_list.append({
                'id': asset_id,
                'name': asset_name,
                'props': asset_props})
        folder_data = json.dumps({'resources': assets_list})
    except ee.EEException:
        # En caso de error, generar mensaje
        folder_data = json.dumps(
            {'error': 'La carpeta seleccionada no tiene archivos. '})
    # Devolver lista de archivos disponibles
    return folder_data


def getShapefileAssets():
    """
    Funcion para obtener lista de archivos en carpeta local www/shapefiles

    Devuelve:
        Lista de archivos
    """
    try:
        # Agregar archivos a lista
        folder_data = json.dumps({'resources': SHAPEFILE_DATA}, sort_keys=True)
    except ee.EEException:
        # En caso de error, generar mensaje.
        folder_data = json.dumps(
            {'error': 'Error al leer los shapefiles disponibles. '})
    # Devolver lista de archivos disponibles
    return folder_data


def visualizeAsset(eo_dataset, cachekey):
    """
    Funcion para delimitar una imagen de Earth Engine al area de Colombia

    Parametros:
        eo_dataset: Nombre dataset (imagen)
        cachekey: identificador en cache
    """
    # Obtener limites de colombia desde dataset disponible en Earth Engine.
    AREA_ESTUDIO = ee.FeatureCollection(
        'USDOS/LSIB_SIMPLE/2017').filter(
        ee.Filter.eq('country_na', 'Colombia'))
    # Leer dataset y acotar al area de Colombia
    image = ee.Image(eo_dataset).clip(AREA_ESTUDIO)
    # Guardar informacion de imagen en cache
    memcache.set(cachekey, image, MEMCACHE_EXPIRATION)
    # Obtener id de mapa para imagen acotada
    mapId = image.getMapId()
    # Devolver informacion de imagen con detalles de mapa actual
    return mapId


def strToFeature(input_string, mode_id):
    """
    Funcion para transformar coordenadas en formato string obtenidas desde
    el front-end en el formato requerido por las geometrias Earth Engine.

    Parametros:
        input_string: string obtenido desde frontend
        mode_id: tipo de geometria ('Point', 'Polygon')

    Devuelve:
        Informacion para generar geometria de Earth Engine
    """
    # Formatear string de entrada
    # Separar elementos y generar lista
    roi = input_string.replace(' ', '').split(',')
    roi_length = len(roi)
    # Definir formato usado por geometrias de Earth Engine
    output_f = ('{ "type": "Feature",' +
                '"properties": { "ADM1_NAME": "mode_id",' +
                '"ADM0_NAME": "Colombia" }, ' +
                '"geometry": { "type": "mode_id", "coordinates":  ')
    # Generar informacion de geometria segun tipo
    if mode_id == 'Point':
        roi = [float(r) for r in reversed(roi)]
    elif roi_length < 6:
        return None
    elif roi_length % 2 != 0:
        return None
    else:
        roi = [float(r) for r in reversed(roi)]
        roi = [[roi[x:x + 2] for x in range(0, len(roi), 2)]]
    geom = str(output_f + str(roi) + ' } }').replace('mode_id', mode_id)
    # Devolver informacion para generar geometria
    return geom


def GetSimaMapId():
    """
    Funcion para obtener informacion del mapa sobre Colombia

    Devuelve:
        Id del mapa actual
    """
    # Definir area de estudio (Colombia)
    AREA_ESTUDIO = ee.FeatureCollection(
        'USDOS/LSIB_SIMPLE/2017').filter(
        ee.Filter.eq('country_na', 'Colombia'))
    # Devolver informacion de mapa
    mapId = AREA_ESTUDIO.getMapId()
    return mapId


def GetFeature(polygon_id, shapefile_id):
    """
    Funcion para generar feature de Earth Engine a partir de poligono json

    Parametros:
        polygon_id: Id del poligono dentro del sistema
        shapefile_id: Id del shapefile dentro del sistema

    Devuelve:
        Informacion del feature generado

    """
    # Generar nombre de archivo
    fil = os.path.join(SHAPEFILE_PATH, shapefile_id, polygon_id + '.json')
    # Leer archivo y generar feature de Earth Engine
    with open(fil) as f:
        feat = ee.Feature(json.load(f))
    return feat


###############################################################################
# CONSTANTES
###############################################################################

# Memcache se usa para acelerar algunos procesos guardando valores.
# El cache expira luego de 24 horas, mas informacion en:
# https://cloud.google.com/appengine/docs/python/memcache/
MEMCACHE_EXPIRATION = 60 * 60 * 24

# Carpeta remota para assets custom
ROOT_ID = 'users/' + config.EE_USER + '/sima'

# Carpeta local donde se guardan las subcarpetas de poligonos
SHAPEFILE_PATH = 'www/shapefiles/'

# Leer shapefiles disponibles en subcarpetas
SHAPEFILE_DATA = {}
for directory in os.listdir(SHAPEFILE_PATH):
    IDS = [name.replace('.json', '') for name in os.listdir(
        os.path.join(SHAPEFILE_PATH, directory))]
    SHAPEFILE_DATA[directory] = IDS

# Opcion de Earth Engine con numero maximo de pixeles a procesar,
# modificar este numero puede permitir ciertos analisis con el riesgo
# de generar errores de timeout.
MAX_PIXELS = 1e12

###############################################################################
# INICIALIZACION
###############################################################################

# Configuracion de credenciales de Earth Engine
EE_CREDENTIALS = ee.ServiceAccountCredentials(
    config.EE_ACCOUNT, config.EE_PRIVATE_KEY_FILE)

# Inicializacion de API de Earth Engine
ee.Initialize(EE_CREDENTIALS)
