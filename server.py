#!/usr/bin/env python
# coding=utf-8
"""
Servidor web para la aplicacion SIMA.
La arquitectura globale esta definida por:
 ______       ____________       _________
|      |     |            |     |         |
|  EE  | <-> | App Engine | <-> | Browser |
|______|     |____________|     |_________|
     \                               /
      '- - - - - - - - - - - - - - -'

El codigo en los scripts python se ejecuta en App Engine. Las rutinas se
llamancuando el usuario carga la pagina web e interactua con la interfaz.
El codigo en App Engine se encarga de la comunicacion con EarthEngine a
partir de la libreria EarthEngine de python y la cuenta (service account)
especificada en el archivo config.py.
"""
import sys
import webapp2
sys.path.insert(0, 'sima')
sys.path.insert(0, 'sima/handlers')
from handlers import *

###############################################################################
#                             Handlers requests http                          #
###############################################################################


# Define los handlers utilizados para cada tipo de request http.
app = webapp2.WSGIApplication([
    ('/eoData', EODataHandler),
    ('/pieChart', PieChartHandler),
    ('/timeseries', SeriesHandler),
    ('/histogram', HistogramHandler),
    ('/customData', resourcesHandler),
    ('/pixelValue', PixelValueHandler),
    ('/polygonArea', PolygonAreaHandler),
    ('/shapefileData', shapefileHandler),
    ('/initMapId', MapIdHandler),
    ('/', MainHandler)
])
