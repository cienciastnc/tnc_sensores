#!/usr/bin/env python
"""An example config.py file."""


import ee

# The service account email address authorized by your Google contact.
# Set up a service account as described in the README.
EE_ACCOUNT = 'simadile@simacolombia.iam.gserviceaccount.com'

# The private key associated with your service account in JSON format.
EE_PRIVATE_KEY_FILE = 'json/simacolombia-77d8fd5f1ed6.json'

# The user folder associated with your service account.
EE_USER = 'cienciastnc'


EE_CREDENTIALS = ee.ServiceAccountCredentials(EE_ACCOUNT, EE_PRIVATE_KEY_FILE)
