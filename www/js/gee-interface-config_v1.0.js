/*                                                                */
/*    Defining options that allows EO Data updating on screen     */
/*                                                                */

 /**
 * Opciones de configuración que permite el manejo, uso y viusalizacon de
 * productos saltelitales en la aplicación sima-dile.
 */
var d = new Date();
var month = d.getMonth() + 1;
var day = d.getDate();
var today_Date = d.getFullYear() + '-' +
    (month < 10 ? '0' : '') + month + '-' +
    (day < 10 ? '0' : '') + day;


var shape_color = 'black';

/**
* Parametrización de los productos satelitales
*
*"handler_id": {
*    "product": "JRC/GSW1_0/GlobalSurfaceWater",  **** Nombre producto
*    "desc":"YYYYYYY",                            **** Descripción del producto
*    "group": "XXXX",                             **** WO || WI ||SWF 
*    "type": "Image",                             **** Image || ImageCollection
*    "band": "transition",                        **** Nombre Banda {{hander_id}}
*    "dateIni": "2015-01-01",                     **** Fecha Inicial
*    "dateFin": "2015-12-31",                     **** Fecha Final
*    "scale": 30,                                 **** Escala espacial de analisis en metros
*    "min": 0.0,                                  **** Valor minimo
*    "max": 10.0,                                 **** Valor maximo
*    "palette": { "colors": ["ffffff", "0000ff", "22b14c", "d1102d", "99d9ea", "b5e61d", "e6a1aa", "ff7f27", "ffc90e", "7f7f7f", "c3c3c3"]}                    **** Paleta de colores
*    }
*
*/

var EO_visualizator = JSON.parse('{"products": {\
"water_mask": {"product": "MODIS/006/MOD44W", "desc":"Clasificación Anual de agua (MODIS)", "masking":"yes" ,"group": "SWV", "type": "ImageCollection","band": "water_mask", "date_scale":"yearly","dateIni": "2000", "dateFin": "2015", "scale": 250, "min": 0,"max": 1, "pie_chart": "true", "histo_chart": "false", "series_chart": "true","classes":["No Water","Water"], "palette": { "colors": ["ffffff", "0000ff"]}},\
"waterClass": {"product": "JRC/GSW1_0/YearlyHistory", "desc":"Clasificación Anual de Agua (Landsat)","masking":"yes" , "group": "SWV", "type": "ImageCollection","band": "waterClass", "date_scale":"yearly","dateIni": "1984", "dateFin": "2015", "scale": 30, "min": 0.0,"max": 3.0, "pie_chart": "true", "histo_chart": "false", "series_chart": "true","classes" :["No data","Not Water","Seasonal Water","Permanent Water"], "palette": { "colors": ["cccccc", "ffffff", "99d9ea", "0000ff"]}},\
"max_extent": {"product": "JRC/GSW1_0/GlobalSurfaceWater", "desc":"Extensión Máxima de Agua", "masking":"yes" ,"group": "SWV", "type": "Image","band": "max_extent", "dateIni": "2015-01-01", "date_scale":"none","dateFin": "2015-12-31", "scale": 30, "min": 0,"max": 1, "pie_chart": "true", "histo_chart": "false", "series_chart": "false", "classes" :["No Water","Water"], "palette": { "colors": ["ffffff", "0000ff"]}},\
"seasonality": {"product": "JRC/GSW1_0/GlobalSurfaceWater", "desc":"Estacionalidad", "masking":"yes" ,"group": "SWV", "type": "Image","band": "seasonality", "dateIni": "2015-01-01", "date_scale":"none","dateFin": "2015-12-31", "scale": 30, "min": 0,"max": 12, "pie_chart": "true", "histo_chart": "true", "series_chart": "false","classes" :["0 meses","1 mes","2 meses","3 meses","4 meses","5 meses","6 meses","7 meses","8 meses","9 meses","10 meses","11 meses","12 meses"], "palette": { "colors": ["ffffff","f9f4fc","f8e7f6","fbd9ea","feccd9","ffbbbb","ffb4c9","ff9bb8","ff80a8","f54fad","db05c0","a800dd","0000ff"]}},\
"transition": {"product": "JRC/GSW1_0/GlobalSurfaceWater", "desc":"Transición", "masking":"yes" ,"group": "SWV", "type": "Image","band": "transition", "date_scale":"none", "dateIni": "2015-01-01", "dateFin": "2015-12-31", "scale": 30, "min": 0,"max": 10, "pie_chart": "true", "histo_chart": "false", "series_chart": "false", "classes" :["No Agua","Permanente","Permanente Nuevo","Permanente Perdido","Estacional","Estacional Nuevo","Estacional Perdido","Estacional a Permanente","Permanente a Estacional","Permanente efímero","Estacional efímero"], "palette": { "colors": ["ffffff", "0000ff", "22b14c", "d1102d", "99d9ea", "b5e61d", "e6a1aa", "ff7f27", "ffc90e", "7f7f7f", "c3c3c3"]}},\
"occurrence": {"product": "JRC/GSW1_0/GlobalSurfaceWater", "desc":"Ocurrencia de Agua", "masking":"yes" ,"group": "SWV", "type": "Image","band": "occurrence", "dateIni": "2015-01-01", "date_scale":"none","dateFin": "2015-12-31", "scale": 30, "min": 0,"max": 100, "pie_chart": "false", "histo_chart": "true", "series_chart": "false", "palette": { "colors": ["ffffff", "ffbbbb", "0000ff"]}},\
"change_abs": {"product": "JRC/GSW1_0/GlobalSurfaceWater", "desc":"Intensidad de Cambio (Absoluto)", "masking":"none" ,"group": "SWV", "type": "Image","band": "change_abs", "dateIni": "2015-01-01", "date_scale":"none","dateFin": "2015-12-31", "scale": 30, "min": -50,"max": 50, "pie_chart": "false", "histo_chart": "true", "series_chart": "false", "palette": { "colors": ["red", "black", "limegreen"]}},\
"change_norm": {"product": "JRC/GSW1_0/GlobalSurfaceWater", "desc":"Intensidad de Cambio (Normalizado)", "masking":"none" ,"group": "SWV", "type": "Image","band": "change_norm", "dateIni": "2015-01-01", "date_scale":"none","dateFin": "2015-12-31", "scale": 30, "min": -50,"max": 50, "pie_chart": "false", "histo_chart": "true", "series_chart": "false", "palette": { "colors": ["red", "black", "limegreen"]}},\
"recurrence": {"product": "JRC/GSW1_0/GlobalSurfaceWater", "desc":"Recurrencia Anual", "masking":"yes" ,"group": "SWV", "type": "Image","band": "recurrence", "dateIni": "2015-01-01", "date_scale":"none","dateFin": "2015-12-31", "scale": 30, "min": 0,"max": 100, "pie_chart": "false", "histo_chart": "true", "series_chart": "false", "palette": { "colors": ["ffffff", "ffbbbb", "0000ff"]}},\
"water": {"product": "GLCF/GLS_WATER", "desc":"Mascara Agua Superficial 2000","masking":"none" , "group": "SWV", "type": "ICNoDate","band": "water", "dateIni": "2015-01-01", "date_scale":"none","dateFin": "2015-12-31", "scale": 30, "min": 1,"max": 4, "pie_chart": "true", "histo_chart": "false", "series_chart": "false", "classes":{"0":"Not Defined","1":"Land","2":"Water","4":"Snow","200":"Shadow","201":"Cloud"}, "palette": { "colors": ["FAFAFA", "00C5FF", "DF73FF", "828282", "CCCCCC"] ,  "colors_dict": {"1":"FAFAFA","2":"00C5FF","4":"DF73FF","200":"828282","201":"CCCCCC"}}},\
"ndwi": {"product": "LANDSAT/LC08/C01/T1_SR", "desc":"Indice de diferencia de agua normalizado (NDWI)", "masking":"none" ,"group": "WI", "type": "ImageCollection","band": "ndwi", "date_scale":"landsat","dateIni": "2013-03-01", "dateFin": "' + today_Date + '", "scale": 30, "min": 0,"max": 1, "pie_chart": "false", "histo_chart": "false", "series_chart": "true", "palette": { "colors": ["d3d3d3", "0000FF"]}},\
"mndwi": {"product": "LANDSAT/LC08/C01/T1_SR", "desc":"Indice de diferencia de agua normalizado modificado (mNDWI)","masking":"none" ,"group": "WI", "type": "ImageCollection","band": "mndwi", "date_scale":"landsat","dateIni": "2013-03-01", "dateFin": "' + today_Date + '", "scale": 30, "min": 0,"max": 1, "pie_chart": "false", "histo_chart": "false", "series_chart": "true", "palette": { "colors": ["d3d3d3", "0000FF"]}},\
"wi2015": {"product": "LANDSAT/LC08/C01/T1_SR", "desc":"Indice de agua Landsat 2015","masking":"none" , "group": "WI", "type": "ImageCollection","band": "wi2015", "date_scale":"landsat","dateIni": "2013-03-01","dateFin": "' + today_Date + '", "scale": 30, "min": 0,"max": 1, "pie_chart": "false", "histo_chart": "false", "series_chart": "true", "palette": { "colors": ["d3d3d3", "0000FF"]}},\
"ldawi": {"product": "LANDSAT/LC08/C01/T1_SR", "desc":"Indice de agua por Análisis Discriminante Lineal", "masking":"none" ,"group": "WI", "type": "ImageCollection","band": "ldawi", "date_scale":"landsat","dateIni": "2013-03-01", "dateFin": "' + today_Date + '", "scale": 30, "min": 0,"max": 1, "pie_chart": "false", "histo_chart": "false", "series_chart": "true", "palette": { "colors": ["d3d3d3", "0000FF"]}},\
"tcw": {"product": "LANDSAT/LC08/C01/T1_SR", "desc":"Tasselled Cap Wetness", "masking":"none" ,"group": "WI", "type": "ImageCollection","band": "tcw", "date_scale":"landsat","dateIni": "2013-03-01","dateFin": "' + today_Date + '", "scale": 30, "min": -1,"max": 1, "pie_chart": "false", "histo_chart": "false", "series_chart": "true", "palette": { "colors": ["red", "white", "blue"]}},\
"awei_sw": {"product": "LANDSAT/LC08/C01/T1_SR", "desc":"Indice automatizado de extracción de agua (Sombra) ","masking":"none" , "group": "WI", "type": "ImageCollection","band": "awei_sw","date_scale":"landsat","dateIni": "2013-03-01", "dateFin": "' + today_Date + '", "scale": 30, "min": 0,"max": 1, "pie_chart": "false", "histo_chart": "false", "series_chart": "true", "palette": { "colors": ["d3d3d3", "0000FF"]}},\
"awei_nsw": {"product": "LANDSAT/LC08/C01/T1_SR", "desc":"Indice automatizado de extracción de agua (No Sombra)", "masking":"none" ,"group": "WI", "type": "ImageCollection","band": "awei_nsw","date_scale":"landsat","dateIni": "2013-03-01", "dateFin": "' + today_Date + '", "scale": 30, "min": 0,"max": 1, "pie_chart": "false", "histo_chart": "false", "series_chart": "true", "palette": { "colors": ["d3d3d3", "0000FF"]}},\
"treecover2000": {"product": "UMD/hansen/global_forest_change_2018_v1_6", "desc":"GFW Porcentaje Bosque 2000", "masking":"none" ,"group": "GFW", "type": "Image", "band": "treecover2000", "date_scale":"none","dateIni": "1999-01-01", "dateFin": "2018-12-31", "scale": 30, "min": 0,"max": 100, "pie_chart": "false", "histo_chart": "true", "series_chart": "false", "palette": { "colors": ["000000", "00FF00"]}},\
"loss": {"product": "UMD/hansen/global_forest_change_2018_v1_6", "desc":"GFW Pérdida de Bosque", "masking":"none" ,"group": "GFW", "type": "Image","band": "loss", "dateIni": "1999-01-01", "date_scale":"none","dateFin": "2018-12-31", "scale": 30, "min": 0,"max": 1, "pie_chart": "false", "histo_chart": "true", "series_chart": "false","classes" :["Not defined","Forest lost"], "palette": { "colors": ["FFFFFF","FF0000"]}},\
"gain": {"product": "UMD/hansen/global_forest_change_2018_v1_6", "desc":"GFW Ganancia de Bosque", "masking":"none" ,"group": "GFW", "type": "Image","band": "gain", "dateIni": "1999-01-01", "date_scale":"none","dateFin": "2018-12-31", "scale": 30, "min": 0,"max": 2, "pie_chart": "false", "histo_chart": "true", "series_chart": "false","classes" :["Not defined","Forest gain"], "palette": { "colors": ["FFFFFF","0000FF"]}},\
"combined": {"product": "UMD/hansen/global_forest_change_2018_v1_6", "desc":"GFW Cambio de Bosque", "masking":"none" ,"group": "GFW", "type": "Image","band": "combined", "date_scale":"none","dateIni": "1999-01-01", "dateFin": "2018-12-31", "scale": 30, "min": 1,"max": 3, "pie_chart": "false", "histo_chart": "false", "series_chart": "false", "classes" :["No definido","50% Forest","Forest lost","Forest gain","Forest gain"],"palette": { "colors": ["00FF00", "FF0000", "0000FF"]}},\
"b1": {"product": "WWF/HydroSHEDS/03CONDEM", "desc":"DEM Hidrográfico", "masking":"none" , "group": "SWV", "type": "Image", "band": "b1", "dateIni": "2015-01-01","date_scale":"none", "dateFin": "2015-12-31", "scale": 92, "min": -50,"max": 3000, "pie_chart": "false", "histo_chart": "true", "series_chart": "false", "palette": { "colors": ["ffffff","D4D4D4","AAAAAA", "7F7F7F", "555555","2A2A2A", "000000"]}}}}')


/**
* Información adicional de los productos satelitales.
*
*"handler_id": {
*    "title": "JRC/GSW1_0/GlobalSurfaceWater", 
*    "desc":"Text",           ****Product Description 
*    "classes": "Text",            ****Product Classes 
*    "source": "Text",            ****Prduct sources
*    }
*
*/
var EO_data_info = JSON.parse('{"products": {\
  "water_mask": {"title": "Clasificación Anual de Agua (MODIS)", "desc": "El producto de clasificación anual de agua de MODIS (MOD44W) proporciona un mapa de agua superficial anual entre 2000 y 2015 de cobertura global a una resolución de 250 metros. Los mapas de cobertura de agua superficial son generados a partir de datos ópticos MODIS con datos de radar de la misión SRTM (Shuttle Radar Topography Mission). El producto incorpora una serie de máscaras para disminuir errores de clasificación asociados a sombras , áreas quemadas, nubes y hielo. La última versión del producto corresponde a la colección 6 de MODIS.", "source":"Carroll, M. L., Townshend, J. R., DiMiceli, C. M., Noojipady, P., & Sohlberg, R. A. (2009). A new global raster water mask at 250 m resolution. International Journal of Digital Earth, 2(4), 291–308. doi:10.1080/17538940902951401","classes":""},\
  "waterClass": {"title": "Clasificación Anual de Agua (Landsat)", "desc": "El producto clasificación anual de agua de Landsat a 30m entrega información sobre todas las detecciones de agua a nivel anual para el periodo 1984-2015, asi como el tipo de comportamiento (estacional o permanente). El producto de detección anual entrega los siguientes valores:<br>Detección Anual de Agua<center><table class=\'table table-bordered table-dark table-width\'><thead><tr><th scope=\'col\'>Valor</th><th scope=\'col\'>Clase</th></tr></thead><tbody><tr><td>0</td><td>Sin observaciones</td></tr><tr><td>1</td><td>No Agua</td></tr><tr><td>2</td><td>Agua Estacional</td></tr><tr><td>3</td><td>Agua Permanente</td></tr></tbody></table></center>", "source":"Pekel, J. F., Cottam, A., Gorelick, N., & Belward, A. S. (2016). High-resolution mapping of global surface water and its long-term changes. Nature, 540(7633), 418–422. doi:10.1038/nature20584","classes":""},\
  "max_extent": {"title": "Maxima Extensión de Agua)", "classes":"Este producto es parte del proyecto JRC Global Surface Water Mapping Layers (GSW), el cual proporciona mapas a una resolución espacial de 30 metros de la ubicación y distribución temporal de agua superficial a nivel global entre 1984 y 2015. Además, entrega información sobre el comportamiento intra e interanual de los cuerpos de agua superficiales. El producto GSW fue elaborado a partir del uso de índices de vegetación, transformaciones de color, y bandas independientes analizadas mediante sistemas expertos, y análisis visual y razonamiento evidencial de analistas.", "source": "Pekel, J. F., Cottam, A., Gorelick, N., & Belward, A. S. (2016). High-resolution mapping of global surface water and its long-term changes. Nature, 540(7633), 418–422. doi:10.1038/nature20584","desc":"El producto de máxima extención de agua muestra todos los lugares en los cuales se detecto agua al menos una vez durante el periodo de 1984-2015. Este producto entrega los valores 0 (i.e. nunca se detecto agua) y 1 (i.e. al menos una detección de agua)."},\
  "seasonality": {"title": "Estacionalidad", "classes":"Este producto es parte del proyecto JRC Global Surface Water Mapping Layers (GSW), el cual proporciona mapas a una resolución espacial de 30 metros de la ubicación y distribución temporal de agua superficial a nivel global entre 1984 y 2015. Además, entrega información sobre el comportamiento intra e interanual de los cuerpos de agua superficiales. El producto GSW fue elaborado a partir del uso de índices de vegetación, transformaciones de color, y bandas independientes analizadas mediante sistemas expertos, y análisis visual y razonamiento evidencial de analistas.", "source": "Pekel, J. F., Cottam, A., Gorelick, N., & Belward, A. S. (2016). High-resolution mapping of global surface water and its long-term changes. Nature, 540(7633), 418–422. doi:10.1038/nature20584","desc":"El producto de estacionalidad indica el número de meses en que se detecto agua para un año en particular (2015). Los valores fluctúan entre 0 y 12 meses."},\
  "transition": {"title": "Transición de clases de Agua", "classes":"Este producto es parte del proyecto JRC Global Surface Water Mapping Layers (GSW), el cual proporciona mapas a una resolución espacial de 30 metros de la ubicación y distribución temporal de agua superficial a nivel global entre 1984 y 2015. Además, entrega información sobre el comportamiento intra e interanual de los cuerpos de agua superficiales. El producto GSW fue elaborado a partir del uso de índices de vegetación, transformaciones de color, y bandas independientes analizadas mediante sistemas expertos, y análisis visual y razonamiento evidencial de analistas.", "source": "Pekel, J. F., Cottam, A., Gorelick, N., & Belward, A. S. (2016). High-resolution mapping of global surface water and its long-term changes. Nature, 540(7633), 418–422. doi:10.1038/nature20584", "desc": "El producto de transición entrega información respecto al cambio en la estacionalidad entre los primeros y ultimos años del periodo en que se detecto agua, y muestra los cambios entre las clases de \'No Cambio\', \'Agua Estacional\', y \'Agua Permanente\'. Los valores del producto de transición, asi como sus clases correspondientes se muestran en la siguiente tabla:<br><center><table class=\'table table-bordered table-dark table-width\'><thead><tr><th scope=\'co\'>Valor</th><th scope=\'col\'>Clase</th></tr></thead><tbody><tr><td>0</td><td>No Agua</td></tr><tr><td>1</td><td>Permanente</td></tr><tr><td>2</td><td>Permanente Nuevo</td></tr><tr><td>3</td><td>Permanente Perdido</td></tr><tr><td>4</td><td>Estacional</td></tr><tr><td>5</td><td>Estacional Nuevo</td></tr><tr><td>6</td><td>Estacional Perdido</td></tr><tr><td>7</td><td>Estacional a Permanente</td></tr><tr><td>8</td><td>Permanente a Estacional</td></tr><tr><td>9</td><td>Permanente efímero</td></tr><tr><td>10</td><td>Estacional efímero</td></tr><tr><td>255</td><td>Sin observaciones</td></tr></tbody></table></center>"},\
  "occurrence": {"title": "Ocurrencia de Agua", "classes":"Este producto es parte del proyecto JRC Global Surface Water Mapping Layers (GSW), el cual proporciona mapas a una resolución espacial de 30 metros de la ubicación y distribución temporal de agua superficial a nivel global entre 1984 y 2015. Además, entrega información sobre el comportamiento intra e interanual de los cuerpos de agua superficiales. El producto GSW fue elaborado a partir del uso de índices de vegetación, transformaciones de color, y bandas independientes analizadas mediante sistemas expertos, y análisis visual y razonamiento evidencial de analistas.", "source": "Pekel, J. F., Cottam, A., Gorelick, N., & Belward, A. S. (2016). High-resolution mapping of global surface water and its long-term changes. Nature, 540(7633), 418–422. doi:10.1038/nature20584","desc":"El producto de ocurrencia de agua muestra donde y con que frecuencia se detecto agua superficial durante el periodo 1984 y 2015. El producto de ocurrencia de agua entrega valores entre 0% y 100%, los que corresponden al  porcentaje de meses en los cuales se detectó agua superficial."},\
  "change_abs": {"title": "Cambio Absoluto", "classes":"Este producto es parte del proyecto JRC Global Surface Water Mapping Layers (GSW), el cual proporciona mapas a una resolución espacial de 30 metros de la ubicación y distribución temporal de agua superficial a nivel global entre 1984 y 2015. Además, entrega información sobre el comportamiento intra e interanual de los cuerpos de agua superficiales. El producto GSW fue elaborado a partir del uso de índices de vegetación, transformaciones de color, y bandas independientes analizadas mediante sistemas expertos, y análisis visual y razonamiento evidencial de analistas.", "source": "Pekel, J. F., Cottam, A., Gorelick, N., & Belward, A. S. (2016). High-resolution mapping of global surface water and its long-term changes. Nature, 540(7633), 418–422. doi:10.1038/nature20584","desc":"El producto de intensidad en cambio de ocurrencia entrega información sobre donde la ocurrencia media de agua aumento, disminuyó, o se mantuvo constante entre los periodos 1984-1999 y 2000-2015. Los valores fluctuan entre -100%  y 100%, y corresponden a la diferencia entre periodos i.e. (Periodo2 - Periodo1)"},\
  "change_norm": {"title": "Cambio Normalizado", "classes":"Este producto es parte del proyecto JRC Global Surface Water Mapping Layers (GSW), el cual proporciona mapas a una resolución espacial de 30 metros de la ubicación y distribución temporal de agua superficial a nivel global entre 1984 y 2015. Además, entrega información sobre el comportamiento intra e interanual de los cuerpos de agua superficiales. El producto GSW fue elaborado a partir del uso de índices de vegetación, transformaciones de color, y bandas independientes analizadas mediante sistemas expertos, y análisis visual y razonamiento evidencial de analistas.", "source": "Pekel, J. F., Cottam, A., Gorelick, N., & Belward, A. S. (2016). High-resolution mapping of global surface water and its long-term changes. Nature, 540(7633), 418–422. doi:10.1038/nature20584","desc":"El producto de intensidad en cambio de ocurrencia entrega información sobre donde la ocurrencia media de agua aumento, disminuyó, o se mantuvo constante entre los periodos 1984-1999 y 2000-2015. Los valores fluctuan entre -1  y 1 y corresponden a la diferencia entre periodos normalizada por la suma de los periodos i.e. (Periodo2 - Periodo1)/ (Periodo2 + Periodo1)."},\
  "recurrence": {"title": "Recurrencia de Agua", "classes":"Este producto es parte del proyecto JRC Global Surface Water Mapping Layers (GSW), el cual proporciona mapas a una resolución espacial de 30 metros de la ubicación y distribución temporal de agua superficial a nivel global entre 1984 y 2015. Además, entrega información sobre el comportamiento intra e interanual de los cuerpos de agua superficiales. El producto GSW fue elaborado a partir del uso de índices de vegetación, transformaciones de color, y bandas independientes analizadas mediante sistemas expertos, y análisis visual y razonamiento evidencial de analistas.", "source": "Pekel, J. F., Cottam, A., Gorelick, N., & Belward, A. S. (2016). High-resolution mapping of global surface water and its long-term changes. Nature, 540(7633), 418–422. doi:10.1038/nature20584","desc":"El producto de recurrencia de agua muestra la frecuencia con la que el agua vuelve año a año durante el periodo 1984 y 2015. El producto de recurrencia de agua entrega valores entre 0% y 100%."},\
  "water": {"title": "GLCF: Landsat Global Inland Water (Feng et al., 2015)", "desc":"La máscara de agua superficial de GLCF muestra los cuerpos de agua presentes detectados durante 2000 por el sensor TM. El algoritmo se basa en el uso de los índices NDWI (McFeeters, 1996) y mNDWI (Xu, 2006) junto con variables de topografía como la pendiente y exposición para determinar la cobertura de agua superficial a 30 metros de resolución. La siguiente tabla muestra los posibles valores entregados por el producto.", "source": "Feng, M., Sexton, J. O., Channan, S., & Townshend, J. R. (2016). A global, high-resolution (30-m) inland water body dataset for 2000: first results of a topographic–spectral classification algorithm. International Journal of Digital Earth, 9(2), 113–133. doi:10.1080/17538947.2015.1026420", "classes": "<center><table class=\'table table-bordered table-dark table-width\'><thead><tr><th scope=\'col\'>Valor</th><th scope=\'col\'>Clase</th></tr></thead><tbody><tr><td>1</td><td>No Agua</td></tr><tr><td>2</td><td>Agua</td></tr><tr><td>4</td><td>Nieve/hielo</td></tr><tr><td>200</td><td>Sombra de nube</td></tr><tr><td>201</td><td>Nube</td></tr></tbody></table></center>"},\
  "ndwi": {"title": "Normalized Difference Water Index (Mcfeeters, 1996)", "desc": "El índice de agua de diferencia normalizada (NDWI por sus siglas en inglés) diseñado por Mcfeeters, S. (1996) realza el agua superficial por sobre la vegetación y el suelo desnudo al calcular la diferencia normalizada entre la reflectancia superficial de dos bandas en el rango verde e infrarrojo (NIR) cercano del espectro electromagnético: <hr><center>NDWI = (Verde - NIR) / (Verde + NIR)</center><hr>La selección de estas bandas permite:<ol><li>Maximizar la señal de los cuerpos de agua al usar longitudes de onda en el rango verde.</li><li>Minimizar la baja reflectancia del infrarrojo cercano por los cuerpos de agua.</li><li>Maximizar la alta reflectancia del infrarrojo cercano en la vegetación terrestre y el suelo.</li></ol>Al calcular este índice se obtienen valores positivos para los cuerpos de agua superficial, y valores cercanos a cero o negativos para el suelo y la vegetación terrestre. Sin embargo, estudios posteriores (Xu, H., 2006) mostraron que en algunos casos el suelo edificado tiene un comportamiento espectral en el verde y NIR similar al del agua superficial (ambos reflejan la luz verde más que la luz infrarroja cercana). ", "source":"Mcfeeters S. (1996). The use of the normalized difference water index in the delineation of open water features. Int. J. Remote Sens, 17:1425–1432, doi: 10.1080/01431169608948714.","classes":""},\
  "mndwi": {"title": "Modified Normalized Difference Water Index (Xu, 2006)", "desc": "Xu, H. (2006) propuso modificar el NDWI de Mcfeeters (1996) para mejorar la discriminación entre cuerpos de agua y terreno edificado. El índice modificado (mNDWI) reemplaza el infrarrójo cercano por el infrarrójo de onda corta (SWIR):<hr><center>mNDWI = (Verde - SWIR)/(Verde + SWIR)</center><hr>La selección del infrarrojo medio se basa en el diferente comportamiento de la vegetación, el terreno edificado, y el agua superficial en el verde e infrarrojo de onda corta. Tanto la vegetación como el terreno edificado reflejan más energía en el infrarrojo de onda corta que en el verde, mientras que el agua superficial tiene una respuesta inversa.", "source":"Xu H. (2006). Modification of normalised difference water index (NDWI) to enhance open water features in remotely sensed imagery. Int. J. Remote Sens, 27:3025–3033, doi: 10.1080/01431160600589179.","classes":""},\
  "wi2015": {"title": "Landsat Water Index 2015", "desc": "Fisher, A., et al., (2016) propúso un índice para identificación de agua superficial basado en la reflectancia superficial de 5 bandas presentes en los sensores “Thematic Mapper” (TM), “Enhanced Thematic Mapper” (ETM+), y “Operational Land Imager” a bordo de los satélites Landsat-5, Landsat-7, y Landsat-8, respectivamente. El índice WI2015 se calcúla a partir de la combinación lineal de las bandas verde, roja, infrarrója cercana (NIR), infrarrója de onda corta 1 (SWIR1), e infrarrója de onda corta 2 (SWIR2):<hr><center>WI2015 = 1.7204 + 171 * Verde + 3 * Rojo - 70 * NIR - 45 * SWIR1 - 71 * SWIR2 </center>", "source":"Fisher, A., Flood, N., & Danaher, T. (2016). Comparing Landsat water index methods for automated water classification in eastern Australia. Remote Sensing of Environment, 175, 167–182. doi:10.1016/j.rse.2015.12.055", "classes":""},\
  "ldawi": {"title": "Linear Discriminant Analysis Water Index (Fisher & Danaher, 2013)", "desc": "El Linear Discriminant Analysis Water Index (LDAWI) fue diseñado por Fischer & Danaher (2013) para aprovechar las bandas presentes en el sensor HRG a bordo del satélite SPOT. El LDAWI se basa en variables predictoras definidas a partir de  las bandas verde, roja, infrarroja cercana, e infrarroja de onda corta:<hr><center>LDAWI = &alpha; + &beta;1 * x1 + &beta;2 * x2 + &hellip; + &beta;n * xn</center><hr>donde &alpha; es el intercepto, y &beta;i corresponde al coeficiente de cada variable predictora xi descrita en la siguiente tabla.<br>Coeficientes y variables predictoras de LDAWI<center><table class=\'table table-bordered table-dark table-width\'><thead><tr><th scope=\'col\'>Parametro</th><th scope=\'col\'>Valor</th><th scope=\'col\'>Variable Predictora</th><th scope=\'col\'>Valor</th></tr></thead><tbody><tr><td>ɑ</td><td>224.14</td><td></td><td></td></tr><tr><td>β1</td><td>-76.18</td><td>x1</td><td>ln(verde)</td></tr><tr><td>β2</td><td>-18.20</td><td>x2</td><td>ln(rojo)</td></tr><tr><td>β3</td><td>-43.00</td><td>x3</td><td>ln(NIR)</td></tr><tr><td>β4</td><td>96.42</td><td>x4</td><td>ln(SWIR)</td></tr><tr><td>β5</td><td>3.79</td><td>x5</td><td>ln(verde) x ln(rojo)</td></tr><tr><td>β6</td><td>16.28</td><td>x6</td><td>ln(verde) x ln(NIR)</td></tr><tr><td>β7</td><td>-6.25</td><td>x7</td><td>ln(verde) x ln(SWIR)</td></tr><tr><td>β8</td><td>1.54</td><td>x8</td><td>ln(rojo) x ln(NIR)</td></tr><tr><td>β9</td><td>-1.14</td><td>x9</td><td>ln(rojo) x ln(SWIR)</td></tr><tr><td>β10</td><td>-12.77</td><td>x10</td><td>ln(NIR) x ln(SWIR)</td></tr></tbody></table></center>", "source":"Fisher, A., & Danaher, T. (2013). A water index for SPOT5 HRG satellite imagery, New South Wales, Australia, determined by linear discriminant analysis. Remote Sensing, 5(11), 5907–5925. doi:10.3390/rs5115907","classes":""},\
  "tcw": {"title": "Tasselled Cap Wetness (Crist, 1985)", "desc": "El índice TCW (Crist, 1985) se basa en el método Tasseled Cap (Kauth and Thomas, 1976) de reducción de dimensionalidad para transformar las bandas reflectivas del sensor TM en planos de brillo, verdor, humedad. Aunque no diseñado específicamente para la discriminación de cuerpos de agua, la transformaciones propuestas por Crist, y en particular la referente al plano de humedad, ha sido usada en diversos estudios para identificar y analizar cuerpos de agua superficial. La siguiente tabla muestra los coeficientes requeridos para transformar las bandas del sensor TM en los planos mencionados previamente.<hr>TCPlano=Azul * C1 + Verde * C2 + Rojo * C3 + NIR * C4 + SWIR1 * C5 + SWIR2 * C6", "source":"Crist, E. P. (1985). A TM Tasseled Cap equivalent transformation for reflectance factor data. Remote Sensing of Environment, 17(3), 301–306. doi:10.1016/0034-4257(85)90102-6\\n\\nAli Baig, M.H., Zhang, L., Tong, S., Tong, Q. (2014)Derivation of a tasselled cap transformation based on Landsat 8 at-satellite reflectance. Remote Sensing Letters. 5:5, 423-431. doi:10.1080/2150704X.2014.915434","classes":"Coeficientes para transformación del índice Tasselled Cap Wetness<table class=\'table table-bordered table-dark table-width\'><thead><tr><th scope=\'co\'>plano</th><th scope=\'col\'>Humedad</th></tr></thead><tbody><tr><td>Azul (C1)</td><td>0.0315</td></tr><tr><td>Verde (C2)</td><td>0.2021</td></tr><tr><td>Rojo (C3)</td><td>0.3102</td></tr><tr><td>NIR (C4)</td><td>0.1594</td></tr><tr><td>SWIR1 (C5)</td><td>0.6806</td></tr><tr><td>SWIR2 (C6)</td><td>-0.6109</td></tr></tbody></table>"},\
  "awei_sw": {"title": "Automated Water Extraction Index Shadow (Feyisa et al., 2014)", "desc": "Feyisa et al., (2014) propuso dos índices para la identificación de agua superficial en escenas con y sin sombra (AWEIshadow y AWEIno_shadow) a partir de la reflectancia superficial de bandas del sensor TM (Landsat 5). La combinación de bandas y sus coeficientes respectivos fueron determinados empíricamente con un método iterativo, con el objetivo de maximizar la separabilidad de píxeles de agua y otras coberturas. <b><u>Esta capa corresponde a la versión del índice para escenas con sombra:</b></u><hr><br><center>AWEIshadow = Azul + 2.5 * Verde - 1.5 * (NIR + SWIR1) - 0.25 * SWIR2</center>", "source":"Feyisa, G. L., Meilby, H., Fensholt, R., & Proud, S. R. (2014). Automated Water Extraction Index: A new technique for surface water mapping using Landsat imagery. Remote Sensing of Environment, 140, 23–35. doi:10.1016/j.rse.2013.08.029","classes":""},\
  "awei_nsw": {"title": "Automated Water Extraction Index Non-Shadow(Feyisa et al., 2014)", "desc": "Feyisa et al., (2014) propuso dos índices para la identificación de agua superficial en escenas con y sin sombra (AWEIshadow y AWEIno_shadow) a partir de la reflectancia superficial de bandas del sensor TM. La combinación de bandas y sus coeficientes respectivos fueron determinados empíricamente con un método iterativo, con el objetivo de maximizar la separabilidad de píxeles de agua y otras coberturas. <b><u>Esta capa corresponde a la versión del índice para escenas sin sombra:</u></b><hr><br><center>AWEInoshadow = 4 * (Verde - SWIR1) - (0.25 * NIR + 2.75 * SWIR1)</center>", "source":"Feyisa, G. L., Meilby, H., Fensholt, R., & Proud, S. R. (2014). Automated Water Extraction Index: A new technique for surface water mapping using Landsat imagery. Remote Sensing of Environment, 140, 23–35. doi:10.1016/j.rse.2013.08.029","classes":""},\
  "treecover2000": {"title": "GFW Porcentaje Bosque 2000", "desc": "Los productos GFW (Global Forest Watch) entregan información global respecto al estado y cambio de los bosques desde el año 2000. El producto de Porcentaje Bosque indica el porcentaje de cobertura de dosel para toda la vegetación con una altura mayor a 5 metros." , "source":"Hansen, M. C., P. V. Potapov, R. Moore, M. Hancher, S. A. Turubanova, A. Tyukavina, D. Thau, S. V. Stehman, S. J. Goetz, T. R. Loveland, A. Kommareddy, A. Egorov, L. Chini, C. O. Justice, and J. R. G. Townshend. 2013. “High-Resolution Global Maps of 21st-Century Forest Cover Change.” Science 342 (15 November): 850–53","classes":""},\
  "loss": {"title": "GFW Pérdida de Bosque 2000-2018", "desc": "Los productos GFW (Global Forest Watch) entregan información global respecto al estado y cambio de los bosques desde el año 2000.<br>El producto de Pérdida de bosque muestra las regiones en las que se detectó un cambio de areas de bosque a no-bosque durante el periodo 2000-2018.", "source":"Hansen, M. C., P. V. Potapov, R. Moore, M. Hancher, S. A. Turubanova, A. Tyukavina, D. Thau, S. V. Stehman, S. J. Goetz, T. R. Loveland, A. Kommareddy, A. Egorov, L. Chini, C. O. Justice, and J. R. G. Townshend. 2013. “High-Resolution Global Maps of 21st-Century Forest Cover Change.” Science 342 (15 November): 850–53","classes":""},\
  "gain": {"title": "GFW Ganancia de Bosque 2000-2018", "desc": "Los productos GFW (Global Forest Watch) entregan información global respecto al estado y cambio de los bosques desde el año 2000.<br>El producto de de ganancia de bosque muestra las regiones en las que se detectó un cambio de cobertura desde no-bosque a bosque durante el periodo 2000-2018.", "source":"Hansen, M. C., P. V. Potapov, R. Moore, M. Hancher, S. A. Turubanova, A. Tyukavina, D. Thau, S. V. Stehman, S. J. Goetz, T. R. Loveland, A. Kommareddy, A. Egorov, L. Chini, C. O. Justice, and J. R. G. Townshend. 2013. “High-Resolution Global Maps of 21st-Century Forest Cover Change.” Science 342 (15 November): 850–53","classes":""},\
  "combined": {"title": "GFW Mapa Combinado de Ganancia y Pérdida", "desc": "Los productos GFW (Global Forest Watch) entregan información global respecto al estado y cambio de los bosques desde el año 2000.<br>El mapa combinado muestra las areas donde se detecto una cobertura de bosque mayor al 50% para el año 2000, asi como también las áreas de ganancia y perdida durante el periodo 2000-2018.", "source":"Hansen, M. C., P. V. Potapov, R. Moore, M. Hancher, S. A. Turubanova, A. Tyukavina, D. Thau, S. V. Stehman, S. J. Goetz, T. R. Loveland, A. Kommareddy, A. Egorov, L. Chini, C. O. Justice, and J. R. G. Townshend. 2013. “High-Resolution Global Maps of 21st-Century Forest Cover Change.” Science 342 (15 November): 850–53. doi: 10.1126/science.1244693","classes":""},\
  "b1": {"title": "DEM Hidrológicamente Acondicionado", "desc": "El DEM (modelo de elevación digital) HydroSHEDS entrega información hidrográfica local y regional a partir de datos de elevación de la mision Shuttle Radar Topography Mission (SRTM). El DEM HydroSHEDS es resultado de un proceso de corrección y condicionado especialmente para el análisis y modelamiento hidrográfico.", "source":"Lehner, B., Verdin, K., Jarvis, A. (2008): New global hydrography derived from spaceborne elevation data. Eos, Transactions, AGU, 89(10): 93-94. doi: 10.1029/2008EO100001", "classes": ""}\
}}');


/**
* Gnenera un menu desplegable con todos los productos disponibles..
* @param {option_selected} Opción seleccionada
* @return {{none}} 
*/
function populateEOProducts(option_selected) {
    var dropdown = $("#EOVisualizer");
    var products = EO_visualizator.products;
    var option_value;
    var option_txt;
    dropdown.empty();
    $.each(products, function(key, value) {
        if (option_selected == value.group) {
            option_value = (value.band);
            option_txt = (value.desc);
            dropdown.append($("<option />")
                .val(option_value)
                .text(option_txt));
        }
    });
    updateFlags();
}


/**
* Actualiza los iconos de capacidades disponibles acorde al producto seleccionado.
* @param {{none}} 
* @return {{none}} 
*/
function populateChartsInfo() {
    var dropdownPie = $("#pieSelector");
    var dropdownHisto = $("#histoSelector");
    var dropdownSerie = $("#productSelector");
    var products = EO_visualizator.products;
    var option_value;
    var option_txt;
    dropdownPie.empty();
    dropdownHisto.empty();
    dropdownSerie.empty();
    $.each(products, function(key, value) {
        option_value = (value.band);
        option_txt = (value.desc);
        if (value.pie_chart == "true") dropdownPie.append($("<option />")
            .val(option_value)
            .text(option_txt));
        if (value.histo_chart == "true") dropdownHisto.append($("<option />")
            .val(option_value)
            .text(option_txt));
        if (value.series_chart == "true") dropdownSerie.append($("<option />")
            .val(option_value)
            .text(option_txt));
    });
}

/**
* Actualiza las estructuras de datos para visualizar los graficos.
* @param {{none}} 
* @return {{none}} 
*/

function updateFlags() {
    $("#no_hay_graficos")
        .hide();
    var prod_selected = $('#EOVisualizer')
        .val();
    if (typeof prod_selected === 'undefined' || prod_selected === null) {
        prod_selected = 'transition';
    }
    var products = EO_visualizator.products;
    $('#series_flag')
        .removeClass('flag_selected');
    $('#histo_flag')
        .removeClass('flag_selected');
    $('#pie_flag')
        .removeClass('flag_selected');
    var graficos_disponibles = false;
    if (products[prod_selected].series_chart == "true") {
        $('#series_flag')
            .addClass('flag_selected');
        $("#series_control")
            .show();
        $("#series-div")
            .show();
        graficos_disponibles = true;
    } else {
        $("#series_control")
            .hide();
        $("#series-div")
            .hide();
    }
    if (products[prod_selected].histo_chart == "true") {
        $('#histo_flag')
            .addClass('flag_selected');
        $("#histo_control")
            .show();
        $("#histo-div")
            .show();
        graficos_disponibles = true;
    } else {
        $("#histo_control")
            .hide();
        $("#histo-div")
            .hide();
    }
    if (products[prod_selected].pie_chart == "true") {
        $('#pie_flag')
            .addClass('flag_selected');
        $("#pie_chart_control")
            .show();
        $("#pie-chart-div")
            .show();
        graficos_disponibles = true;
    } else {
        $("#pie_chart_control")
            .hide();
        $("#pie-chart-div")
            .hide();
    }
    if (graficos_disponibles) {
        $("#no_hay_graficos")
            .hide();
    } else {
        $("#no_hay_graficos")
            .show();
    }
}

/**
* Configuración de visualización de Google Maps.
* @param {{none}} 
* @return {{none}} 
*/

var googleMapsStyle = JSON.parse('[\
  {\
    "featureType": "administrative.land_parcel",\
    "stylers": [\
      {\
        "visibility": "off"\
      }\
    ]\
  },\
  {\
    "featureType": "administrative.neighborhood",\
    "stylers": [\
      {\
        "visibility": "off"\
      }\
    ]\
  },\
  {\
    "featureType": "poi.business",\
    "stylers": [\
      {\
        "visibility": "off"\
      }\
    ]\
  },\
  {\
    "featureType": "poi.park",\
    "elementType": "labels.text",\
    "stylers": [\
      {\
        "visibility": "off"\
      }\
    ]\
  },\
  {\
    "featureType": "road",\
    "stylers": [\
      {\
        "visibility": "off"\
      }\
    ]\
  },\
  {\
    "featureType": "road",\
    "elementType": "labels",\
    "stylers": [\
      {\
        "visibility": "off"\
      }\
    ]\
  },\
  {\
    "featureType": "water",\
    "elementType": "labels.text",\
    "stylers": [\
      {\
        "visibility": "off"\
      }\
    ]\
  }\
]');

/**
* Ejecución de metodos de configuración al cargar la aplicacion.
*/
populateEOProducts("SWV");
populateChartsInfo();
updateFlags();