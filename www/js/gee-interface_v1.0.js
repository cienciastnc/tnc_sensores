 /**
 * Ejecuta la aplicacion sima-dile. El codigo se ejecuta en 
 * el navegodaro del usuario. Se comunica con AppEngine y ademas 
 * procesa los resultado visualizandolos en pantalla.
 */
sima = {}; 

// Instancia de Googel maps.
var map_id;

// Id para el uso de datos procesados y almacenados en cache.
var cachekey;

// Instancia herramienta de dibujado de Google.
var drawingManager;

// Polygono actual.
var currentPolygon;

// Poligono de Google Maps convertido en Array.
var coordinatesArray = [];

// Pixel actual
var marker;

// Array de pixels
var markers = [];

// Feature seleccionada
var feature;

// variables de fecha globales.
var month_start = 0;
var month_end = 6;

var baseUrl = "";

if (!(typeof (apiBaseUrl) === "undefined")) {
    baseUrl = apiBaseUrl;
}

// Rango de fechas de imagenes landsat
var landsatDates = [];

// Set de regiones seleccionadas.
var shapefileData = {};

// Tipo de regiones seleccionado
var selected_shapefile = 'Departamentos';
var shapefile_features_added = [];

var loading_map = false;

var setLandsatRangeSliderMinV = 0;
var setLandsatRangeSliderMaxV = 10;

var aoiPolygonSelectedFlag = false;
var aoiRegionSelectedFlag = false;

/**
 * Arranca la aplicacion. Principal punto de ejecucion.
 * @param {string} eeMapId The Earth Engine map ID.
 * @param {string} eeToken The Earth Engine map token.
 * @param {string} serializedPolygonIds
 * @PARAM {string} serializedCuencaIds
 */
sima.boot = function (mapType) {
    // Creamos la aplicacion de sima-dile.
    google.charts.load('current', {
        packages: ['corechart']
        , callback: function () {
            // Agregar poligonos
            var app = new sima.App(mapType);
        }
    });
};


///////////////////////////////////////////////////////////////////////////////
//                               La aplicacion.                            //
///////////////////////////////////////////////////////////////////////////////



/**
 * sima-dile aplicacion principal.
 * Renderiza la interfaz de usuario y gestiona los eventos.
 * @param {google.maps.ImageMapType} mapType El tipo de mapa a renderizar en pantalla.
 * @param {Array<string>} polygonIds Los IDs de los poligonos a mostrar en el mapa.
 * @constructor
 */

sima.App = function (mapType) {
    // Crea y visualiza el mapa.
    map_id = this.createMap(mapType);
    this.map = map_id;
		
	google.maps.event.addListener(map_id,'tilesloaded',
		function () {
			$("#maploading").hide(); 
		}	
	);
	
	
    // Agrega un handler para la gestion de clicks en el area del mapa.
    this.map.data.addListener('click', this.handlePolygonClick.bind(this));
    google.maps.event.addListener(this.map, 'click', function (event) {
        var lat_s = event.latLng.lat();
        var long_s = event.latLng.lng();
        if (markers.length >= 1) {
            marker.setMap(null);
        }
        marker = new google.maps.Marker({
            position: event["latLng"]
            , animation: google.maps.Animation.DROP
            , title: event["latLng"].toString()
        });
        if ($('#pixel_toggle')
            .is(':checked')) {
            marker.setMap(map_id);
            markers.push(marker);
            sima.App.prototype.handlePixelClick(lat_s, long_s);
        }
    });
};


/**
 * Crea un mapa de Google Maps con los datos de configuracion especiicados en gee-interface-config
 * Este mapa esta anclado al domino a traves del elemento "map".
 * @param {google.maps.ImageMapType} mapType tipo de mapa a incluir.
 * @return {google.maps.Map} Instancia del mapa que deber ser renderizada.
 */

sima.App.prototype.createMap = function (mapType) {
    var mapOptions = {
        backgroundColor: '#000000'
        , center: sima.App.DEFAULT_CENTER
        , restriction: {
            latLngBounds: {
                north: sima.App.RESTRICTIONS.north
                , south: sima.App.RESTRICTIONS.south
                , west: sima.App.RESTRICTIONS.west
                , east: sima.App.RESTRICTIONS.east
            }
        }
        , mapTypeControl: true
        , mapTypeControlOptions: {
            style: google.maps.MapTypeControlStyle.HORIZONTAL_BAR
            , position: google.maps.ControlPosition.TOP_CENTER
        }
        , zoomControl: true
        , zoomControlOptions: {
            position: google.maps.ControlPosition.LEFT_TOP
        }
        , scaleControl: true
        , streetViewControl: false
        , fullscreenControl: true
        , fullscreenControlOptions: {
            position: google.maps.ControlPosition.LEFT_TOP
        }
        , zoom: sima.App.DEFAULT_ZOOM
        , styles: googleMapsStyle
    };

    var mapEl = $('.map')
        .get(0);
    var map = new google.maps.Map(mapEl
        , mapOptions);
    sima.App.prototype.initShapeFiles();
    return map;
};

 /**
 * Actualiza el layout del mapa con los valores seleccionads en las diferentes opciones.
 * Este mapa esta anclado al domino a traves del elemento "map".
 * @param {{none}}
 * @return {{none}}
 */
sima.App.prototype.updateInformation = function () {

    if ($('#custom_toggle')
            .is(':checked')) {
        var dataset_id = $('#User_products')
            .val();
		if(dataset_id.length >0)
		{
			$.get(baseUrl + '/customData?mode_id=query&dataset_id=' + dataset_id)
				.done(
			function (data){
                if (data['title']) {
                    var title = data['title']
                } else{
                    var title = ""
                }
                if (data['classes']) {
                    var classes = data['classes']
                } else{
                    var classes = ""
                }
                if (data['desc']) {
                    var desc = data['desc']
                } else{
                    var desc = ""
                }
                if (data['source']) {
                    var source = data['source']
                } else{
                    var source = ""
                }
                $('#EO_info_title')
                    .html(title)
                $('#EO_info_desc')
                    .html(desc)
                $('#EO_info_content')
                    .html(classes)
                $('#EO_info_source')
                    .html(source)

             }
			);
		}
    } else {
        var info_val = $('#EOVisualizer')
            .val();
        var title = EO_data_info['products'][info_val].title;
        var desc = EO_data_info['products'][info_val].desc;
        var classes = EO_data_info['products'][info_val].classes;
        var source = EO_data_info['products'][info_val].source;
		$('#EO_info_title')
			.html(title);
		$('#EO_info_desc')
			.html(desc);
		$('#EO_info_content')
			.html(classes);
		$('#EO_info_source')
			.html(source);
		}
    }


/**
 * Actualiza los graficos a renderizar en pantalla.
 * @param {{none}}
 * @return {{none}}
 */
sima.App.prototype.updatePlots = function () {
    var info_val = $('#EOVisualizer')
        .val();
    // Gestion de series temporales
    if (EO_visualizator["products"][info_val].series_chart === "true" &&
        $("#serie_toggle")
        .is(':checked')) {
        sima.App.prototype.customTimeSeries('last', 'last', 'last');
    }
    // Gesiton de histogramas
    if (EO_visualizator["products"][info_val].histo_chart === "true" &&
        $("#histo_toggle")
        .is(':checked')) {
        sima.App.prototype.customHistogram('last', 'last', 'last');
    }
    // Gesiton de Pie charts
    if (EO_visualizator["products"][info_val].pie_chart === "true" &&
        $("#pie_toggle")
        .is(':checked')) {
        sima.App.prototype.customPieChart('last', 'last', 'last');
    }
}


/**
 * Carga en memoria la coleccion de json files requeridos para el procesado.
 * @param {{none}}
 * @return {{none}}
 */
sima.App.prototype.initShapeFiles = function () {
    $.get(baseUrl + '/shapefileData')
        .done((function (data) {
            if (data['error']) {
                console.log('No shapefiles available.');
            } else {
                shapefileData = data['resources'];
                sima.App.prototype.enableShapefileSelector(data);
            }
        }));
};

/**
 * Habilita los set de regiones disponibles en el sista para ser seleccionados.
 * @param {data_r} Set de regiones configuradas en AppEngine.
 * @return {{none}}
 */
sima.App.prototype.enableShapefileSelector = function (data_r) {
    var dropdown = $("#ShapeSelector");
    dropdown.empty();
    $.each(data_r, function (key, value) {
        $.each(value, function (key_inside, value_inside) {
            dropdown.append($("<option />")
                .val(key_inside)
                .text(key_inside));
        });
    });
}

/**
 * Actualiza la leyenda en pantalla acorde al producto seleccionado.
 * @param {{none}}
 * @return {{none}}
 */
sima.App.prototype.updateLegend = function () {
	var legend_val  = null;
    if ($('#custom_toggle').is(':checked')) 
	{
        legend_val = $('#User_products').val().split('/');
        legend_val = legend_val[legend_val.length - 1];
		legend_val = legend_val.length > 0 ? legend_val: null;
    }
	else
	{
        legend_val = $('#EOVisualizer').val();
        if (typeof legend_val === 'undefined' || legend_val === null) 
		{
                legend_val = 'transition';
		}
    }
	if(legend_val === null){
		$('#img-legend').hide();
	}
	else{
		$('#img-legend').html('<img src="' + baseUrl + '/www/img/legends/' + legend_val + '.png">');
		$('#img-legend').show();
	}
}

/**
 * Actualiza la leyenda en pantalla acorde al producto seleccionado.
 * @param {loadSlider} Tipo de datos selccionado. ej:"yearly"
 * @return {{none}}
 */
sima.App.prototype.updateEOMapType = function (loadSlider) {
	
	$("#maploading").show();
    $('#EOVisualizer')
        .attr('disabled', true);
    $('#EO_products')
        .attr('disabled', true);
    $("#menu_loading")
        .show();
    var product_id = $('#EOVisualizer')
        .val();

    if (typeof product_id === 'undefined' || product_id === null) {
        product_id = 'transition';
    }
    var eo_type = EO_visualizator["products"][product_id].type;
    var eo_max = EO_visualizator["products"][product_id].max;
    var eo_min = EO_visualizator["products"][product_id].min;
    var eo_palette = EO_visualizator["products"][product_id].palette.colors;
    var dataset_id = EO_visualizator["products"][product_id].product;
    var startdate_id = EO_visualizator["products"][product_id].dateIni;
    var enddate_id = EO_visualizator["products"][product_id].dateFin;
    var date_scale = EO_visualizator["products"][product_id].date_scale;
    var masking = EO_visualizator["products"][product_id].masking;
    var str_palette = sima.App.prototype.palette2string(eo_palette);

    if (masking === 'yes') {
        $("#masking_control")
            .show();
    } else {
        $("#masking_control")
            .hide();
        if ($('#masking_toggle')
            .is(':checked')) {
            $('#masking_toggle')
                .trigger('click');
        }
    }
    var masking_toggle = $("#masking_toggle")
        .is(':checked');
    var group_id = EO_visualizator["products"][product_id].group;

    var day_start_id = '1980-01-01';
    var day_end_id = '2015-12-31';
    var agregation_type = 'single';

    var selected_value = $("input[name='date_type_selector']:checked")
        .val();
    if (selected_value === 'range') {
        agregation_type = $("#rangeAggregationTypeSelector")
            .val();
    }
    if (loadSlider) {
        if (date_scale === "yearly") {
            var startYear = Math.abs(parseInt(startdate_id));
            var endYear = Math.abs(parseInt(enddate_id));
            sima.App.prototype.setYearSlider();
            selectSingleDateSlider();
            day_start_id = startYear + '-01-01';
            day_end_id = endYear + '-12-31';
        } else if (date_scale === "landsat") {
            sima.App.prototype.setLandsatSlider();
            selectSingleDateSlider();
            var dateParts = enddate_id.split('-');
            var year = dateParts[0];
            var month = parseInt(dateParts[1]) - 1;
			if(month ===0)
			{
				month = 12;
				year = year - 1;
			}
            var lastDayMonth = getLastDayMonthnth(month);
            var monthStr = month;
            if (monthStr < 10) {
                monthStr = '0' + monthStr;
            }
            day_start_id = year + '-' + monthStr + '-01';
            day_end_id = year + '-' + monthStr + '-' + lastDayMonth;
        }
    } else {
        if (date_scale === "yearly") {
            if (agregation_type === 'single') {
                day_start_id = Math.abs(parseInt($("#slider-range")
                    .slider("value"))) + '-01-01';
                day_end_id = Math.abs(parseInt($("#slider-range")
                    .slider("value"))) + '-12-31';
            } else {
                var selectedValues = $("#slider-range")
                    .slider("values");
                day_start_id = Math.abs(parseInt(selectedValues[0])) + '-01-01';
                day_end_id = Math.abs(parseInt(selectedValues[1])) + '-12-31';
            }
        } else if (date_scale === "landsat") {
            if (agregation_type === 'single') {
                day_start_id = landsatDates[parseInt($("#slider-range")
                    .slider("value"))] + '-01';
                var dateParts = day_start_id.split('-');
                var year = dateParts[0];
                var month = dateParts[1];
                var lastDayMonth = getLastDayMonthnth(month);
                day_end_id = year + '-' + month + '-' + lastDayMonth;

            } else {
                var selectedValues = $("#slider-range")
                    .slider("values");
                day_start_id = landsatDates[parseInt(selectedValues[0])] + '-01';
                var dateParts = landsatDates[parseInt(selectedValues[1])].split('-');
                var year = dateParts[0];
                var month = dateParts[1];
                var lastDayMonth = getLastDayMonthnth(month);
                day_end_id = landsatDates[parseInt(selectedValues[1])] + '-' + lastDayMonth;
            }
        }
    }
    EO_visualizator["products"][product_id]['day_start_id'] = day_start_id
    EO_visualizator["products"][product_id]['day_end_id'] = day_end_id
    loading_map = true;
	setInterval(function(){$("#maploading").hide();  }, 10000);
    $.get(baseUrl + '/eoData?product_id=' + product_id + '&dataset_id=' + dataset_id + '&type_id=' + eo_type + '&min_id=' + eo_min + '&startdate_id=' + day_start_id + '&enddate_id=' + day_end_id + '&group_id=' + group_id + '&day_start_id=' + day_start_id + '&day_end_id=' + day_end_id + '&max_id=' + eo_max + '&palette_id=' + str_palette + "&masking_toggle=" + masking_toggle + "&agregation_type=" + agregation_type)
        .done((function (data) {
            if (data['eeMapId']) {
                var neeMapId = data['eeMapId'];
                var ntoken = data['eeToken'];
                cachekey = data['cachekey'];
                mapType = sima.App.getEeMapType(neeMapId, ntoken);
                sima.App.prototype.addLayer(mapType);
                loading_map = false;				
                $('#rangeApply')
                    .attr('disabled', false);
            } else {
                console.log(' No mapid ');
            }
            $('#EOVisualizer')
                .attr('disabled', false);
            $('#EO_products')
                .attr('disabled', false);
            $("#menu_loading")
                .hide();
        }));
};

 /**
 * Habilita el selector de rango.
 * @param {{none}}
 * @return {{none}}
 */
sima.App.prototype.activateRangeSlider = function () {
    var product_id = $('#EOVisualizer')
        .val();
    var date_scale = EO_visualizator["products"][product_id].date_scale;
    if (date_scale === "yearly") {
        sima.App.prototype.setYearRangeSlider();
    }
    if (date_scale === "landsat") {
        sima.App.prototype.setLandsatRangeSlider();
    }
}


 /**
 * Habilita el selector de rango unico.
 * @param {{none}}
 * @return {{none}}
 */
sima.App.prototype.activateSingleSlider = function () {
    var product_id = $('#EOVisualizer')
        .val();
    if (typeof product_id === 'undefined' || product_id === null) {
        product_id = 'transition';
    }
    var date_scale = EO_visualizator["products"][product_id].date_scale;
    if (date_scale === "yearly") {
        sima.App.prototype.setYearSlider();
    }
    if (date_scale === "landsat") {
        sima.App.prototype.setLandsatSlider();
    }
}


 /**
 * Configura los valores disponibles en selector de transicion para indices de agua.
 * @param {{none}}
 * @return {{none}}
 */
sima.App.prototype.setLandsatSlider = function () {
    var product_id = $('#EOVisualizer')
        .val();
    if (typeof product_id === 'undefined' || product_id === null) {
        product_id = 'transition';
    }
    var startdate_id = EO_visualizator["products"][product_id].dateIni;
    var enddate_id = EO_visualizator["products"][product_id].dateFin;

    var minyear = parseInt(startdate_id.split('-')[0]);
    var maxyear = parseInt(enddate_id.split('-')[0]);
    var minmonth = parseInt(startdate_id.split('-')[1]);
    var maxmonth = parseInt(enddate_id.split('-')[1]);
    landsatDates = [];
    for (var cy = minyear; cy <= maxyear; cy++) {
        for (var cm = 1; cm <= 12; cm++) {
            if (cy === minyear && cm < minmonth) {

            } else if (cy === maxyear && cm >= maxmonth) {

            } else {
                var cmStr = cm;
                if (cm < 10) {
                    cmStr = '0' + cm;
                }
                landsatDates.push(cy + '-' + cmStr);
            }
        }
    }
    $("#rangeAggregationTypeSelector")
        .hide();
    var currValue = getYearMonth(landsatDates[landsatDates.length - 1]);
    $("#rangeValue")
        .html(currValue.replace('-', '/'));
    if ($("#slider-range")
        .slider("instance") !== undefined) {
        $("#slider-range")
            .slider("destroy");
    }
    $("#slider-range")
        .slider({
            min: 0
            , max: landsatDates.length - 1
            , value: landsatDates.length - 1
            , step: 1
            , slide: function (event, ui) {
                $("#rangeValue")
                    .html(landsatDates[ui.value].replace('-', '/'));
            }
        });
}

/**
 * Configura los valores disponibles en selector de rango para indices de agua.
 * @param {{none}}
 * @return {{none}}
 */
sima.App.prototype.setLandsatRangeSlider = function () {
    updateRangeAggregationTypeSelector('landsat');
    var product_id = $('#EOVisualizer')
        .val();
    var startdate_id = EO_visualizator["products"][product_id].dateIni;
    var enddate_id = EO_visualizator["products"][product_id].dateFin;

    var minyear = parseInt(startdate_id.split('-')[0]);
    var maxyear = parseInt(enddate_id.split('-')[0]);
    var minmonth = parseInt(startdate_id.split('-')[1]);
    var maxmonth = parseInt(enddate_id.split('-')[1]);
    landsatDates = [];
    for (var cy = minyear; cy <= maxyear; cy++) {
        for (var cm = 1; cm <= 12; cm++) {
            if (cy === minyear && cm < minmonth) {

            } else if (cy === maxyear && cm >= maxmonth) {

            } else {
                var cmStr = cm;
                if (cm < 10) {
                    cmStr = '0' + cm;
                }
                landsatDates.push(cy + '-' + cmStr);
            }
        }
    }
    var minv = landsatDates[landsatDates.length - 10];
    var maxv = landsatDates[landsatDates.length - 1];
    $("#rangeAggregationTypeSelector")
        .show();
    $("#rangeValue")
        .html(minv.replace('-', '/') + ' - ' + maxv.replace('-', '/'));
    if ($("#slider-range")
        .slider("instance") !== undefined) {
        $("#slider-range")
            .slider("destroy");
    }

    $("#slider-range")
        .slider({
            range: true
            , min: 0
            , max: (landsatDates.length - 1)
            , values: [landsatDates.length - 10, (landsatDates.length - 1)]
            , step: 1
            , slide: function (event, ui) {
                if (Math.abs(ui.values[0] - ui.values[1]) > 12) {
                    if (ui.handleIndex === 0) {
                        updateSlider(ui.values[0], Math.min(ui.values[0] + 12, (landsatDates.length - 1)));
                    }

                    if (ui.handleIndex === 1) {
                        updateSlider(Math.max(0, ui.values[1] - 12), ui.values[1]);
                    }
                }


                $("#rangeValue")
                    .html(landsatDates[ui.values[0]].replace('-', '/') + ' - ' + landsatDates[ui.values[1]].replace('-', '/'));
            }
        });
}

/**
 * Configura los valores disponibles en selector temporal en los porductos que lo admiten.
 * @param {{none}}
 * @return {{none}}
 */
sima.App.prototype.setYearSlider = function () {
    var product_id = $('#EOVisualizer')
        .val();
    var startdate_id = EO_visualizator["products"][product_id].dateIni;
    var enddate_id = EO_visualizator["products"][product_id].dateFin;
    var minv = Math.abs(parseInt(startdate_id));
    var maxv = Math.abs(parseInt(enddate_id));
    $("#rangeAggregationTypeSelector")
        .hide();
    $("#rangeValue")
        .html(maxv + "/01/01" + ' - ' + maxv + "/12/31");
    if ($("#slider-range")
        .slider("instance") !== undefined) {
        $("#slider-range")
            .slider("destroy");
    }
    $("#slider-range")
        .slider({
            min: minv
            , max: maxv
            , value: maxv
            , step: 1
            , slide: function (event, ui) {
                $("#rangeValue")
                    .html(ui.value + "/01/01" + ' - ' + ui.value + "/12/31");
            }
        });
}

/**
 * Configura los rangos de valores disponibles para el selector temporal.
 * @param {{none}}
 * @return {{none}}
 */
sima.App.prototype.setYearRangeSlider = function () {
    updateRangeAggregationTypeSelector('normal');
    var product_id = $('#EOVisualizer')
        .val();
    var startdate_id = EO_visualizator["products"][product_id].dateIni;
    var enddate_id = EO_visualizator["products"][product_id].dateFin;
    var minv = Math.abs(parseInt(startdate_id));
    var maxv = Math.abs(parseInt(enddate_id));
    $("#rangeAggregationTypeSelector")
        .show();
    $("#rangeValue")
        .html(minv + "/01/01" + ' - ' + maxv + "/12/31");
    console
    if ($("#slider-range")
        .slider("instance") !== undefined) {
        $("#slider-range")
            .slider("destroy");
    }
    $("#slider-range")
        .slider({
            range: true
            , min: minv
            , max: maxv
            , values: [minv, maxv]
            , step: 1
            , slide: function (event, ui) {
                $("#rangeValue")
                    .html(ui.values[0] + "/01/01" + ' - ' + ui.values[1] + "/12/31");
            }
        });
}

/**
 * Convierte la paleta de colores a string.
 * @param {eo_palette} Listado Javascrip0t con los valores de colores para un producto determinado.
 * @return {{none}}
 */
sima.App.prototype.palette2string = function (eo_palette) {
    var blkstr = [];
    $.each(eo_palette, function (idx2, val2) {
        var str = val2;
        blkstr.push(str);
    });
    return blkstr.join(",");
}

/**
 * Agrega un producto satelital indicado al mapa.
 * @param {mapType} Tipo de mapa compatible con Google Maps a renderizar en pantalla.
 * @return {{none}}
 */
sima.App.prototype.addLayer = function (mapType) {
    map_id.overlayMapTypes.push(mapType);
}


/**
 * Elimina todas las capas desplegadas sobre el mapa.
 * @param {index} Indice de capa rendereizada a eliminar.
 * @return {{none}}
 */
sima.App.prototype.clearMap = function () {
    map_id.overlayMapTypes.clear();
}

/**
 * Actauliza el valor de opacidad asociada a la capa de productos..
 * @param {opacity_value} Nuevo valor de opacidad.
 * @return {{none}}
 */
sima.App.prototype.changeOpacity = function (opacity_value) {
    var opacity_level = parseFloat(opacity_value / 100);
    map_id.overlayMapTypes.getAt(0)
        .setOpacity(opacity_level)
}


/**
 * Deshabilita la funcionalidad de pixel
 * @param {{none}} 
 * @return {{none}}
 */
sima.App.prototype.disablePixel = function () {
    map_id.setOptions({
        draggableCursor: 'default'
    });
    if (markers.length >= 1) {
        marker.setMap(null);
    }
}

/**
 * Habilita la funcionalidad de pixel
 * @param {{none}} 
 * @return {{none}}
 */
sima.App.prototype.enablePixel = function () {
    map_id.setOptions({
        draggableCursor: 'help'
    });
}


/**
 * Gestion de recursos, diferencia entre dos modos, lectura o escritura.
 * @param {mode_id} read|write.
 * @return {{none}}
 */
sima.App.prototype.manageResources = function (mode_id) {
    if (mode_id == 'write') {
        var dataset_id = $('#User_products')
            .val();
        if (dataset_id === null || dataset_id.length === 0) {
            $("#menu_loading")
                .hide();
        } else {
            $('#User_products')
                .attr('disabled', true);
            $("#menu_loading")
                .show();
            $.get(baseUrl + '/customData?mode_id=' + mode_id + '&dataset_id=' + dataset_id)
                .done((function (data) {
                    if (data['eeMapId']) {
                        var neeMapId = data['eeMapId'];
                        var ntoken = data['eeToken'];
                        cachekey = data['cachekey'];
                        mapType = sima.App.getEeMapType(neeMapId, ntoken);
                        sima.App.prototype.addLayer(mapType);
                        $('#rangeApply')
                            .attr('disabled', false);
                    } else {
                        console.log('No mapid');
                    }
                    $('#User_products')
                        .attr('disabled', false);
                    $("#menu_loading")
                        .hide();
                }));
        }
    } else {
        $.get(baseUrl + '/customData?mode_id=' + mode_id)
            .done((function (data) {
                if (data['error']) {
                    var dropdown = $("#User_products");
                    dropdown.append($("<option />")
                        .val(null)
                        .text('No hay capas de usuario'));
                } else {
                    if (data['resources'].length > 0) {
                        sima.App.prototype.enableCustomSelector(data['resources']);
                    } else {
                        var dropdown = $("#User_products");
                        dropdown.append($("<option />")
                            .val(null)
                            .text('No hay capas de usuario'));
                    }
                }
                $("#menu_loading")
                    .hide();
            }));
    }
}

/**
 * Agreg datos custom de usuario al front end.
 * @param {data_r} Datos proporcionados por el backend.
 * @return {{none}}
 */

sima.App.prototype.enableCustomSelector = function (data_r) {
    var dropdown = $("#User_products");
    dropdown.empty();
    $.each(data_r, function (key, value) {
        dropdown.append($("<option />")
            .val(value.id)
            .text(value.name));
    });
}


 /**
 * Agrega los poligonos basados en in id en pantalla
 * @param {Array<string>} IDs de los poligonos a mostrar en pantalla.
 * @return {{none}}
 */
sima.App.prototype.addPolygons = function (polygonIds, basePath) {
	$("#maploading").show();
    if (this.map) {} else {
        this.map = map_id;
    }
    if (shapefile_features_added.length > 0) {
        for (var i = 0; i < shapefile_features_added.length; i++) {
            this.map.data.remove(shapefile_features_added[i]);
        }
        shapefile_features_added = [];
    }
    selected_shapefile = basePath;
	var totalPols = polygonIds.length;
	var loadedPols = 0;
    polygonIds.forEach((function (polygonId) {
            var features = this.map.data.loadGeoJson(baseUrl + '/www/shapefiles/' + basePath + '/' + polygonId + '.json', {}
                , function (features) {
                    shapefile_features_added.push(features[0]);
					loadedPols = loadedPols + 1;
					if(loadedPols === totalPols)
					{
						$("#maploading").hide();
					}
                }
            );

        })
        .bind(this));

    this.map.data.setStyle(function (feature) {
        return {
            fillColor: shape_color
            , fillOpacity: '0'
            , strokeColor: shape_color
            , strokeWeight: 2
        };
    });
	
};


/**
* Oculta los shapes de regiones en el mapa
* @param {{none}} 
* @return {{none}}
*/
sima.App.prototype.hideStates = function () {
    map_id.data.setStyle({
        visible: false
    });
}

/**
* Muestra los shapes de regiones en el mapa
* @param {{none}} 
* @return {{none}}
*/
sima.App.prototype.showStates = function () {

    map_id.data.setStyle({
        visible: true
    });
    map_id.data.setStyle(function (feature) {
        return {
            fillColor: 'black'
            , fillOpacity: '0'
            , strokeColor: shape_color
            , strokeWeight: 1
        };
    });
}


/**
* Habilita la herramienta de dibujado
* @param {{none}} 
* @return {{none}}
*/
sima.App.prototype.drawPolygon = function () {
    drawingManager = new google.maps.drawing.DrawingManager({
        drawingMode: google.maps.drawing.OverlayType.POLYGON
        , drawingControl: false
        , polygonOptions: {
            fillColor: shape_color
            , strokeColor: shape_color
        }
    });
    google.maps.event.addListener(drawingManager, 'overlaycomplete'
        , function (event) {
            if (event.overlay &&
                event.overlay.latLngs &&
                event.overlay.latLngs.j &&
                event.overlay.latLngs.j.length > 0 &&
                event.overlay.latLngs.j[0].j &&
                event.overlay.latLngs.j[0].j.length === 2) {
                alert("Polygon should have at least three sides");
                currentPolygon = event.overlay;
                sima.App.prototype.clearPolygon();
            } else {
                sima.App.prototype.setPolygon(event.overlay);
            }

        });
    drawingManager.setMap(map_id);
}

/**
* Deshabilita la herramienta de dibujado
* @param {{none}} 
* @return {{none}}
*/
sima.App.prototype.disablePolygon = function () {
    try {
        if (currentPolygon) {
            currentPolygon.setMap(null);
        }
    } catch (err) {
        console.log(err);
    }

    drawingManager.setOptions({
        drawingMode: null
    });
}

/**
* Elmimina el poligono selecccionado
* @param {{none}} 
* @return {{none}}
*/
sima.App.prototype.clearPolygon = function () {
    if (currentPolygon) {
        currentPolygon.setMap(null);
        currentPolygon = undefined;
    }
    drawingManager.setOptions({
        drawingMode: google.maps.drawing.OverlayType.POLYGON
    });
};

/**
* Define y renderiza el PIe Chart en pantalla
* @param {pie_data} Datos a representar en el grafico
* @param {desc} Descripcion a incluir en el grafico.
* @param {region_name} Nombre de la region selccionada y a representar.
* @param {product_id}  Id del producto sobre el que estmaos trabajando.
* @return {{none}}
*/
sima.App.prototype.showHighPieChart = function (pie_data, desc, region_name, product_id) {
    chart_data = sima.App.prototype.getPieData(pie_data, product_id);
    palette_data = sima.App.prototype.getPiePalette(pie_data, product_id);
    var product_id = $('#EOVisualizer')
        .val();
    Highcharts.chart('pie-div', {
        chart: {
            plotBackgroundColor: 'transparent'
            , backgroundColor: 'transparent'
            , plotBorderWidth: null
            , plotShadow: false
            , type: 'pie'
        },
        credits: {
            enabled: false
        }
        , subtitle: {
            text: desc
            , style: {
                font: '16pt Trebuchet MS, Verdana, sans-serif'
                , color: '#2b2b2b'
            }
        , }
        , title: {
            text: null
        }
        , legend: {
            itemStyle: {
                font: '9pt Trebuchet MS, Verdana, sans-serif'
                , color: '#2b2b2b'
            }
            , itemHoverStyle: {
                color: '#FFF'
            }
            , itemHiddenStyle: {
                color: '#444'
            }
        },

        tooltip: {
            pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
        }
        , plotOptions: {
            pie: {
                colors: palette_data
                , allowPointSelect: true
                , color: 'red'
                , cursor: 'pointer'
                , dataLabels: {
                    enabled: false
                , }
                , showInLegend: true,

            }
        }
        , series: [{
            name: 'Total'
            , colorByPoint: true
            , data: chart_data
      }], exporting: {
            filename: product_id+'_export_data',
            xls: {
                columnHeaderFormatter: function(item, key) {
                    if (!key) {
                        return 'custom title'
                    }
                    return false
                }
            },
            buttons: {
                contextButton: {
                    menuItems: ['downloadJPEG', 'downloadPDF', 'downloadCSV', 'downloadXLS'],
                    align: 'left',
                    verticalAlign: 'top'
                }
            }
        }

    });
};


/**
* Define y renderiza el PIe Chart en pantalla
* @param {ts_valid} Numerp de pixeles validos representables.
* @param {ts_value} Datos a visualizar del producto seleccionado
* @param {desc} Descripcion a incluir en el grafico.
* @param {wrapper_class}  Wrapper donde se escirbira el codigo resultante de la funcion.
* @param {region_name} Nombre de la region selccionada y a representar.
* @param {y1_name} Nombre eje principal
* @param {y2_name} Nombre eje secundario
* @return {{none}}
*/
sima.App.prototype.showHighSeriesDual = function (ts_valid, ts_value, desc, wrapper_class, region_name, y1_name, y2_name) {
    Highcharts.chart(wrapper_class, {
        chart: {
            zoomType: 'xy'
            , backgroundColor: 'transparent'
        , }
        , credits: {
            enabled: false
        }
        , title: {
            text: null
        }
        , subtitle: {
            text: desc
            , style: {
                font: '16pt Trebuchet MS, Verdana, sans-serif'
                , color: '#2b2b2b'
            }
        , }
        , xAxis: {
            type: 'datetime'
            , gridLineColor: 'rgba(43,43,43,0.3)'
            , gridLineWidth: 1
            , labels: {
                style: {
                    font: '9pt Trebuchet MS, Verdana, sans-serif'
                    , color: '#2b2b2b'
                }
            }
        }
        , yAxis: [{ // Eje Y principal
            title: {
                text: y1_name
                , style: {
                    color: Highcharts.getOptions()
                        .colors[0]
                }
            }
            , labels: {
                format: '{value}'
                , style: {
                    color: Highcharts.getOptions()
                        .colors[0]
                }
            }
      }, { // Eje Y secundario
            title: {
                text: y2_name
                , style: {
                    color: Highcharts.getOptions()
                        .colors[1]
                }
            }
            , labels: {
                format: '{value}'
                , style: {
                    color: Highcharts.getOptions()
                        .colors[1]
                }
                , min: 0
                , max: 100
            }
            , opposite: true
      }]
        , tooltip: {
            shared: true,
            valueDecimals: 2
        }
        , series: [
            {
                name: y1_name
                , type: 'column'
                , data: ts_value
                , tooltip: {
                    valueSuffix: ''
                }
          }
            , {
                name: y2_name
                , yAxis: 1
                , type: 'spline'
                , data: ts_valid
                , tooltip: {
                    valueSuffix: ''
                }
      }], exporting: {
            buttons: {
                contextButton: {
                    menuItems: ['downloadJPEG', 'downloadPDF', 'downloadCSV', 'downloadXLS']
                }
            }
        }
    });
}

/**
* Formatea datos provenientes de AppEngine para poder ser representados en histograma.
* @param {histo_data} Datos histograma procedentes de backend.
* @return {formatted_data} Datos formateados
*/
sima.App.prototype.getHistoData = function (histo_data) {
    var formatted_data = [];

    for (i = 0; i < histo_data.properties.values.length; i++) {
        formatted_data.push([histo_data.properties.classes[i], histo_data.properties.values[i]])
    }
    return formatted_data;
}


/**
* Formatea datos de paleta de colores provenientes de AppEngine para poder ser representados en Pie Chart.
* @param {pie_data} Datos Pie Chart procedentes de backend.
* @param {product_id} Id del producto porcesado.
* @return {formatted_data} Datos formateados
*/
sima.App.prototype.getPiePalette = function (pie_data, product_id) {
    var formatted_data = [];
    if (typeof (EO_visualizator['products'][product_id]['palette']['colors_dict']) !== 'undefined') {
        for (i = 0; i < pie_data.properties.values.length; i++) {
            formatted_data.push('#' + EO_visualizator['products'][product_id]['palette']['colors_dict'][pie_data.properties.values[i].transition_class_value]);
        }
    } else {
        for (i = 0; i < pie_data.properties.values.length; i++) {
            formatted_data.push('#' + EO_visualizator['products'][product_id]['palette']['colors'][pie_data.properties.values[i].transition_class_value]);
        }
    }
    return formatted_data;
}

/**
* Formatea datos provenientes de AppEngine para poder ser representados en Pie Chart.
* @param {pie_data} Datos Pie Chart procedentes de backend.
* @param {product_id} Id del producto porcesado.
* @return {formatted_data} Datos formateados
*/
sima.App.prototype.getPieData = function (pie_data, product_id) {
    var formatted_data = [];
    for (i = 0; i < pie_data.properties.values.length; i++) {
        formatted_data.push({
            name: EO_visualizator['products'][product_id]['classes'][pie_data.properties.values[i].transition_class_value]
            , y: pie_data.properties.values[i].sum
        })
    }
    return formatted_data;
}



/**
* Define y renderiza el Histograma en pantalla
* @param {histo_data} Datos a visualizar del producto seleccionado
* @param {desc} Descripcion a incluir en el grafico.
* @param {wrapper_class}  Wrapper donde se escirbira el codigo resultante de la funcion.
* @param {region_name} Nombre de la region selccionada y a representar.
* @return {{none}}
*/
sima.App.prototype.showHighHistogram = function (histo_data, desc, wrapper_class, region_name) {
    chart_data = sima.App.prototype.getHistoData(histo_data);

    Highcharts.chart(wrapper_class, {
        chart: {
            type: 'column'
            , backgroundColor: 'transparent'
        , }
        , credits: {
            enabled: false
        }
        , title: {
            text: null
        }
        , subtitle: {
            text: desc
            , style: {
                font: '16pt Trebuchet MS, Verdana, sans-serif'
                , color: '#2b2b2b'
            }
        }
        , xAxis: {
            type: 'category'
            , labels: {
                rotation: -45
                , style: {
                    font: '9pt Trebuchet MS, Verdana, sans-serif'
                    , color: '#2b2b2b'
                }
            }
        }
        , yAxis: {
            min: 0
            , gridLineColor: 'rgba(43,43,43,0.3)'
            , gridLineWidth: 1
            , title: {
                text: 'Frecuencia'
            }
            , style: {
                font: '9pt Trebuchet MS, Verdana, sans-serif'
                , color: '#2b2b2b'
            }
            , labels: {
                style: {
                    font: '9pt Trebuchet MS, Verdana, sans-serif'
                    , color: '#2b2b2b'
                }
            }
        }
        , legend: {
            enabled: false
        }
        , tooltip: {
            pointFormat: desc
        }
        , series: [{
            name: 'Count - ' + region_name
            , data: chart_data
            , dataLabels: {
                enabled: true
                , rotation: -90
                , color: '#FFFFFF'
                , align: 'right'
                , format: '{point.y:.1f}', // un decimal
                y: 10, // 10 pixels desde abajo hasta el top

            }
    }], exporting: {
            buttons: {
                contextButton: {
                    menuItems: ['downloadJPEG', 'downloadPDF', 'downloadCSV', 'downloadXLS']
                }
            }
        }
    });

}

/**
* Devuelve el valor del pixel de un proucto seleccionado.
* @param {polygon_s} Id del tipo de dato a analizar (pixel)
* @param {mode} Modo de ejecucion.
* @return {{none}}
*/
sima.App.prototype.customPixelValue = function (polygon_s, mode) {

    var prod_selected = $('#EOVisualizer')
        .val();
    var type_id = EO_visualizator["products"][prod_selected].type;
    var scale_id = EO_visualizator["products"][prod_selected].scale;
    var band_id = EO_visualizator["products"][prod_selected].band;
    var dataset_id = EO_visualizator["products"][prod_selected].product;
    var eo_max = EO_visualizator["products"][prod_selected].max;
    var eo_min = EO_visualizator["products"][prod_selected].min;
    var productHasClasses = EO_visualizator["products"][prod_selected].classes !== undefined;
    if ($('#custom_toggle')
        .is(':checked')) {
        productHasClasses = false;
    }
    $.get(baseUrl + '/pixelValue?shapefile_id=' + selected_shapefile + '&polygon_id=' +
            polygon_s + '&max_id=' + eo_max + '&min_id=' + eo_min + '&dataset_id=' +
            dataset_id + '&band_id=' + band_id + '&mode_id=' + mode + '&scale_id=' +
            scale_id + '&cachekey=' + cachekey
        )
        .done((
            function (data) {
                if (productHasClasses) {
                    var selected_value = $("input[name='date_type_selector']:checked")
                        .val();
                    var selected_value_option = $("#rangeAggregationTypeSelector")
                        .val();
                    var valorData = EO_visualizator["products"][prod_selected].classes[data];
                    if (valorData === undefined || typeof (valorData) === 'undefined' || valorData === null) {
                        valorData = 'No definido';
                        $('#value-wrapper')
                            .html('Valor: ' + valorData);
                    } else {
                        $('#value-wrapper')
                            .html('Valor: ' + valorData);
                    }
                } else {
                    if (data === undefined || typeof (data) === 'undefined' || data === null) {
                        data = 'No definido';
                    }
                    var selected_value = $("input[name='date_type_selector']:checked")
                        .val();
                    if (selected_value === 'single') {
                        $('#value-wrapper')
                            .html('Valor: ' + data);
                    }
                }
            }
        ));
}

/**
* Calcula el area de un determinado poligono.
* @param {polygon_s} Id del tipo de dato a analizar (pixel)
* @param {mode} Modo de ejecucion.
* @return {{none}}
*/
sima.App.prototype.customPolygonArea = function (polygon_s, mode) {
    $.get(baseUrl + '/polygonArea?shapefile_id=' + selected_shapefile + '&polygon_id=' + polygon_s + '&mode_id=' + mode)
        .done((function (data) {
            $('#value-wrapper')
                .html('Superficie Total Seleccionada: ' + data);
            $("#value-wrapper")
                .show();
        }));

}

/**
* Define y renderiza un Histograna en pantalla
* @param {polygon_s} Tipo de region o estructura a analizar.
* @param {region_name} Nombre de la region selccionada y a representar.
* @return {{none}}
*/
sima.App.prototype.customHistogram = function (polygon_s, mode, region_name) {
    var flagHisto = false;
    if ($('#analysis')
        .hasClass('active')) {
        flagHisto = true;
    } else if ($('#options')
        .hasClass('active')) {
        var prod_selected = $('#EOVisualizer')
            .val();
        var prod_text = $('#EOVisualizer option:selected')
            .text();
        var products = EO_visualizator.products;
        if (products[prod_selected].histo_chart == "true") {
            $('#EOVisualizer')
                .val(prod_selected);
            flagHisto = true;
        } else {
           $('#histo-wrapper')
                .html(' ');
        }
    }

    if (flagHisto) {
        var histo_str = $('#EOVisualizer')
            .val();
        var histo_desc = $("#EOVisualizer")
            .find('option:selected')
            .text();
        var band_id = EO_visualizator["products"][histo_str].band;
        var dataset_id = EO_visualizator["products"][histo_str].product;
        var eo_max = EO_visualizator["products"][histo_str].max;
        var eo_min = EO_visualizator["products"][histo_str].min;
        sima.App.setSpinner('on');
        $('#histo-wrapper')
            .html('<div class="loader"></div>');

        var ajax_url_get = baseUrl + '/histogram?shapefile_id=' + selected_shapefile + '&polygon_id=' + polygon_s + '&max_id=' + eo_max + '&min_id=' + eo_min + '&dataset_id=' + dataset_id + '&band_id=' + band_id + '&mode_id=' + mode

        $.ajax({
            type: "get"
            , url: ajax_url_get
            , success: function (data, text) {
                if (data['error']) {
                    sima.App.setSpinner('off');
                    $('#histo-wrapper')
                        .html(data['error']);
                } else {
                    sima.App.prototype.showHighHistogram(data, histo_desc, 'histo-wrapper', region_name);
                    sima.App.setSpinner('off');
                }
            }
            , error: function (request, status, error) {
                $('#histo-wrapper')
                    .html(request.responseText);
                sima.App.setSpinner('off');
            }
        });
    }

}

/**
* Define y renderiza un PieChart en pantalla
* @param {polygon_s} Tipo de region o estructura a analizar.
* @param {mode} Moo o tipo de ejecuion a realizar en el backend.
* @param {region_name} Nombre de la region selccionada y a representar.
* @return {{none}}
*/
sima.App.prototype.customPieChart = function (polygon_s, mode, region_name) {
    var agg_id = $("#rangeAggregationTypeSelector")
        .val();
    var pie_str = $('#EOVisualizer')
        .val();
    var pie_desc = $("#EOVisualizer")
        .find('option:selected')
        .text();
    var band_id = EO_visualizator["products"][pie_str].band;
    var dataset_id = EO_visualizator["products"][pie_str].product;
    var type_id = EO_visualizator["products"][pie_str].type;
    var startdate_id = EO_visualizator["products"][pie_str].day_start_id;
    var enddate_id = EO_visualizator["products"][pie_str].day_end_id;
    if ($('#layer_toggle')
        .is(':checked')) {
        pie_data_type = 'product';
        if (EO_visualizator["products"][pie_str].pie_chart !== "true") {
            return false;
        }
    } else if ($('#custom_toggle')
        .is(':checked')) {
        pie_data_type = 'user';
        return false;
    } else {
        return false;
    }
    sima.App.setSpinner('on');
    $('#pie-div')
        .html('<div  class="loader"></div>');
    var ajax_url_get = baseUrl + '/pieChart?shapefile_id=' + selected_shapefile + '&polygon_id=' + polygon_s + '&dataset_id=' + dataset_id + '&band_id=' + band_id + '&mode_id=' + mode + '&data_source=' + pie_data_type +
        '&type_id=' + type_id + "&startdate_id=" + startdate_id + "&enddate_id=" + enddate_id + "&agg_id=" + agg_id
    var draw_pieChart = true;
    if ((EO_visualizator["products"][$('#EOVisualizer')
            .val()].date_scale !== 'none') && (agg_id == 'promedio' || agg_id == 'diferencia')) {
        draw_pieChart = false;
    }
    if (draw_pieChart === true) {
        $.ajax({
            type: "get"
            , url: ajax_url_get
            , success: function (data, text) {
                if (data['error']) {
                    sima.App.setSpinner('off');
                    $('#pie-div')
                        .html(data['error']);
                } else {
                    sima.App.prototype.showHighPieChart(data, pie_desc, region_name, band_id);
                    sima.App.setSpinner('off');
                }
            }
            , error: function (request, status, error) {
                $('#pie-div')
                    .html(request.responseText);
                sima.App.setSpinner('off');
            }
        });
    } else {
        $('#pie-div')
            .html('Pie chart no disponible para la opcion: ' + agg_id);
    }
}

/**
* Define y renderiza una serie temporal en pantalla
* @param {polygon_s} Tipo de region o estructura a analizar.
* @param {mode} Moo o tipo de ejecuion a realizar en el backend.
* @param {region_name} Nombre de la region selccionada y a representar.
* @return {{none}}
*/
sima.App.prototype.customTimeSeries = function (polygon_s, mode, region_name) {
    var flagSeries = false;
    var prod_selected = $('#EOVisualizer')
        .val();
    var prod_text = $('#EOVisualizer option:selected')
        .text();
    var products = EO_visualizator.products;
    var selected_value = $("input[name='date_type_selector']:checked")
        .val();
    if (EO_visualizator["products"][prod_selected].series_chart !== "true") {
        return false;
    }
    if (products[prod_selected].series_chart == "true") {
        flagSeries = true;
    } else {
        return false;
    }
    if (flagSeries) {
        var series_str = $('#EOVisualizer')
            .val();
        var series_desc = $("#EOVisualizer")
            .find('option:selected')
            .text();
        if ($('#layer_toggle')
            .is(':checked')) {
            $('#value-wrapper')
                .html('');
            sima.App.setSpinner('on');
            $('#series-wrapper')
                .html('<div class="loader"></div>');
            var scale_id = products[prod_selected].scale;
            var band_id = products[prod_selected].band;
            var dataset_id = products[prod_selected].product;
            var startdate_id = products[prod_selected].day_start_id;
            var enddate_id = products[prod_selected].day_end_id;
            sima.App.setSpinner('on');
            $('#series-wrapper')
                .html('<div class="loader"></div>');
            var ajax_url_get = baseUrl + '/timeseries?shapefile_id=' + selected_shapefile + '&polygon_id=' + polygon_s + '&dataset_id=' + dataset_id + '&band_id=' + band_id + '&scale_id=' + scale_id + '&handler_id=' + series_str + '&mode_id=' + mode + "&startdate_id=" + startdate_id + "&enddate_id=" + enddate_id
            try {
                $.ajax({
                    type: "get"
                    , url: ajax_url_get
                    , success: function (data, text) {
                        if (data['error']) {
                            $('#series-wrapper')
                                .html(data['error'].toString());
                            sima.App.setSpinner('off');
                        } else {
                            sima.App.prototype.showHighSeriesDual(data['ts_valid'], data['ts_value'], series_desc, 'series-wrapper', region_name, data['y1_name'], data['y2_name']);
                            sima.App.setSpinner('off');
                        }
                    }
                    , error: function (request, status, error) {
                        $('#series-wrapper')
                            .html(request.responseText);
                        sima.App.setSpinner('off');
                    }
                });

            } catch (err) {
                $('#series-wrapper')
                    .html(err);
                sima.App.setSpinner('off');
            }
        } else {
            alert('No product selected');
        }
    }

}

/**
* Actualiza la variable global currentPoligon al nuevo valor seleccionado
* y actualiza los productos seleccionados a hacer uso del nuevo poligono.
* @param {newPolygon} Nuevo poligono dibujado en pantalla.
* @return {{none}}
*/
sima.App.prototype.setPolygon = function (newPolygon) {
    aoiPolygonSelectedFlag = true;
    this.clearPolygon();
    currentPolygon = newPolygon;
    this.getCoordinates(currentPolygon);
    var region_name = "Poligono manual"
    var mode = 'Polygon';
    $('#region_selected')
        .html(region_name);
    sima.App.SetAnalysis('on');
    setChartsOn();
    // Calcular el area del poligono
    sima.App.prototype.customPolygonArea(coordinatesArray, mode);
    // Gestion Time Series
    var selectedProd = $('#EOVisualizer')
        .val();
    if (EO_visualizator["products"][selectedProd].series_chart === "true" &&
        $("#serie_toggle")
        .is(':checked')) {
        sima.App.prototype.customTimeSeries(coordinatesArray, mode, region_name);
    }
    // Gestion Histogram
    if (EO_visualizator["products"][selectedProd].histo_chart === "true" &&
        $("#histo_toggle")
        .is(':checked')) {
        sima.App.prototype.customHistogram(coordinatesArray, mode, region_name);
    }
    // Gestion PieChart
    if (EO_visualizator["products"][selectedProd].pie_chart === "true" &&
        $("#pie_toggle")
        .is(':checked')) {
        sima.App.prototype.customPieChart(coordinatesArray, mode, region_name);
    }
};

/**
* Lee las coordenadas de un poligono en un Array
* @param {none} 
* @return {{none}}
*/
sima.App.prototype.getCoordinates = function () {
    var coordinate = [];
    coordinatesArray = [];
    for (var i = 0; i < currentPolygon.getPath()
        .getLength(); i++) {
        coordinate = currentPolygon.getPath()
            .getAt(i)
            .toUrlValue(6)
        coordinatesArray.push(coordinate);
    }
};

/**
* Activa el spinner bloqueando la interaccion con la aplicacion mientras dura el procesado en el AppEngine
* @param {none} 
* @return {{none}}
*/
sima.App.setSpinner = function (mode) {
    if (mode == 'on') {
        $(".spinner-overlay, .spinner-content")
            .addClass("active");
    } else {
        $(".spinner-overlay, .spinner-content")
            .removeClass("active");
    }
}

/**
* Gestion de datos luego un pixel ha sido seleccionado en pantalla.
* @param {lat_s} Latitud Pixel seleccionado
* @param {long_s} Logitud Pixel seleccionado
* @return {{none}}
*/
sima.App.prototype.handlePixelClick = function (lat_s, long_s) {
    var coordinate;
    coordinate = parseFloat(lat_s)
        .toFixed(6)
        .toString() + ',' + parseFloat(long_s)
        .toFixed(6)
        .toString();
    var region_name = 'Pixel seleccionado';
    var mode = 'Point';
    $('#region_selected')
        .html(region_name);
    sima.App.SetAnalysis('on');
    setChartsOn();
    var selectedProd = $('#EOVisualizer')
        .val();

    if (EO_visualizator["products"][selectedProd].series_chart === "true" &&
        $("#serie_toggle")
        .is(':checked')) {
        sima.App.prototype.customTimeSeries(coordinate, mode, region_name);
    }
    sima.App.prototype.customPixelValue(coordinate, mode);
}

/**
* Gestion de selccion de poligono. 
* @param {event} Evento que contiene los detalles del poligono seleccionado.
* @return {{none}}
*/
sima.App.prototype.handlePolygonClick = function (event) {
    aoiRegionSelectedFlag = true;
    // Obtener datos del poligono
    feature = event.feature;
    this.clear();
    this.map.data.overrideStyle(feature, {
        strokeWeight: 4
        , fillOpacity: '0.25'
    });

    // Obtener informacion de la region.
    var region_name = feature.getProperty('ADM1_NAME');
    var id = feature.getProperty('OBJECTID_1');
    var region_props = Object.keys(feature.l);
    var region_props_n = region_props.length;
    var mode = 'auto';
    $('#region_selected')
        .html(region_name);

    sima.App.SetAnalysis('on');
    setChartsOn();
    sima.App.prototype.setShapeTable();
    // Calcular area de la region
    sima.App.prototype.customPolygonArea(id, mode);
    var selectedProd = $('#EOVisualizer')
        .val();
    // Gestion TimeSeries
    if (EO_visualizator["products"][selectedProd].series_chart === "true" &&
        $("#serie_toggle")
        .is(':checked')) {
        sima.App.prototype.customTimeSeries(id, mode, region_name);
    }
    // Gestion Histogram
    if (EO_visualizator["products"][selectedProd].histo_chart === "true" &&
        $("#histo_toggle")
        .is(':checked')) {
        sima.App.prototype.customHistogram(id, mode, region_name);
    }
    // Gestion PieChart
    if (EO_visualizator["products"][selectedProd].pie_chart === "true" &&
        $("#pie_toggle")
        .is(':checked')) {
        sima.App.prototype.customPieChart(id, mode, region_name);
    }
};


/**
* Habilita/deshabilita tab de analisis. 
* @param {mode} Indica la accion a realizar sobre dicho tab.
* @return {{none}}
*/
sima.App.SetAnalysis = function (mode) {
    if (mode == 'on') {
        $(".analysis-overlay, .analysis-content")
            .addClass("active");
        $(".analysis-content")
            .show();
    } else {
        $(".analysis-content")
            .hide();
    }
}

/**
* Actualiza la informacion de los graficos al seleccionar otro producto.. 
* @param {{none}} 
* @return {{none}}
*/
sima.App.prototype.updateChartsInfo = function () {
    var selectedProd = $('#EOVisualizer')
        .val();
    if (aoiRegionSelectedFlag) {
        var region_name = feature.getProperty('ADM1_NAME');
        var id = feature.getProperty('OBJECTID_1');
        var mode = 'auto';
        if (EO_visualizator["products"][selectedProd].series_chart === "true" &&
            $("#serie_toggle")
            .is(':checked')) {
            setChartsOn();
            sima.App.prototype.customTimeSeries(id, mode, region_name);
        }
        // Gestion Histogram
        if (EO_visualizator["products"][selectedProd].histo_chart === "true" &&
            $("#histo_toggle")
            .is(':checked')) {
            setChartsOn();
            sima.App.prototype.customHistogram(id, mode, region_name);
        }
        // Gestion PieChart
        if (EO_visualizator["products"][selectedProd].pie_chart === "true" &&
            $("#pie_toggle")
            .is(':checked')) {
            setChartsOn();
            sima.App.prototype.customPieChart(id, mode, region_name);
        }
    }
    if (aoiPolygonSelectedFlag) {
        var region_name = "Poligono manual"
        var mode = 'Polygon';
        //Gestion Time Series
        if (EO_visualizator["products"][selectedProd].series_chart === "true" &&
            $("#serie_toggle")
            .is(':checked')) {
            setChartsOn();
            sima.App.prototype.customTimeSeries(coordinatesArray, mode, region_name);
        }
        // Gestion Histogram
        if (EO_visualizator["products"][selectedProd].histo_chart === "true" &&
            $("#histo_toggle")
            .is(':checked')) {
            setChartsOn();
            sima.App.prototype.customHistogram(coordinatesArray, mode, region_name);
        }
        // Gestion PieChart
        if (EO_visualizator["products"][selectedProd].pie_chart === "true" &&
            $("#pie_toggle")
            .is(':checked')) {
            setChartsOn();
            sima.App.prototype.customPieChart(coordinatesArray, mode, region_name);
        }
    }
}


/**
* Actualiza la tabla de informacion asociada al shape seleccionado. 
* @param {{none}} 
* @return {{none}}
*/
sima.App.prototype.setShapeTable = function () {

    var table = $("#resultTable");
    if ($('#polygon_detail_toggle')
        .is(':checked')) {
        // Generate table for display but dont return information until requested
        var result_html = "<table class='table table-bordered table-light table-width'>"
        feature.forEachProperty(function (value, property) {
            result_html += ["<tr>", '<td>', property, '</td><td>', value, '</td>', '</tr>'].join("\n");
        });
        result_html += ["</table>"];
        table.html(result_html);

    } else {
        table.html("");
    }
    $("#polygon_detail_div")
        .show();
}

/**
* ELimina los detalles y el poligono seleccionado. 
* @param {{none}} 
* @return {{none}}
*/
sima.App.prototype.clear = function () {
    $('.panel .title')
        .empty()
        .hide();
    $('.panel .wiki-url')
        .hide()
        .attr('href', '');
    $('.panel .chart')
        .empty()
        .hide();
    $('.panel .error')
        .empty()
        .hide();
    $('.panel')
        .hide();
    this.map.data.revertStyle();
};


/**
* Genera un mapa haciendo uso de los recursos de Google Maps
* @param {eeMapId} Id Earth Engine que permite la visualizacion del mapa
* @param {eeToken} Token de autenticacion.
* @return {google.maps.ImageMapType} Mapa a visualizar.
*/
sima.App.getEeMapType = function (eeMapId, eeToken) {
    var eeMapOptions = {
        getTileUrl: function (tile, zoom) {
            var url = sima.App.EE_URL + '/map/';
            url += [eeMapId, zoom, tile.x, tile.y].join('/');
            url += '?token=' + eeToken;
            return url;
        }
        , tileSize: new google.maps.Size(256, 256)
    };
    return new google.maps.ImageMapType(eeMapOptions);
};

sima.App.EE_URL = 'https://earthengine.googleapis.com';


sima.App.DEFAULT_ZOOM = 6;
if (typeof (defaultZoom) !== "undefined") {
    sima.App.DEFAULT_ZOOM = defaultZoom;
}

sima.App.DEFAULT_CENTER = {
    lng: -74
    , lat: 4.7
};
if (typeof (defaultLat) !== "undefined" && typeof (defaultLong) !== "undefined") {
    sima.App.DEFAULT_CENTER = {
        lng: defaultLong
        , lat: defaultLat
    };
}

sima.App.RESTRICTIONS = {
    north: 25.8
    , south: -10.8
    , west: -90
    , east: 70
};
if (typeof (defaultRestrictions) !== "undefined") {
    sima.App.RESTRICTIONS = defaultRestrictions;
}