/**
* Confirgacion de eventos y listeners de la aplicacion sima-dile.
*/

$(document)
    .ready(function () {
        /*
        * Gestor DRUPAL
        */
        if (document.getElementById("dss-simadile-main-page")) {
        } else {
            /*
            * Gestor applicacion stand Alone
            */
            initializeUI();
            $('#sidebarCollapse')
                .on('click', function () {
                    $('#sidebar')
                        .toggleClass('active');
                    if ($('#sidebar')
                        .hasClass('active')) hideTags()
                    else showTags();
                });
        }
    });

/**
* Carga todo el contenido de front-end e inicializa la UI.
* @param {none}
* @return {none}
*/
function loadHtml() {
    $('#dss-simadile-main-page')
        .load(baseUrl + '/www/template.html'
            , function () {
                $('#menu_loading_img')
                    .attr("src", baseUrl + "/www/img/spinner.gif");
                initializeUI();
                $('#sidebarCollapse')
                    .on('click', function () {
                        $('#sidebar')
                            .toggleClass('active');
                        if ($('#sidebar')
                            .hasClass('active')) hideTags()
                        else showTags();
                    });
            }
        );
}

var current_view = "info";
var current_step = "0";
/**
* Inicializa la UI.
* @param {none}
* @return {none}
*/
function initializeUI() {
    /**
    * Controladores de eventos del Front End.
    */
    $('#mapCollapse')
        .on('click', function () {
            if ($(".container.main")
                .width() === 1140 || $(".container.main")
                .width() == 1110) {
                $(".container.main")
                    .width('90%');
            } else {
                $(".container.main")
                    .width(1140);
            }
        });

    $(".components li a")
        .click(function (event) {
            event.preventDefault();
            $(this)
                .parent()
                .addClass('active')
                .siblings()
                .removeClass('active');

        });

    $("#analysis")
        .on("click", function (event) {
            event.preventDefault();
            $('#controles')
                .show();
            $("#mapa_ctrl")
                .hide();
            $(".options-overlay, .options-content")
                .removeClass("active");
        });

    $("#options")
        .on("click", function (event) {
            event.preventDefault();
            $("#controles")
                .hide()
            $("#mapa_ctrl")
                .show();
        });

    $("#help")
        .on("click", function (event) {
            event.preventDefault();
            $(".help-overlay, .help-content")
                .addClass("active");
            $("#controles")
                .hide();
            $("#mapa_ctrl")
                .hide();
        });

    $(".close_analysis")
        .on("click", function (event) {
            event.preventDefault();
            setChartsOff();
        });

    $(".close_help")
        .on("click", function (event) {
            event.preventDefault();
            $(".overlays-div")
                .toggleClass('active');

            $(".help-overlay, .help-content")
                .removeClass("active");
            $(".analysis-overlay, .analysis-content")
                .removeClass("active");

            if ($(".overlays-div")
                .hasClass('active')) {
                $("#closeHelpIcon")
                    .addClass('fa-times');
                $("#closeHelpIcon")
                    .removeClass('fa-plus');
                if (current_view === 'info') {
                    $(".help-overlay, .help-content")
                        .addClass("active");
                    $('.open_analysis')
                        .show();
                }
                if (current_view === 'graph') {
                    $(".analysis-overlay, .analysis-content")
                        .addClass("active");
                    $('.expand_help')
                        .show();
                }
            } else {
                $("#closeHelpIcon")
                    .addClass('fa-plus');
                $("#closeHelpIcon")
                    .removeClass('fa-times');
                $('.open_analysis')
                    .hide();
                $('.expand_help')
                    .hide();
            }

        });

    $(".expand_analysis, .open_analysis")
        .on("click", function (event) {
            event.preventDefault();
            setChartsOn();
            $('.expand_help')
                .show();
            $('.open_analysis')
                .hide();
            current_view = "graph";
        });

    $(".expand_help, .open_help")
        .on("click", function (event) {
            event.preventDefault();
            setHelpOn();
            $('.open_analysis')
                .show();
            $('.expand_help')
                .hide();
            current_view = "info";
        });


    $("#options")
        .on("click", function (event) {
            event.preventDefault();
            $("#controles")
                .hide();
            //Close Help
        });

    $("#analysis")
        .on("click", function (event) {
            event.preventDefault();
            //CLose Options
            $(".options-overlay, .options-content")
                .removeClass("active");
            $("#controles")
                .show();
            //Close Help
        });

    $('#productSelector')
        .change(function () {
            var product_selected = $('#productSelector')
                .val();
            var product_description = $("#productSelector")
                .find('option:selected')
                .attr("name");

        });

    $('#EOVisualizer')
        .change(function () {
            updateGeneralLayout();
            updateFlags();
            if (EO_visualizator["products"][$('#EOVisualizer')
                    .val()].date_scale !== 'none') {
                $('#layer-options')
                    .show();
            } else {
                $('#layer-options')
                    .hide();
            }
        });

    $('#EO_products')
        .change(function () {
            var group_id = $('#EO_products')
                .val();
            populateEOProducts(group_id);
            updateGeneralLayout();

            if (EO_visualizator["products"][$('#EOVisualizer')
                    .val()].date_scale !== 'none') {
                $('#layer-options')
                    .show();
            } else {
                $('#layer-options')
                    .hide();
            }
        });

    $('#User_products')
        .change(function () {
			var dataset_id = $('#User_products')
            .val();
			if(dataset_id.length >0)
			{
				sima.App.prototype.updateLegend();
				sima.App.prototype.updateInformation();
				sima.App.prototype.updateChartsInfo();
				sima.App.prototype.manageResources('write');
				sima.App.prototype.clearMap();
				$('#series-wrapper')
					.hide();
				$('#histo-wrapper')
					.hide();
				$('#pie-div')
					.hide();
				$('#layer-options')
					.hide();
			}
        });

    $('#ShapeSelector')
        .on('change', function () {
            selected_shapefile = this.value;
            sima.App.prototype.addPolygons(shapefileData[this.value], this.value);
        });

    $('#serie_toggle')
        .click(function () {
            if (!this.checked) {
                $('#productSelector')
                    .attr('disabled', true);
                $('#series-wrapper')
                    .hide();
            } else {
                $('#productSelector')
                    .attr('disabled', false);
                $('#series-wrapper')
                    .show();

                if (aoiPolygonSelectedFlag) {
                    var region_name = "Poligono manual"
                    var mode = 'Polygon';
                    setChartsOn();
                    var selectedProd = $('#EOVisualizer')
                        .val();
                    if (EO_visualizator["products"][selectedProd].series_chart === "true") {
                        sima.App.prototype.customTimeSeries(coordinatesArray, mode, region_name);
                    }
                }
                if (aoiRegionSelectedFlag) {
                    var region_name = feature.getProperty('ADM1_NAME');
                    var id = feature.getProperty('OBJECTID_1');
                    var mode = 'auto';

                    setChartsOn();

                    var selectedProd = $('#EOVisualizer')
                        .val();
                    if (EO_visualizator["products"][selectedProd].series_chart === "true") {
                        sima.App.prototype.customTimeSeries(id, mode, region_name);
                    }
                }

            }
        });

    $('#histo_toggle')
        .click(function () {
            if (!this.checked) {
                $('#histoSelector')
                    .attr('disabled', true);
                $('#histo-wrapper')
                    .hide();
            } else {
                $('#histoSelector')
                    .attr('disabled', false);
                $('#histo-wrapper')
                    .show();
                if ($('#histo_toggle')
                    .is(':checked') && $('#pixel_toggle')
                    .is(':checked')) {

                    $('#pixel_toggle')
                        .prop("checked", false);
                    sima.App.prototype.disablePixel();
                    $("#region_selected")
                        .html("");
                    $("#value-wrapper")
                        .html("");

                }
                if (aoiPolygonSelectedFlag) {
                    var region_name = "Poligono manual"
                    var mode = 'Polygon';
                    setChartsOn();
                    var selectedProd = $('#EOVisualizer')
                        .val();
                    if (EO_visualizator["products"][selectedProd].histo_chart === "true") {
                        sima.App.prototype.customHistogram(coordinatesArray, mode, region_name);
                    }
                }
                if (aoiRegionSelectedFlag) {
                    var region_name = feature.getProperty('ADM1_NAME');
                    var id = feature.getProperty('OBJECTID_1');
                    var mode = 'auto';
                    setChartsOn();
                    var selectedProd = $('#EOVisualizer')
                        .val();
                    if (EO_visualizator["products"][selectedProd].histo_chart === "true") {
                        sima.App.prototype.customHistogram(id, mode, region_name);
                    }
                }

            }
        });

    $('#pie_toggle')
        .click(function () {
            if (!this.checked) {
                $('#pieSelector')
                    .attr('disabled', true);
                $('#pie-div')
                    .hide();
            } else {
                $('#pieSelector')
                    .attr('disabled', false);
                $('#pie-div')
                    .show();
                if ($('#pie_toggle')
                    .is(':checked') && $('#pixel_toggle')
                    .is(':checked')) {
                    $('#pixel_toggle')
                        .prop("checked", false);
                    sima.App.prototype.disablePixel();
                    $("#region_selected")
                        .html("");
                    $("#value-wrapper")
                        .html("");
                }
                if (aoiPolygonSelectedFlag) {
                    var region_name = "Poligono manual"
                    var mode = 'Polygon';
                    setChartsOn();
                    var selectedProd = $('#EOVisualizer')
                        .val();
                    // Handler Histogram
                    if (EO_visualizator["products"][selectedProd].pie_chart === "true") {
                        sima.App.prototype.customPieChart(coordinatesArray, mode, region_name);
                    }
                }
                if (aoiRegionSelectedFlag) {
                    var region_name = feature.getProperty('ADM1_NAME');
                    var id = feature.getProperty('OBJECTID_1');
                    var mode = 'auto';
                    setChartsOn();
                    var selectedProd = $('#EOVisualizer')
                        .val();
                    // Handler Histogram
                    if (EO_visualizator["products"][selectedProd].pie_chart === "true") {
                        sima.App.prototype.customPieChart(id, mode, region_name);
                    }
                }
            }
        });

    $("#polygon_detail_toggle")
        .click(function () {
            sima.App.prototype.setShapeTable();
        });


    $('#region_toggle')
        .click(function () {
            if ($('#drawing_toggle')
                .is(':checked') && $('#region_toggle')
                .is(':checked')) {
                $('#drawing_toggle')
                    .prop("checked", false);
                sima.App.prototype.disablePolygon();
                $('#polygon_detail_div')
                    .hide();
                $("#region_selected")
                    .html("");
                $("#value-wrapper")
                    .html("");

            }
            if ($('#pixel_toggle')
                .is(':checked') && $('#region_toggle')
                .is(':checked')) {
                $('#pixel_toggle')
                    .prop("checked", false);
                sima.App.prototype.disablePixel();
                $("#region_selected")
                    .html("");
                $("#value-wrapper")
                    .html("");
            }
            if (!this.checked) {
                $('#ShapeSelector')
                    .attr('disabled', true);
                $('#polygon_detail_div')
                    .hide();
                $("#region_selected")
                    .html("");
                $("#value-wrapper")
                    .html("");
                aoiRegionSelectedFlag = false;
                sima.App.prototype.hideStates();
            } else {
                $('#polygon_detail_div')
                    .show();
                $('#ShapeSelector')
                    .attr('disabled', false);
                selected_shapefile = $('#ShapeSelector')
                    .val();
                sima.App.prototype.addPolygons(shapefileData[$('#ShapeSelector')
                        .val()], $('#ShapeSelector')
                    .val());
            }
        });

    $('#drawing_toggle')
        .click(function () {
            if ($('#drawing_toggle')
                .is(':checked') && $('#region_toggle')
                .is(':checked')) {
                $('#region_toggle')
                    .prop("checked", false);
                $('#ShapeSelector')
                    .attr('disabled', false);
                selected_shapefile = $('#ShapeSelector')
                    .val();
                sima.App.prototype.addPolygons(shapefileData[$('#ShapeSelector')
                        .val()], $('#ShapeSelector')
                    .val());
            }
            if ($('#drawing_toggle')
                .is(':checked') && $('#pixel_toggle')
                .is(':checked')) {
                $('#pixel_toggle')
                    .prop("checked", false);
                sima.App.prototype.disablePixel();
                $("#region_selected")
                    .html("");
                $("#value-wrapper")
                    .html("");

            }
            if (!this.checked) {
                sima.App.prototype.disablePolygon();
                $('#polygon_detail_div')
                    .hide();
                $("#region_selected")
                    .html("");
                $("#value-wrapper")
                    .html("");
                aoiPolygonSelectedFlag = false;
            } else {
                $('#polygon_detail_div')
                    .hide();
                $('#ShapeSelector')
                    .attr('disabled', true);
                sima.App.prototype.hideStates();
                sima.App.prototype.drawPolygon();
            }
        });

    $('#pixel_toggle')
        .click(function () {
            if (!$('#layer_toggle')
                .is(':checked') && !$('#custom_toggle')
                .is(':checked')) {
                alert("Debe seleccionar una capa");
                return false;
            }

            if ($('#drawing_toggle')
                .is(':checked')) {
                $('#drawing_toggle')
                    .prop("checked", false);
                sima.App.prototype.disablePolygon();
                $('#polygon_detail_div')
                    .hide();
                $("#region_selected")
                    .html("");
                $("#value-wrapper")
                    .html("");
            }
            if ($('#region_toggle')
                .is(':checked')) {
                $('#region_toggle')
                    .prop("checked", false);
                $('#ShapeSelector')
                    .attr('disabled', true);
                sima.App.prototype.hideStates();
            }
            if ($('#pie_toggle')
                .is(':checked') && $('#pixel_toggle')
                .is(':checked')) {
                $('#pie_toggle')
                    .prop("checked", false);
                $('#pieSelector')
                    .attr('disabled', false);
                $('#pie-div')
                    .show();
            }
            if ($('#histo_toggle')
                .is(':checked') && $('#pixel_toggle')
                .is(':checked')) {
                $('#histo_toggle')
                    .prop("checked", false);
                $('#histoSelector')
                    .attr('disabled', false);
                $('#histo-wrapper')
                    .show();
            }
            if (!this.checked) {
                sima.App.prototype.disablePixel();
                $("#region_selected")
                    .html("");
                $("#value-wrapper")
                    .html("");
            } else {
                $('#polygon_detail_div')
                    .hide();
                $("#polygon_detail_div")
                    .hide();
                $('#pieSelector')
                    .attr('disabled', true);
                $('#pie-div')
                    .hide();
                $('#histoSelector')
                    .attr('disabled', true);
                $('#histo-wrapper')
                    .hide();
                sima.App.prototype.enablePixel();
            }
        });

    $('#custom_toggle')
        .click(function () {
            if (this.checked) {
                hideAllGraphControls();
                if ($('#layer_toggle')
                    .is(':checked')) {
                    $('#layer_toggle')
                        .prop("checked", false);
                    hideProductControls();
                    $('#EOVisualizer')
                        .attr('disabled', true);
                    $('#EO_products')
                        .attr('disabled', true);
                    $('.legend-overlay')
                        .hide();
                    sima.App.prototype.clearMap();
                }
                hideAllGraphControls();
                setChartsOff();
                sima.App.prototype.disablePixel();
                sima.App.prototype.updateLegend();
                sima.App.prototype.updateInformation();
                sima.App.prototype.clearMap();
                sima.App.prototype.manageResources('write');
				var dataset_id = $('#User_products')
					.val();
				if(dataset_id.length >0)
				{
					$(".overlays-div")
                    .show();
				}
				else
				{
					$(".overlays-div")
                    .hide();
				}
                $('#pie_toggle')
                    .prop("checked", false);
                $('#histo_toggle')
                    .prop("checked", false)
                $('#serie_toggle')
                    .prop("checked", false);
                $('#pixel_toggle')
                    .prop("checked", false);
                $('#drawing_toggle')
                    .prop("checked", false);
                $(".overlays-div")
                    .addClass("active");
                $("#closeHelpIcon")
                    .addClass('fa-times');
                $("#closeHelpIcon")
                    .removeClass('fa-plus');
                $(".help-overlay, .help-content")
                    .addClass("active");
                $('.open_analysis')
                    .show();
                $('.legend-overlay')
                    .show();
                $('#layer-options')
                    .hide();
                $('#User_products')
                    .show();
                $('#User_products')
                    .attr('disabled', false);
                $(".analysis-overlay, .analysis-content")
                    .removeClass("active");
                $("#drawing_toggle_control")
                    .hide();
                $('#pixel_toggle_control')
                    .hide()
                $("#masking_control")
                    .hide();
                $('.expand_help')
                    .show();
            } else {
                $('#User_products')
                    .attr('disabled', true);
                $('#User_products')
                    .hide();
                sima.App.prototype.clearMap();
                if ($('#pixel_toggle')
                    .is(':checked')) {
                    $('#pixel_toggle')
                        .prop("checked", false);
                }
            }
        });

    $('#layer_toggle')
        .click(function () {
            setChartsOff();
            if (!this.checked) {
                hideProductControls();
                sima.App.prototype.clearMap();
                if ($('#pixel_toggle')
                    .is(':checked')) {
                    $('#pixel_toggle')
                        .prop("checked", false);
                }
                $('#EOVisualizer')
                    .attr('disabled', true);
                $('#EO_products')
                    .attr('disabled', true);
                $('.legend-overlay')
                    .hide();
                $('#layer-options')
                    .hide();
                $("#closeHelpIcon")
                    .addClass('fa-plus');
                $("#closeHelpIcon")
                    .removeClass('fa-times');
                $('.open_analysis')
                    .hide();
                $('.expand_help')
                    .hide();
                $(".overlays-div")
                    .removeClass("active");
                $(".help-overlay, .help-content")
                    .removeClass("active");
                $(".analysis-overlay, .analysis-content")
                    .removeClass("active");
            } else {
                if ($('#custom_toggle')
                    .is(':checked')) {
                    $('#custom_toggle')
                        .prop("checked", false);
                    $('#User_products')
                        .attr('disabled', true);
                    $('#User_products')
                        .hide();
                    sima.App.prototype.clearMap();
                }
                showProductControls();
                $(".overlays-div")
                    .show();
                $(".overlays-div")
                    .addClass("active");
                $("#closeHelpIcon")
                    .addClass('fa-times');
                $("#closeHelpIcon")
                    .removeClass('fa-plus');
                if (current_view === 'info') {
                    $(".help-overlay, .help-content")
                        .addClass("active");
                    $('.open_analysis')
                        .show();
                }
                if (current_view === 'graph') {
                    $(".analysis-overlay, .analysis-content")
                        .addClass("active");
                    $('.expand_help')
                        .show();
                }
                $('.legend-overlay')
                    .show();
                if ($('#EO_products')
                    .val() == 'WI') {
                    $('#layer-options')
                        .show();
                } else {
                    $('#layer-options')
                        .hide();
                }
                $('#EOVisualizer')
                    .attr('disabled', false);
                $('#EO_products')
                    .attr('disabled', false);
                if (EO_visualizator["products"][$('#EOVisualizer')
                        .val()].date_scale !== 'none') {
                    $('#layer-options')
                        .show();
                } else {
                    $('#layer-options')
                        .hide();
                }
                $("#drawing_toggle_control")
                    .show();
                $("#pixel_toggle_control")
                    .show();
                updateGeneralLayout();
                updateFlags();
            }
        });

    $("input[name=date_type_selector]:radio")
        .change(function () {
            var selected_value = $("input[name='date_type_selector']:checked")
                .val();
            if (selected_value === 'single') {
                sima.App.prototype.activateSingleSlider();
            } else if (selected_value === 'range') {
                sima.App.prototype.activateRangeSlider();
            }
        });

    $('#date_type_radio_box')
        .change(function () {
            var selected_value = $("input[name='date_type_selector']:checked")
                .val();
            if (selected_value === 'single') {
                sima.App.prototype.activateSingleSlider();
            } else if (selected_value === 'range') {
                sima.App.prototype.activateRangeSlider();
            }
        });

    $("#masking_toggle")
        .click(function () {
            if ($('#layer_toggle')
                .is(':checked') || $('#custom_toggle')
                .is(':checked')) {
                updateGeneralLayout();
            } else {
                alert("Debe seleccionar capa");
                return false;
            }
        });

    $("#opacity_Selector")
        .on('change input', function () {
            sima.App.prototype.changeOpacity($(this)
                .val());
        });

    $("#rangeApply")
        .click(function (event) {
            $('#rangeApply')
                .attr('disabled', true);
            sima.App.prototype.clearMap()
            sima.App.prototype.updateEOMapType(false);
            sima.App.prototype.updateLegend();
            sima.App.prototype.updatePlots();
        });

    sima.App.prototype.updateRange = function (day_start, day_end) {
        var d_ini = new Date();
        var d_end = new Date();
        d_ini.setDate(d_ini.getDate() - day_start);
        d_end.setDate(d_end.getDate() - day_end);
        $("#rangeValue")
            .html(d_end.toLocaleDateString() + " - " + d_ini.toLocaleDateString());
    };

    sima.App.prototype.manageResources('read');
    $('#User_products')
        .attr('disabled', true);
    resetControls();
    hideProductControls();
    selectSingleDateSlider();
    $('#User_products')
        .hide();
    $('#EO_products')
        .val('SWV');
    $(".overlays-div")
        .hide();
}

/**
* Metodos de control de estructuras y datos en front End. Habilitan y facilitan a interaccion.
*/

/**
* Actualizar parametros de Slider.
* @param {val_min} Valor minimo slider
* @param {val_max} VAlor maximo slider
* @return {none}
*/
function updateSlider(val_min, val_max) {
    $("#slider-range")
        .slider({
            values: [val_min, val_max]
        });
}

/**
* Oculta un div de grafico indicado.
* @param {chart_id} Valor minimo slider (pie|series|histo)
* @return {none}
*/
function disableChartDiv(chart_id) {
    if (chart_id == "pie") $('#pie-chart-div')
        .hide();
    if (chart_id == "series") $('#series-wrapper')
        .hide();
    if (chart_id == "histo") $('#histo-wrapper')
        .hide();
}

/**
* Muestra un div de grafico indicado.
* @param {chart_id} Valor minimo slider (pie|series|histo)
* @return {none}
*/
function disableChartDiv(chart_id) {
    if (chart_id == "pie") $('#pie-chart-div')
        .show();
    if (chart_id == "series") $('#series-wrapper')
        .show();
    if (chart_id == "histo") $('#histo-wrapper')
        .show();
}

/**
* Reseteo de controles.
* @param {none}
* @return {none}
*/
function resetControls() {
    if ($('#masking_toggle')
        .is(':checked')) {
        $('#masking_toggle')
            .prop("checked", false);
    }
    if ($('#layer_toggle')
        .is(':checked')) {
        $('#layer_toggle')
            .prop("checked", false);
    }
    if ($('#custom_toggle')
        .is(':checked')) {
        $('#custom_toggle')
            .prop("checked", false);
    }
    if (!$('#pie_toggle')
        .is(':checked')) {
        $('#pie_toggle')
            .prop("checked", false);
    }
    if (!$('#serie_toggle')
        .is(':checked')) {
        $('#serie_toggle')
            .prop("checked", false);
    }
    if (!$('#histo_toggle')
        .is(':checked')) {
        $('#histo_toggle')
            .prop("checked", false);
    }
    if ($('#region_toggle')
        .is(':checked')) {
        $('#region_toggle')
            .prop("checked", false);
    }
    if ($('#drawing_toggle')
        .is(':checked')) {
        $('#drawing_toggle')
            .prop("checked", false);
    }
    if ($('#pixel_toggle')
        .is(':checked')) {
        $('#pixel_toggle')
            .prop("checked", false);
    }
}

/**
* Seleccion de fecha unica en el slider temporal
* @param {none}
* @return {none}
*/
function selectSingleDateSlider() {
    $("input[name='date_type_selector'][value='single']")
        .prop('checked', true);
    sima.App.prototype.activateSingleSlider();
    return false;
}

/**
* Seleccion de rango de fechas en el slider temporal
* @param {none}
* @return {none}
*/
function setDatesSliderRange() {
    var second_date = new Date('2014-05-24')
    var first_date = new Date('2019-04-26')
    var millisecondsPerDay = 1000 * 60 * 60 * 24;
    var millisBetween = first_date.getTime() - second_date.getTime();
    var days = millisBetween / millisecondsPerDay;
    return (days);
}

/**
* Deshabilita la ventana de informacion adicional
* @param {none}
* @return {none}
*/
function setHelpOff() {
    $('#EO_info_title')
        .hide();
    $('#EO_info_desc')
        .hide();
    $('#EO_info_content')
        .hide();
    $('#EO_info_source')
        .hide();
    $('.expand_help')
        .show();
}

/**
* Habilita la ventana de informacion adicional
* @param {none}
* @return {none}
*/
function setHelpOn() {
    $('#EO_info_title')
        .show();
    $('#EO_info_desc')
        .show();
    $('#EO_info_content')
        .show();
    $('#EO_info_source')
        .show();
    $(".analysis-overlay, .analysis-content")
        .removeClass("active");
    $(".help-overlay, .help-content")
        .addClass("active");
    $('.white_font')
        .show();
    $('.expand_help')
        .hide();
}

/**
* Deshabilita todos los controles de graficos
* @param {none}
* @return {none}
*/
function hideAllGraphControls() {
    $("#series_control")
        .hide();
    $("#histo_control")
        .hide();
    $("#pie_chart_control")
        .hide();
    $("#no_hay_graficos")
        .show();
}

/**
* Desactiva todos los graficos
* @param {none}
* @return {none}
*/
function setChartsOff() {
    $('#pie-div')
        .hide();
    $('#series-wrapper')
        .hide();
    $('#histo-wrapper')
        .hide();
    $('.white_font')
        .hide();
    $('.close_analysis')
        .hide();
    $('.expand_analysis')
        .show();
}

/**
* Activa todos los graficos
* @param {none}
* @return {none}
*/
function setChartsOn() {
    $(".analysis-overlay, .analysis-content")
        .addClass("active");
    $(".help-overlay, .help-content")
        .removeClass("active");
    if (!$('#pixel_toggle')
        .is(':checked')) {
        if ($('#pie_toggle')
            .is(':checked')) $('#pie-div')
            .show();
        if ($('#histo_toggle')
            .is(':checked')) $('#histo-wrapper')
            .show();
    }
    if ($('#serie_toggle')
        .is(':checked')) $('#series-wrapper')
        .show();
    $('.white_font')
        .show();
    $('.close_analysis')
        .show();
}

/**
* Actualiza el Layout de la app acorde a las diferntes opciones seleccionadas.
* @param {none}
* @return {none}
*/
function updateGeneralLayout() {
    sima.App.prototype.clearMap();
    sima.App.prototype.updateEOMapType(true);
    sima.App.prototype.updateLegend();
    sima.App.prototype.updateInformation();
    sima.App.prototype.updateChartsInfo();
    $("#opacity_Selector")
        .val(100);
}

/**
* Oculta los controles de productos.
* @param {none}
* @return {none}
*/
function hideProductControls() {
    $("#layer_caption2")
        .hide();
    $("#layer_caption3")
        .hide();
    $(".graph_indicators")
        .hide();
    $('#EOVisualizer')
        .hide();
    $('#EO_products')
        .hide();
}

/**
* Muestra los controles de productos.
* @param {none}
* @return {none}
*/
function showProductControls() {
    $("#layer_caption2")
        .show();
    $("#layer_caption3")
        .show();
    $(".graph_indicators")
        .show();
    $('#EOVisualizer')
        .show();
    $('#EO_products')
        .show();
}

/**
* Oculta etiquetas de informacion
* @param {none}
* @return {none}
*/
function hideTags() {
    $('#pie_caption')
        .hide();
    $('#series_caption')
        .hide();
    $('#histo_caption')
        .hide();
    $('#layer_caption')
        .hide();
    $('#layer_caption2')
        .hide();
    $('#layer_caption3')
        .show();
    $('#shape_caption')
        .hide();
    $('#polygon_caption')
        .hide();
    $('#drawing_caption')
        .hide();
    $('#pixel_caption')
        .hide();
    $('#opacity_caption')
        .hide();
    $('#custom_caption')
        .hide();

}

/**
* Muestra etiquetas de informacion
* @param {none}
* @return {none}
*/
function showTags() {
    $('#pie_caption')
        .show();
    $('#series_caption')
        .show();
    $('#histo_caption')
        .show();
    $('#layer_caption')
        .show();
    $('#layer_caption2')
        .show();
    $('#layer_caption3')
        .show();
    $('#shape_caption')
        .show();
    $('#polygon_caption')
        .hide();
    $('#drawing_caption')
        .show();
    $('#pixel_caption')
        .show();
    $('#opacity_caption')
        .show();
    $('#custom_caption')
        .show();

}

/**
* Actualiza opciones al menu de seleccion de imagenes con rango.
* @param {type} Tipo de imagen seleccionada.
* @return {none}
*/
function updateRangeAggregationTypeSelector(type) {
    if (type === 'landsat') {
        $("#rangeAggregationTypeSelector")
            .html(
                '<option value="maximo">Máximo</option>' +
                '<option value="minimo">Mínimo</option>' +
                '<option value="promedio">Promedio</option>'
            );
    } else {
        $("#rangeAggregationTypeSelector")
            .html(
                '<option value="maximo">Máximo</option>' +
                '<option value="minimo">Mínimo</option>' +
                '<option value="promedio">Promedio</option>' +
                '<option value="diferencia">Diferencia</option>'
            );
    }
}

/**
* Obtiene el ultimo dia de un mes pasado por parametro.
* @param {month} Mes a procesar.
* @return {lastDayMonth} Valor numerico del ultimo dia de mes.
*/
function getLastDayMonthnth(month) {
    var lastDayMonth = 31;
    if (parseInt(month) === 2) {
        lastDayMonth = '28';
    }
    if (parseInt(month) === 4 ||
        parseInt(month) === 6 ||
        parseInt(month) === 9 ||
        parseInt(month) === 11) {
        lastDayMonth = '30';
    }
    return lastDayMonth;
}

/**
* Obtiene el valor numerico de un mes sobre un anio pasado por parametro.
* @param {none}.
* @return {none}.
*/
function getYearMonth(date) {
    var dateParts = date.split('-');
    if (dateParts.length < 2) {
        dateParts = date.split('/');
    }
    var year = dateParts[0];
    var month = dateParts[1];
    return year + '-' + month;
}

/**
* Manipula los pasos de la "Ayuda" para agilizar la visualizacion.
* @param {date} Fecha a procesar.
* @return {integer} Valor numerico de un mes.
*/

function startObjectsIntro() {
    var intro = introJs().setOption('showProgress', true);
    intro.start().onbeforechange(function () {
        if (intro._currentStep >= "6") {
            $('#analysis').click();
            if (intro._currentStep == "6" && $('#region_toggle').is(":checked") == false) {
                $('#region_toggle').click();
            }
            if (intro._currentStep == "7"){
                if ($('#pie_toggle').is(":checked") == false){
                    $('#pie_toggle').click();
                }
                if ($('#serie_toggle').is(":checked") == false){
                    $('#serie_toggle').click();
                }
                if ($('#histo_toggle').is(":checked") == false){
                    $('#histo_toggle').click();
                }
            }

        } else {
            $('#options').click();
            if (intro._currentStep == "2" && $('#layer_toggle').is(":checked") == false) {
                $('#layer_toggle').click();
            }
        }
        current_step = intro._currentStep;
    });
}

$('#pie_toggle').click();
$('#serie_toggle').click();
$('#histo_toggle').click();